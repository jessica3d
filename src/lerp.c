#include "lerp.h"

/* Key choose based on a key value */
static void choose_key(struct lerp *lrp, double val, int *first, int *second)
{
	/* Checking lower limits */
	if (val <= lrp->keys[0]) {
		*first = *second = 0;
		return;
	} 

	for (int i = 1; i < lrp->num_key; i++) {
		if (val <= lrp->keys[i]) {
			*first = i-1;
			*second = i;
			return;
		}
	}

	/* At higher limit */
	*first = *second = lrp->num_key - 1;
}

/* Setting of the key fraction */
static void lerp_set_key(struct lerp* lrp, size_t off, double *val)
{
	lrp->key = *val;
	lerp_eval(lrp);
}

/* Quaternion spherical linear interpolation */
static void quat_lerp_eval(struct quat_lerp *qlrp)
{
	int interv[2];
	struct lerp *lrp = (struct lerp*)qlrp;
	double key = lrp->key;
	
	/* Getting interpolation interval */
	choose_key(lrp, lrp->key, &interv[0], &interv[1]);

	/* Checking perfect values */
	if (key == lrp->keys[interv[0]]) {
		qlrp->value = qlrp->values[interv[0]];
		return;
	}

	if (key == lrp->keys[interv[1]]) {
		qlrp->value = qlrp->values[interv[1]];
		return;
	}

	/* Boundaries */
	if (interv[0] == interv[1]) {
		qlrp->value = qlrp->values[interv[0]];
		return;
	}

	/* Interpolating */
	qlrp->value = quat_slerp(&qlrp->values[interv[0]], 
				 &qlrp->values[interv[1]], 
				 (key-lrp->keys[interv[0]]) /
				 (lrp->keys[interv[1]]-lrp->keys[interv[0]]));
}

/* Quaternion/Orientation dynamic functions setup */
void quat_lerp_dyn_setup(struct quat_lerp_dynt *dynt)
{
	((struct lerp_dynt*)dynt)->eval = (lerp_eval_func)quat_lerp_eval;
}

/* Object type related info */
const struct obj_att lerp_set_fraction_att = {
	.name = "set_fraction",
	.size = sizeof(double),
	.set = (set_func)lerp_set_key
};

static const struct obj_att *lerp_atts[] = {
	&lerp_set_fraction_att,
	NULL
};

const struct obj_comp lerp_comp = {
	.attribs = lerp_atts
};

const struct obj_att quat_lerp_value_changed_att = {
	.name = "value_changed",
	.off = offsetof(struct quat_lerp, value),
	.size = sizeof(quat),
	.get = get_quat
};

static const struct obj_att *quat_lerp_atts[] = {
	&quat_lerp_value_changed_att,
	NULL
};

const struct obj_comp quat_lerp_comp = {
	.attribs = quat_lerp_atts,
	.dyn_setup = (dyn_setup_func)quat_lerp_dyn_setup
};

/* Dynamic tables */
struct quat_lerp_dynt quat_lerp_dynt;

const struct obj_comp normal_lerp_comp;
const struct obj_comp vec3_lerp_comp;
const struct obj_comp vec2_lerp_comp;
const struct obj_comp scalar_lerp_comp;

