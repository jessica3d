#ifndef SCENE_H
#define SCENE_H

/* timespec */
#include <time.h>

#include "color.h"
#include "list.h"
#include "stack.h"
#include "view.h"
#include "group.h"

#include "rendering.h"

#define	MAX_LIGHT	4

struct environment {
	color4		color;
};

struct viewport {
	GLint	origin[2];
	GLint	size[2];
};

struct scene {
	/* Rendering info */
	STACK(struct viewpoint)	viewpoints;
	/* FIXME: Use tree of arrays for routes */
	LIST(struct route*)	routes;
	LIST(struct sensor*)	sensors;
	LIST(struct lerp*)	lerps;

	struct group		content;	/* Root node for content */
	struct light		*lights[MAX_LIGHT];
	struct environment	env;

	/* Rendering info */
	struct viewport	vp;
	struct view	view;
	struct timespec	setup_time;		/* Setup time value */
};

/* Scene manip */
void scene_init(struct scene *scn);
void scene_destroy(struct scene *scn);
void scene_setup(struct scene *scn);
void scene_render(struct scene *scn);
void scene_resize(struct scene *scn);
void scene_eval_sensors(struct scene *scn);

#endif
