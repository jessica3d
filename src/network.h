#ifndef NETWORK_H
#define NETWORK_H

/* size_t */
#include <stddef.h>

/* Initialization of the network component */
int network_init(void);

/* Tells if a location should be accessed remotely */
int is_remote(const char *loc);

/* Data fetch from a remote location, returning data size in size */
int network_receive(const char *url, char **data, size_t *size);


#endif	/* NETWORK_H */
