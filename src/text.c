#include "jessica.h"
#include "text.h"
#include "image.h"
#include "manager.h"

#ifndef HAVE_PANGOCAIRO
#include <pango/pangocairo.h>
#endif

/* Memory management */
#include <stdlib.h>

/* strcmp(3) */
#include <string.h>

#ifdef HAVE_PANGOCAIRO
/* Families translations between X3D and Pango */
struct family_trans {
	const char	*x3d;
	const char	*pango;
} static const families_trans[] = {
	{ "SERIF", "serif" },
	{ "SANS", "sans-serif" },
	{ "TYPEWRITER", "monospace" }
};

#define FONT_RES	80		/* Font size in point */

static const char default_pango_family[] = "serif";

/* Default font when required */
static struct font *default_font;

/* Translation of a families list to a pango description */
static char* families_to_pango(char **families, int num_family)
{
	const char *fams[num_family];
	int fam_lens[num_family];

	int total_len = 0;
	int pos;

	/* Checking family translations */
	for (int i = 0; i < num_family; i++) {
		for (int j = 0; j < sizeof(families_trans)/
				sizeof(struct family_trans); i++) {
			fams[i] = NULL;
			if (!strcmp(families_trans[i].x3d, families[i])) {
				fams[i] = families_trans[i].pango;
				break;
			}

			if (!fams[i])
				fams[i] = families[i];
			fam_lens[i] = strlen(fams[i]);
			total_len += fam_lens[i];
		}
	}

	/* Concatenating these families */
	char *pango_fams = (char*)malloc(total_len + (num_family<<1)-1);
	memcpy(pango_fams, fams[0], fam_lens[0]);
	pos = fam_lens[0];

	/* Joining entries in a coma list */
	for (int i = 1; i < num_family; i++) {
		pango_fams[pos++] = ',';
		pango_fams[pos++] = ' ';
		memcpy(&pango_fams[pos], fams[i], fam_lens[i]);
		pos += fam_lens[i];
	}

	return pango_fams;
}
#endif

/* Text renderering object initialization */
static void text_init(struct text* txt)
{
	/* X3D Specification 15.4.2 */
	((struct geom*)txt)->cull = 1;
	txt->max_extent = -1;
}

/* Text rendering setup */
static int text_setup(struct text *txt)
{
#ifdef HAVE_PANGOCAIRO
	struct font *ft;
	int total_len;
	char *buf;
	int w, h, wpow2, hpow2;

	assert(txt->strings);

	if (txt->font) {
		font_setup(txt->font);
		ft = txt->font;
	} else {
		if (!default_font) {
			default_font = (struct font*)obj_create(&font_type);
			font_setup(default_font);
		}
		ft = default_font;
	}

	/* Getting text length, through pango extents */

	/* Dummy render surface for calcs */
	cairo_surface_t *canvas = cairo_image_surface_create(
					CAIRO_FORMAT_ARGB32, 1, 1);
	cairo_t *cr = cairo_create(canvas);
	cairo_surface_destroy(canvas);

	/* Creating layout for this surface */
	PangoLayout *layout = pango_cairo_create_layout(cr);

	/* Setting font */
	pango_layout_set_font_description (layout, ft->pango_ft);

	/* Aligning text in S dir */
	switch (ft->justify[0]) {
		case FONT_JUSTIFY_BEGIN:
			pango_layout_set_alignment(layout, PANGO_ALIGN_LEFT);
			break;
		case FONT_JUSTIFY_END:
			pango_layout_set_alignment(layout, PANGO_ALIGN_RIGHT);
			break;
		case FONT_JUSTIFY_FIRST:
			pango_layout_set_alignment(layout, PANGO_ALIGN_LEFT);
			break;
		case FONT_JUSTIFY_MIDDLE:
			pango_layout_set_alignment(layout, PANGO_ALIGN_CENTER);
			break;
	}

	/* Getting metrics */
	PangoContext *pango_ctx = pango_layout_get_context(layout);;
	PangoFontMetrics *metrics = pango_context_get_metrics(pango_ctx,
					ft->pango_ft, ft->pango_lang);
	int ascent = pango_font_metrics_get_ascent(metrics);
	int descent = pango_font_metrics_get_descent(metrics);
	
	pango_font_metrics_unref(metrics);

	/* Setting spacing between lines */
	pango_layout_set_spacing(layout, (1-ft->spacing)*(ascent - descent));

	/* Setting default layout size to query length */
	pango_layout_set_width(layout, -1);
	total_len = 0;
	if (txt->num_string > 1) {
		int line_lens[txt->num_string];

		for (int i = 0; i < txt->num_string; i++) {
			line_lens[i] = strlen(txt->strings[i]);
			total_len += line_lens[i]+1;
		}
		total_len--;

		/* Pango paragraphs are based on line breaks */
		buf = (char*)malloc(total_len+1);
		memcpy(buf, txt->strings[0], line_lens[0]);
		int pos = line_lens[0];
		for (int i = 1; i < txt->num_string; i++) {
			buf[pos++] = '\n';
			memcpy(buf+pos, txt->strings[i], line_lens[i]);
			pos += line_lens[i];
		}

		buf[pos] = 0;
	} else {
		buf = txt->strings[0];
		total_len = strlen(txt->strings[0]);
	}

	/* Pushing text to Pango */
	pango_layout_set_text(layout, buf, total_len);
	if (txt->num_string > 1)
		free(buf);

	int layout_lens[pango_layout_get_line_count(layout)];

	/* TODO: Adjust the length before rendering */
	for (int i = 0; i < pango_layout_get_line_count(layout); i++) {
		pango_layout_get_size(pango_layout_get_line(layout, i)
				->layout, &layout_lens[i], NULL);
		if (txt->max_length < layout_lens[i])
			txt->max_length = layout_lens[i];
	}

	/* Noting size, based on ascent */
	pango_layout_get_size(layout, &w, &h);
	txt->width = w / (double)ascent;
	txt->height = h / (double)ascent;
	txt->max_length /= (double)ascent;

	/* Recaling in output device coords */
	w = w*ft->size/PANGO_SCALE;
	h = h*ft->size/PANGO_SCALE;

	/* Adusting to a power of two */
	wpow2 = to_pow2(w);
	hpow2 = to_pow2(h);
	txt->width *= wpow2 / (double)w;
	txt->height *= hpow2 / (double)h;

	/* Creating real rendering surface */
	cairo_destroy(cr);
	unsigned char *data = malloc(wpow2*hpow2<<2);
	canvas = cairo_image_surface_create_for_data(data, CAIRO_FORMAT_ARGB32, 
						     wpow2, hpow2, wpow2<<2);
	cr = cairo_create(canvas);

	/* Clearing surface */
	cairo_set_operator(cr, CAIRO_OPERATOR_CLEAR);
	cairo_paint(cr);

	/* Rendering font */
	cairo_set_operator(cr, CAIRO_OPERATOR_OVER);
	cairo_set_source_rgb(cr, 1.0, 1.0, 1.0);
	/* Centering */
	cairo_translate(cr, (wpow2-w)/2.0, (hpow2-h)/2.0);

	pango_cairo_update_layout(cr, layout);
	pango_cairo_show_layout(cr, layout);

	/* Over with font rendering */
	g_object_unref(layout);
	cairo_destroy(cr);
	cairo_surface_destroy(canvas);

	glGenTextures(1, &txt->tid);
	glBindTexture(GL_TEXTURE_2D, txt->tid);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	load_raw_mem(data, GL_TEXTURE_2D, wpow2, hpow2, 0, GL_BGRA, 
		     GL_UNSIGNED_BYTE);

	/* Over with the data */
	glBindTexture(GL_TEXTURE_2D, 0);

	return 0;
#else
	return -1;
#endif
}

/* Text rendering */
static void text_rendergeom(struct text *txt)
{
	/* No rendering on a zero length string */
	if (txt->max_extent == 0)
		return;

	/* Need back to front */
	if (!render_ctx.btf_rendering) {
		/* Getting depth */
		manager_back_to_front_register(0);
		return;
	}

	/* Scaling */
	glPushMatrix();

	if (txt->font)
		glScaled(txt->font->size*txt->width, 
			 txt->font->size*txt->height, 1);
	else
		glScaled(txt->width, txt->height, 1);

	/* Compress when max extent is crossed */
	if (txt->max_extent > 0 && txt->max_length > txt->max_extent)
		glScaled(txt->max_extent / txt->max_length , 1, 1);

	/* Removing text background */
	glPushAttrib(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glDepthMask(GL_FALSE);

	/* Putting the result on a surface */
	glBindTexture(GL_TEXTURE_2D, txt->tid);
	glBegin(GL_QUADS);
		glNormal3d(0, 0, 1);
		glTexCoord2d(0, 0);
		glVertex2d(-0.5, -0.5);

		glTexCoord2d(1, 0);
		glVertex2d(0.5, -0.5);
		
		glTexCoord2d(1, 1);
		glVertex2d(0.5, 0.5);

		glTexCoord2d(0, 1);
		glVertex2d(-0.5, 0.5);
	glEnd();
	glBindTexture(GL_TEXTURE_2D, 0);
	glPopAttrib();

	glPopMatrix();
}

/* Text rendering object destructor */
static void text_destroy(struct text *txt)
{
	if (txt->tid)
		glDeleteTextures(1, &txt->tid);

	if (txt->font)
		obj_destroy((struct obj*)txt->font);
}

/* Font initialization */
static void font_init(struct font *ft)
{
	/* X3D Specification 15.4.1 */
	ft->justify[1] = FONT_JUSTIFY_BEGIN;
	ft->size = 1;
	ft->spacing = 1;
}

/* Font destructor */
static void font_destroy(struct font *ft)
{
#if HAVE_PANGOCAIRO
	if (ft->pango_ft)
		pango_font_description_free(ft->pango_ft);
#endif
}

/* Font rendering setup */
int font_setup(struct font *ft)
{
#if HAVE_PANGOCAIRO
	/* Creating font description */
	ft->pango_ft = pango_font_description_new();

	/* Setting family */
	if (ft->families) {
		char *fams = families_to_pango(ft->families, ft->num_family);
		pango_font_description_set_family(ft->pango_ft, fams);
		free(fams);
	} else
		pango_font_description_set_family_static(ft->pango_ft,
							 default_pango_family);

	/* Settings style */
	switch (ft->style) {
		case FONT_STYLE_BOLD:
			pango_font_description_set_weight(ft->pango_ft,
							  PANGO_WEIGHT_BOLD);
			break;
		case FONT_STYLE_ITALIC:
			pango_font_description_set_style(ft->pango_ft,
							 PANGO_STYLE_ITALIC);
			break;

		case FONT_STYLE_BOLDITALIC:
			pango_font_description_set_weight(ft->pango_ft,
							  PANGO_WEIGHT_BOLD);
			pango_font_description_set_style(ft->pango_ft,
							 PANGO_STYLE_ITALIC);
			break;
	}

	/* Identifying language */
	ft->pango_lang = pango_language_from_string(ft->lang);

	/* Setting size */
	pango_font_description_set_size(ft->pango_ft, FONT_RES*PANGO_SCALE);

	return 0;
#else
	return -1;
#endif
}

/* Text dynamic table setup */
static void text_dyn_setup(struct text_dynt *dynt)
{
	((struct geom_dynt*)dynt)->setup = (geom_setup_func)text_setup;
	((struct geom_dynt*)dynt)->rendergeom = (geom_rendergeom_func)
			text_rendergeom;
}

/* Text component */
const struct obj_comp text_comp = {
	.init = (init_func)text_init,
	.destroy = (init_func)text_destroy,
	.dyn_setup = (dyn_setup_func)text_dyn_setup
};

/* Font compoment */
const struct obj_comp font_comp = {
	.init = (init_func)font_init,
	.destroy = (init_func)font_destroy
};

/* Text dynamic functions */
struct text_dynt text_dynt;

