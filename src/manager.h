#ifndef MANAGER_H
#define MANAGER_H

#include "jessica.h"

#include <gtk/gtkgl.h>

/* struct timespec */
#include <time.h>

#include "scene.h"
#include "stack.h"
#include "fifo.h"

struct output_context {
	GdkGLConfig	*glconfig;
	GdkGLDrawable	*win;
	GdkGLContext	*ctx;
	int		npot;		/* Non power of two textures support */
} extern out_ctx;

struct render_context {
	int				btf_rendering;
	struct timespec			time;		/* Render time */
	STACK(struct renderable*)	hierarchy;
	FIFO(struct event*)		events;
} extern render_ctx;

extern struct scene *scene;

int manager_load_file(const char *filename);

/* Manager setup, to be run _before_ the main loop is started */
int manager_setup(void);

/* Dirty scene notification */
void manager_need_update(void);

/* Back to front rendering needs in a rendering pass */
void manager_back_to_front_register(coord_t depth);

#endif	/* MANAGER_H */
