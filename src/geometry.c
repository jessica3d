#include "jessica.h"
#include "geometry.h"
#include "rendering.h"

/* Geometry start of rendering */
void geom_begin(struct geom *gm)
{
	geom_setup(gm);

	if (gm->cull)
		glCullFace(GL_BACK);
}

void geom_end(struct geom *gm)
{
	if (gm->cull)
		glCullFace(GL_NONE);
}

const struct obj_comp geom_comp;

