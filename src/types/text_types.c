#include "text_types.h"

const struct obj_type text_type = {
	.name		= "Text",
	.obj_size	= sizeof(struct text),
	.num_comp	= sizeof((const void*[]){ TEXT_COMP })/sizeof(void*),
	.comp		= { TEXT_COMP, NULL },
	.dynt		= &text_dynt
};

const struct obj_type font_type = {
	.name		= "Font",
	.obj_size	= sizeof(struct font),
	.num_comp	= sizeof((const void*[]){ FONT_COMP })/sizeof(void*),
	.comp		= { FONT_COMP, NULL }
};

