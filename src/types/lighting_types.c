#include "lighting_types.h"

const struct obj_type dirlight_type = {
	.name		= "Dirlight",
	.obj_size	= sizeof(struct dirlight),
	.num_comp	= sizeof((const void*[]){ DIRLIGHT_COMP }) /
				sizeof(void*),
	.comp		= { DIRLIGHT_COMP, NULL },
	.dynt		= &dirlight_dynt
};

const struct obj_type spotlight_type = {
	.name		= "Spotlight",
	.obj_size	= sizeof(struct spotlight),
	.num_comp	= sizeof((const void*[]){ SPOTLIGHT_COMP }) /
				sizeof(void*),
	.comp		= { SPOTLIGHT_COMP, NULL },
	.dynt		= &spotlight_dynt
};

const struct obj_type omnilight_type = {
	.name		= "Omnilight",
	.obj_size	= sizeof(struct omnilight),
	.num_comp	= sizeof((const void*[]){ OMNILIGHT_COMP }) / 
				sizeof(void*),
	.comp		= { OMNILIGHT_COMP, NULL },
	.dynt		= &omnilight_dynt
};


