#include "geometry_types.h"

const struct obj_type box_type = {
	.name		= "Box",
	.obj_size	= sizeof(struct box),
	.num_comp	= sizeof((const void*[]){ BOX_COMP })/sizeof(void*),
	.comp		= { BOX_COMP, NULL },
	.dynt		= &box_dynt
};

const struct obj_type cone_type = {
	.name		= "Cone",
	.obj_size	= sizeof(struct cone),
	.num_comp	= sizeof((const void*[]){ CONE_COMP })/sizeof(void*),
	.comp		= { CONE_COMP, NULL },
	.dynt		= &cone_dynt
};

const struct obj_type cylinder_type = {
	.name		= "Cylinder",
	.obj_size	= sizeof(struct cylinder),
	.num_comp	= sizeof((const void*[]){ CYLINDER_COMP })/
				sizeof(void*),
	.comp		= { CYLINDER_COMP, NULL },
	.dynt		= &cylinder_dynt
};

const struct obj_type sphere_type = {
	.name		= "Spehre",
	.obj_size	= sizeof(struct sphere),
	.num_comp	= sizeof((const void*[]){ SPHERE_COMP })/sizeof(void*),
	.comp		= { SPHERE_COMP, NULL },
	.dynt		= &sphere_dynt
};

const struct obj_type mesh_type = {
	.name		= "Mesh",
	.obj_size	= sizeof(struct mesh),
	.num_comp	= sizeof((const void*[]){ MESH_COMP })/sizeof(void*),
	.comp		= { MESH_COMP, NULL },
	.dynt		= &mesh_dynt
};



