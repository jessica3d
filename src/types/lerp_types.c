#include "lerp_types.h"

const struct obj_type vec3_lerp_type = {
	.name		= "Coordinate Interpolator",
	.obj_size	= sizeof(struct vec3_lerp),
	.num_comp	= sizeof((const void*[]){ VEC3_LERP_COMP }) /
				sizeof(void*),
	.comp		= { VEC3_LERP_COMP, NULL }
};

const struct obj_type vec2_lerp_type = {
	.name		= "2D Coordinate Interpolator",
	.obj_size	= sizeof(struct vec2_lerp),
	.num_comp	= sizeof((const void*[]){ VEC2_LERP_COMP }) /
				sizeof(void*),
	.comp		= { VEC2_LERP_COMP, NULL }
};

const struct obj_type quat_lerp_type = {
	.name		= "Orientation Interpolator",
	.obj_size	= sizeof(struct quat_lerp),
	.num_comp	= sizeof((const void*[]){ QUAT_LERP_COMP }) /
				sizeof(void*),
	.comp		= { QUAT_LERP_COMP, NULL },
	.dynt		= &quat_lerp_dynt
};

const struct obj_type normal_lerp_type = {
	.name		= "Normal Interpolator",
	.obj_size	= sizeof(struct normal_lerp),
	.num_comp	= sizeof((const void*[]){ NORMAL_LERP_COMP }) /
				sizeof(void*),
	.comp		= { NORMAL_LERP_COMP, NULL }
};

const struct obj_type scalar_lerp_type = {
	.name		= "Scalar Interpolator",
	.obj_size	= sizeof(struct scalar_lerp),
	.num_comp	= sizeof((const void*[]){ SCALAR_LERP_COMP }) /
				sizeof(void*),
	.comp		= { SCALAR_LERP_COMP, NULL }
};

