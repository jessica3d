#ifndef SENSOR_TYPES_H
#define SENSOR_TYPES_H

#include "model_types.h"
#include "sensor.h"

#define SENSOR_COMP	BASE_COMP,\
			&sensor_comp

#define TIME_SENSOR_COMP	SENSOR_COMP,\
				&time_sensor_comp

#endif	/* SENSOR_TYPES_H */
