#ifndef LERP_TYPES_H
#define LERP_TYPES_H

#include "model_types.h"
#include "lerp.h"

#define LERP_COMP	BASE_COMP,\
			&lerp_comp

#define QUAT_LERP_COMP	LERP_COMP,\
			&quat_lerp_comp

#define VEC2_LERP_COMP	LERP_COMP,\
			&vec2_lerp_comp

#define VEC3_LERP_COMP	LERP_COMP,\
			&vec3_lerp_comp

#define SCALAR_LERP_COMP	LERP_COMP,\
				&scalar_lerp_comp

#define NORMAL_LERP_COMP	LERP_COMP,\
				&normal_lerp_comp

#endif	/* LERP_TYPES_H */
