#include "sensor_types.h"

const struct obj_type time_sensor_type = {
	.name		= "TimeSensor",
	.obj_size	= sizeof(struct time_sensor),
	.num_comp	= sizeof((const void*[]){ TIME_SENSOR_COMP }) /
				sizeof(void*),
	.comp		= { TIME_SENSOR_COMP, NULL },
	.dynt		= &time_sensor_dynt
};

