#ifndef SHADING_TYPES_H
#define SHADING_TYPES_H

#include "model_types.h"
#include "shading.h"

#define SHADING_COMP	BASE_COMP,\
			&shading_comp

#define MATERIAL_COMP	BASE_COMP,\
			&material_comp

#define LINING_COMP	BASE_COMP,\
			&lining_comp

#define FILLING_COMP	BASE_COMP,\
			&filling_comp


#endif	/* SHADING_TYPES_H */
