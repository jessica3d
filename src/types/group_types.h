#ifndef GROUP_TYPES_H
#define GROUP_TYPES_H

#include "group.h"
#include "model_types.h"

#define RENDERABLE_COMP	BASE_COMP,\
			&renderable_comp

#define GROUP_COMP	RENDERABLE_COMP,\
			&group_comp

#define SHAPE_COMP	RENDERABLE_COMP,\
			&shape_comp

#endif	/* GROUP_TYPES_H */
