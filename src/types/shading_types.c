#include "shading_types.h"

const struct obj_type shading_type = {
	.name		= "Shading",
	.obj_size	= sizeof(struct shading),
	.num_comp	= sizeof((const void*[]){ SHADING_COMP })/sizeof(void*),
	.comp		= { SHADING_COMP, NULL }
};

const struct obj_type material_type = {
	.name		= "Material",
	.obj_size	= sizeof(struct material),
	.num_comp	= sizeof((const void*[]){ MATERIAL_COMP }) /
				sizeof(void*),
	.comp		= { MATERIAL_COMP, NULL }
};

const struct obj_type hatch_type = {
	.name		= "Filling",
	.obj_size	= sizeof(struct filling),
	.num_comp	= sizeof((const void*[]){ FILLING_COMP }) /
				sizeof(void*),
	.comp		= { FILLING_COMP, NULL }
};

const struct obj_type lining_type = {
	.name		= "Lining",
	.obj_size	= sizeof(struct lining),
	.num_comp	= sizeof((const void*[]){ LINING_COMP }) /
				sizeof(void*),
	.comp		= { LINING_COMP, NULL }
};


