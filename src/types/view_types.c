#include "view_types.h"

const struct obj_type viewpoint_type = {
	.name		= "Viewpoint",
	.obj_size	= sizeof(struct viewpoint),
	.num_comp	= sizeof((const void*[]){ VIEWPOINT_COMP }) /
				sizeof(void*),
	.comp		= { VIEWPOINT_COMP, NULL }
};

