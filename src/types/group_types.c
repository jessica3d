#include "group_types.h"

const struct obj_type group_type = {
	.name		= "Group",
	.obj_size	= sizeof(struct group),
	.num_comp	= sizeof((const void*[]){ GROUP_COMP })/sizeof(void*),
	.comp		= { GROUP_COMP, NULL },
	.dynt		= &group_dynt
};

const struct obj_type shape_type = {
	.name		= "Shape",
	.obj_size	= sizeof(struct shape),
	.num_comp	= sizeof((const void*[]){ SHAPE_COMP })/sizeof(void*),
	.comp		= { SHAPE_COMP, NULL },
	.dynt		= &shape_dynt
};

