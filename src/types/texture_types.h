#ifndef TEXTURE_TYPES_H
#define TEXTURE_TYPES_H

#include "model_types.h"
#include "texture.h"

#define TEXTURE_COMP	BASE_COMP,\
			&texture_comp

#define TEXTURE2D_COMP	TEXTURE_COMP,\
			&texture2d_comp


#endif	/* TEXTURE_TYPES_H */
