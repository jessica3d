#ifndef GEOMETRY_TYPES_H
#define GEOMETRY_TYPES_H

#include "model_types.h"
#include "geometry.h"

#define GEOM_COMP	BASE_COMP,\
			&geom_comp

#define BOX_COMP	GEOM_COMP,\
			&box_comp

#define CONE_COMP	GEOM_COMP,\
			&cone_comp

#define CYLINDER_COMP	GEOM_COMP,\
			&cylinder_comp

#define SPHERE_COMP	GEOM_COMP,\
			&sphere_comp

#define MESH_COMP	GEOM_COMP,\
			&mesh_comp


#endif	/* GEOMETRY_TYPES_H */
