#include "texture_types.h"

const struct obj_type texture2d_type = {
	.name		= "Texture2D",
	.obj_size	= sizeof(struct texture2d),
	.num_comp	= sizeof((const void*[]){ TEXTURE2D_COMP }) /
				sizeof(void*),
	.comp		= { TEXTURE2D_COMP, NULL },
	.dynt		= &texture2d_dynt
};

