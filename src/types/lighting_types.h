#ifndef LIGHTING_TYPES_H
#define LIGHTING_TYPES_H

#include "lighting.h"
#include "model_types.h"

#define LIGHT_COMP	BASE_COMP,\
			&light_comp

#define DIRLIGHT_COMP	LIGHT_COMP,\
			&dirlight_comp

#define SPOTLIGHT_COMP	LIGHT_COMP,\
			&spotlight_comp

#define OMNILIGHT_COMP	LIGHT_COMP,\
			&omnilight_comp

#endif	/* LIGHTINGM_TYPES_H */
