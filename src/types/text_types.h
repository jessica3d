#ifndef TEXT_TYPES_H
#define TEXT_TYPES_H

#include "geometry_types.h"
#include "text.h"

#define TEXT_COMP	GEOM_COMP,\
			&text_comp

#define FONT_COMP	BASE_COMP,\
			&font_comp

#endif	/* TEXT_TYPES_H */
