#include <gtk/gtk.h>

#include <stdio.h>

#include "jessica.h"
#include "manager.h"
#include "ui.h"
#include "model.h"
#include "network.h"

#define RENDER_MIN_SIZE		400

/* Application startup */
int main(int argc, char **argv)
{
	GtkWidget *render_container;
	int res;
	
	// debug
	setlinebuf(stdout);
	setlinebuf(stderr);

	/* Initialisating the object model */
	model_init();

	/* GTK */
	gtk_init(&argc, &argv);
	/* Rendering area */
	gtk_gl_init(&argc, &argv);

	/* Network initialisation */
	res = network_init();
	if (res < 0) {
		fprintf(stderr, "Error loading network component: %s\n",
			get_error(res));
		return 1;
	}

	/* GL configuration */
	out_ctx.glconfig = gdk_gl_config_new_by_mode(GDK_GL_MODE_RGB | 
						     GDK_GL_MODE_DOUBLE |
						     GDK_GL_MODE_DEPTH);

	/* Output window */
	render_win = gtk_drawing_area_new();
	if (!render_win) {
		 fputs("Can't create output widget\n", stderr);
		 return 1;
	}
	
	/* Configuring for GL use */
	if (!gtk_widget_set_gl_capability(render_win, out_ctx.glconfig, NULL, 
					  TRUE, GDK_GL_RGBA_TYPE)) {
		fputs("Can't set rendering area to use GL\n", stderr);
		return 1;
	}

	/* Setting rendering area for default size */
	gtk_widget_set_size_request(render_win, RENDER_MIN_SIZE, 
				    RENDER_MIN_SIZE);

	/* Creation of the application window */
	render_container = create_app_win();
	if (!render_container)
		return 1;
	
	/* Ready to roll */
	gtk_container_add(GTK_CONTAINER(render_container), render_win);
	gtk_widget_show(render_win);
	gtk_widget_show(app_win);
	out_ctx.win = gtk_widget_get_gl_drawable(render_win);
	out_ctx.ctx = gtk_widget_get_gl_context(render_win);

	/* Manager setup */
	if (manager_setup() < 0)
		return 1;

	/* Load file specified on command line */
	if (argc > 1) {
		res = manager_load_file(argv[1]);
		if (res < 0) {
			fprintf(stderr, "%s: %s\n", argv[1], get_error(res));
			return 1;
		}
	}

	/* Everything's set up */
	gtk_main();

	return 0;
}

