#ifndef RENDERING_H
#define RENDERING_H

#include <GL/gl.h>
#include <GL/glext.h>
#include <GL/glu.h>
#include "ext/ext_binder.h"

/* GL status print (for debug) */
void pglerror(const char *msg);

/* Integer log_2 */
static inline int log2i(unsigned v)
{
	int i;
	for (i = 0; v; i++)
		v >>= 1;
	return --i;
}

/* Ceil power of 2 calc */
static inline int to_pow2(unsigned v)
{
	int lg = log2i(v);
	if ((1<<lg) == v)
		return v;
	return 1<<(lg+1);
}

#endif	/* RENDERING_H */
