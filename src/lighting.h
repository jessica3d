#ifndef LIGHTING_H
#define LIGHTING_H

#include "primitives.h"
#include "shading.h"

struct light {
	struct obj	obj;
	coeff_t		ambient;	/* Ambient coeff */
	coeff_t		intensity;	/* Intensity coeff */
	color4		color;		/* Beam color */
	int		local;		/* Enabled for the hierarchy node */
	int		enabled;	/* Active light */
};

struct light_dynt {
	void	(*setup)(struct light* lgt, int num);
	void	(*update)(struct light* lgt, int num);
};

typedef void	(*light_setup_func)(struct light* lgt, int num);
typedef void	(*light_update_func)(struct light* lgt, int num);

static inline void light_setup(struct light* lgt, int num)
{ ((struct light_dynt*)((struct obj*)lgt)->type->dynt)->setup(lgt, num); }

static inline void light_update(struct light* lgt, int num)
{ ((struct light_dynt*)((struct obj*)lgt)->type->dynt)->update(lgt, num); }

/* Directional light, positioned at infinity */
struct dirlight {
	struct light	light;
	vec3		dir;		/* Light direction source vect */
};

struct dirlight_dynt {
	struct light_dynt	light_dynt;
} extern dirlight_dynt;

/* Spotlight, casting a circle beam */
struct spotlight {
	struct light	light;
	vec3		pos;
	vec3		dir;
	angle_t		beam_width;
	angle_t		cutoff;
	double		radius;
	coeff_t		attenuation[3];
};

struct spotlight_dynt {
	struct light_dynt	light_dynt;
} extern spotlight_dynt;


/* Point light, of the size of a pinhole */
struct omnilight {
	struct light	light;
	vec3		pos;
	coeff_t		attenuation[3];
};

struct omnilight_dynt {
	struct light_dynt	light_dynt;
} extern omnilight_dynt;


extern const struct obj_type dirlight_type;
extern const struct obj_type spotlight_type;
extern const struct obj_type omnilight_type;

extern const struct obj_comp light_comp;
extern const struct obj_comp dirlight_comp;
extern const struct obj_comp spotlight_comp;
extern const struct obj_comp omnilight_comp;


#endif	/* LIGHTING_H */
