#ifndef PRIMITIVES_H
#define PRIMITIVES_H

#include <float.h>
#include <math.h>

typedef double	coord_t;
typedef double	angle_t;

#define COORD_EPSILON	DBL_EPSILON
#define ANGLE_EPSILON	DBL_EPSILON


/* Geometry manipulation structures */
typedef union {
	coord_t v[2];
	struct {
		coord_t	s;
		coord_t	t;
	} c;
} vec2;

typedef union {
	coord_t v[3];
	struct {
		coord_t	x;
		coord_t	y;
		coord_t	z;
	} c;
} vec3;

typedef union {
	coord_t v[4];
	struct {
		coord_t	x;
		coord_t	y;
		coord_t	z;
		coord_t	w;
	} c;
} vec4;

typedef struct {
	vec4	v[4];
} mat4;

/* Constants */
extern const vec3 vec3_zero;
#define VEC3_ZERO	{ 0, 0, 0 }
extern const vec3 vec3_i;
#define VEC3_I		{ 1, 0, 0 }
extern const vec3 vec3_j;
#define VEC3_J		{ 0, 1, 0 }
extern const vec3 vec3_k;
#define VEC3_K		{ 0, 0, 1 }

static inline vec3 vec3_c(coord_t x, coord_t y, coord_t z)
{ vec3 vec = { .v = { x, y, z } }; return vec; }
static inline vec3 vec3_v(coord_t v[])
{ vec3 vec = { .v = { v[0], v[1], v[2] } }; return vec; }

/* Operations */

/* Term to term operations */
static inline vec3 vec3_sub(const vec3 a, const vec3 b)
{ return vec3_c(a.c.x-b.c.x, a.c.y-b.c.y, a.c.z-b.c.z); }

static inline vec3 vec3_add(const vec3 a, const vec3 b)
{ return vec3_c(a.c.x+b.c.x, a.c.y+b.c.y, a.c.z+b.c.z); }

static inline vec3 vec3_mult(const vec3 a, const vec3 b)
{ return vec3_c(a.c.x*b.c.x, a.c.y*b.c.y, a.c.z*b.c.z); }

/* Dot product */
static inline double vec3_dot(const vec3 a, const vec3 b)
{ return a.c.x*b.c.x + a.c.y*b.c.y + a.c.z*b.c.z;}

/* Cross product */
static inline vec3 vec3_cross(const vec3 a, const vec3 b)
{ return vec3_c(a.v[1]*b.v[2] - a.v[2]*b.v[1],
		a.v[2]*b.v[0] - a.v[0]*b.v[2],
		a.v[0]*b.v[1] - a.v[1]*b.v[0]); }

/* Opposite (negate) */
static inline vec3 vec3_neg(const vec3 a)
{ return vec3_c(-a.c.x, -a.c.y, -a.c.z); } 

/* Scalar product */
static inline vec3 vec3_scale(const vec3 a, double fact)
{ return vec3_c(a.c.x*fact, a.c.y*fact, a.c.z*fact); }

/* Euclidean Norm */
static inline double vec3_norm(const vec3 a)
{ return sqrt(vec3_dot(a, a)); }

/* Normalization */
static inline vec3 vec3_normalize(const vec3 a)
{ return vec3_scale(a, 1/vec3_norm(a)); }

/* Operations to self */
static inline void vec3_scale_self(vec3* a, double fact)
{ for (int i = 0; i < 3; i++) a->v[i] *= fact; }

static inline void vec3_add_self(vec3* a, const vec3 b)
{ for (int i = 0; i < 3; i++) a->v[i] += b.v[i]; }

static inline void vec3_sub_self(vec3* a, const vec3 b)
{ for (int i = 0; i < 3; i++) a->v[i] -= b.v[i]; }

static inline void vec3_mult_self(vec3* a, const vec3 b)
{ for (int i = 0; i < 3; i++) a->v[i] *= b.v[i]; }

extern const mat4 mat4_id;
extern const mat4 mat4_zero;

/* Matrix operations */
mat4 mat4_mult(const mat4 *a, const mat4 *b);
vec3 mat4_mult_vec3(const mat4 *m, const vec3 *v);

/* Tranform matrix */
mat4 mat4_scale(const vec3 *scl);
mat4 mat4_transl(const vec3 *disp);
/* Rotations... use quaternions instead! */
mat4 mat4_rot(angle_t angle, const vec3 *axis);
mat4 mat4_rotx(angle_t angle);
mat4 mat4_roty(angle_t angle);
mat4 mat4_rotz(angle_t angle);

/* Quaternions */
typedef union {
	coord_t	v[4];
	struct {
		coord_t	x;
		coord_t	y;
		coord_t	z;
		coord_t	w;
	} c;
} quat;

static inline quat quat_v(coord_t v[])
{ quat qt = { .v = { v[0], v[1], v[2], v[3] } }; return qt; }

static inline quat quat_c(coord_t x, coord_t y, coord_t z, coord_t a)
{ quat qt = { .v = { x, y, z, a } }; return qt; }

/* Basic vector operation */
static inline quat quat_add(const quat a, const quat b)
{ return quat_c(a.c.x+b.c.x, a.c.y+b.c.y, a.c.z+b.c.z, a.c.w+b.c.w); }

static inline quat quat_sub(const quat a, const quat b)
{ return quat_c(a.c.x-b.c.x, a.c.y-b.c.y, a.c.z-b.c.z, a.c.w-b.c.w); }

static inline quat quat_scale(const quat a, double f)
{ return quat_c(a.c.x*f, a.c.y*f, a.c.z*f, a.c.w*f); }

static inline quat quat_neg(const quat a)
{ return quat_c(-a.c.x, -a.c.y, -a.c.z, -a.c.w); }

/* Quaterninon conjugate */
static inline quat quat_conj(const quat a)
{ return quat_c(-a.c.x, -a.c.y, -a.c.z, a.c.w); }

/* Quaternion dot product */
static inline double quat_dot(const quat a, const quat b)
{ return a.c.x*b.c.x + a.c.y*b.c.y + a.c.z*b.c.z + a.c.w*b.c.w; }

/* Quaternion reciprocal */
static inline quat quat_inv(const quat a)
{ return quat_conj(a); /* Unit quaternion inv is its conjugate */ }

/* Quaternion norm (absolute value) */
static inline double quat_norm(const quat a)
{ return sqrt(quat_dot(a, a)); }

/* Quaternion normalization */
static inline quat quat_normalize(const quat a)
{ return quat_scale(a, 1/quat_norm(a)); }

/* Quaternion Grassman product */
static inline quat quat_grassman(const quat a, const quat b)
{
	return quat_c(a.c.x*b.c.w + a.c.w*b.c.x + a.c.y*b.c.z - a.c.z*b.c.y, 
		      a.c.w*b.c.y - a.c.x*b.c.z + a.c.y*b.c.w + a.c.z*b.c.x,
		      a.c.w*b.c.z + a.c.x*b.c.y - a.c.y*b.c.x + a.c.z*b.c.w,
		      a.c.w*b.c.w - a.c.x*b.c.x - a.c.y*b.c.y- a.c.z*b.c.z);
}

/* Creation of a quaternion from an axis and an angle */
static inline quat quat_rot(vec3 a, angle_t an)
{ double c = cos(an/2); double s = sin(an/2);
  return quat_normalize(quat_c(a.c.x*s, a.c.y*s, a.c.z*s, c)); }

/* Axix and angle from a quaternion */
static inline angle_t quat_angle(const quat a)
{ if (fabs(a.c.w) < COORD_EPSILON) return 0;
  return 2*acos(a.c.w); }

static inline vec3 quat_axis(const quat a)
{ double m = 1-a.c.w*a.c.w;
  if (fabs(m) < COORD_EPSILON) return vec3_j;
  double sq = sqrt(m);
  return vec3_c(a.c.x/sq, a.c.y/sq, a.c.z/sq); }

/* Quaternion spherical linear interpolation */
quat quat_slerp(const quat *a, const quat *b, double t);

/* Product of a quaternion by a vector */
vec3 quat_mult_vec3(const quat *a, const vec3 *v);

/* Debug routines */
void printvec3(const vec3 v, const char *);
void printquat(const quat v, const char *);
void printrot(const quat v, const char *);
void printmat4(const mat4 *v, const char *);

#endif	/* PRIMITIVES_H */
