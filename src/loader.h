#ifndef LOADER_H
#define LOADER_H

#include "primitives.h"
#include "color.h"
#include "sensor.h"

/* Filling of double type values from a string, returning the number
 * of elements parsed, modifying the values to the current parsing position */
int loader_fill_double(double *dest, const char **values, int num);

int loader_fill_float(float *dest, const char **values, int num);

int loader_fill_int(int *dest, const char **values, int num);

int loader_fill_rot(quat *qt, const char **values);

int loader_fill_bool(int *dest, const char **values, int num);

/* Filling of defined type */
static inline int loader_fill_coord(coord_t *dest, const char **values, 
				    int num)
{ return loader_fill_double((double*)dest, values, num); }

static inline int loader_fill_angle(angle_t *dest, const char **values, 
				    int num)
{ return loader_fill_double((double*)dest, values, num); }

static inline int loader_fill_timens(timens_t *dest, const char **values, 
				     int num)
{ return loader_fill_double((double*)dest, values, num); }

static inline int loader_fill_color(color_t *dest, const char **values, int num)
{ return loader_fill_float((float*)dest, values, num); }

static inline int loader_fill_color3(color3 *dest, const char **values, int num)
{ return loader_fill_color(dest->v, values, 3)/3; }

static inline int loader_fill_vec3(vec3 *dest, const char **values)
{ return loader_fill_coord(dest->v, values, 3)/3; }

static inline int loader_fill_vec4(vec4 *dest, const char **values)
{ return loader_fill_coord(dest->v, values, 4)/4; }

/* Filling of an index array, returning the number of indexes */
int loader_fill_int_array(int **array, const char *values, int pack_num);

/* Filling of a double array, returning the number of values */
int loader_fill_double_array(double **array, const char *values, int pack_num);

/* Vector array */
int loader_fill_vec3_array(vec3 **array, const char *values);

/* Rotation/Quaternion array */
int loader_fill_rot_array(quat **array, const char *values);

/* String filling */
int loader_fill_string(char **dest, const char **values, int num);

/* URL list filling */
int loader_fill_string_array(char ***dest, const char *values);

#endif	/* LOADER_H */
