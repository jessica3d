#include <config.h>

/* malloc & friends ... */
#include <stdlib.h>

/* Typedefs */
#include <stdint.h>

/* fprintf(3) */
#include <stdio.h>

#if HAVE_LIBPNG
#include <png.h>
#endif
#if HAVE_LIBJPEG
#include <jpeglib.h>
#include <jerror.h>
#endif

/* open(2) */
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

/* close(2 */
#include <unistd.h>

/* mmap(2) */
#include <sys/mman.h>


#include "rendering.h"
#include "manager.h"


#if HAVE_LIBPNG
/* Setting of ping image in the gl texture */
static int set_png(png_structp png, png_infop info, GLuint target, 
		   const char *filename)
{
	png_uint_32 width, height;
	int bit_depth, color_type;
	int row_bytes;
	int fill_bytes = 0;		/* Power of two filling on row */
	int fill_w = 0;			
	int fill_rows = 0;		/* Height power of two filling */
	GLenum gl_fmt, gl_type;
	char *buffer;
	
	png_get_IHDR(png, info, &width, &height, &bit_depth, &color_type, 
		     NULL, NULL, NULL);

	/* Only two bit depth can be used, as expand is done by the loader */
	if (bit_depth == 16)
		gl_type = GL_UNSIGNED_SHORT;
	else	/* 1 to 8 bits samples */
		gl_type = GL_UNSIGNED_BYTE;

	/* Mapping PNG data format to GL */
	switch (color_type) {
		case PNG_COLOR_TYPE_RGB_ALPHA:
			gl_fmt = GL_RGBA;
			break;
		case PNG_COLOR_TYPE_RGB:
			gl_fmt = GL_RGB;
			break;
		case PNG_COLOR_TYPE_GRAY:
			gl_fmt = GL_LUMINANCE;
			break;
		case PNG_COLOR_TYPE_GRAY_ALPHA:
			gl_fmt = GL_LUMINANCE_ALPHA;
			break;
		default:
			/* Unsupported are the masks */
			fprintf(stderr, "Unsupported PNG %s file\n", 
				filename); 
			return -1;
	}

	row_bytes = png_get_rowbytes(png, info);

	/* Setting power of two status */
	if (!out_ctx.npot) {
		int pow2w = to_pow2(width);
		int pow2h = to_pow2(height);
		fill_w = pow2w - width;
		fill_bytes = fill_w*(row_bytes/width);
		fill_rows = pow2h - height;
	}

	/* Allocating required memory */
	buffer = (char*)malloc((height+fill_rows)*(row_bytes+fill_bytes));
	png_bytep rows[height];
	rows[height-1] = (png_bytep)buffer;
	memset(&buffer[row_bytes], 0, fill_bytes);

	int i, rowpx = row_bytes+fill_bytes;
	for (i = height-2; i >= 0; i--) {
		rows[i] = rows[i+1]+row_bytes+fill_bytes;
		memset(&buffer[rowpx+row_bytes], 0, fill_bytes);
		rowpx += row_bytes + fill_bytes;
	}

	memset(&buffer[height*(row_bytes+fill_bytes)], 0, 
	       fill_rows*(row_bytes+fill_bytes));

	/* Finaly reading that PNG */
	png_read_image(png, rows);

	/* Can now send this to GL */
	glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
	glTexImage2D(target, 0, gl_fmt, width+fill_w, height+fill_rows, 0, 
		     gl_fmt, gl_type, buffer);
	free(buffer);

	return 0;
}


#endif

#if HAVE_LIBPNG
struct png_src {
	const void	*data;
	size_t		size;
	size_t		pos;
};

/* PNG Input callback */
static void png_read_mem_data(png_structp png, png_bytep data, png_size_t len)
{
	struct png_src* src = (struct png_src*)png_get_io_ptr(png);

	if (src->pos + len > src->size)
		longjmp(png_jmpbuf(png), 1);

	memcpy(data, (png_bytep)src->data + src->pos, len);
	src->pos += len;
}

/* Comme PNG image loader */
static int load_png(png_structp png, GLuint target, int *width, int *height)
{
	png_infop info;
	png_uint_32 w, h;
	int bit_depth, color_type;

	/* Image info */
	info = png_create_info_struct(png);
	if (!info)
		return -1;

	/* Setting error handling */
	if ((setjmp(png_jmpbuf(png))))
		return -1;

	png_set_sig_bytes(png, 8);

	/* Need info about that image */
	png_read_info(png, info);
	png_get_IHDR(png, info, &w, &h, &bit_depth, &color_type, 
		     NULL, NULL, NULL);

	/* Transformations setting */
	/* Unpacking 1, 2 and 4 bits components  to bytes */
	if (bit_depth < 8)
		png_set_packing(png);

	/* Expanding paletted PNG to RGB */
	if (color_type == PNG_COLOR_TYPE_PALETTE)
		png_set_palette_to_rgb(png);

	/* Finaly set the full alpha value in alpha-masked PNG */
	if (png_get_valid(png, info, PNG_INFO_tRNS))
		png_set_tRNS_to_alpha(png);

	/* Transfer this to the bound GL texture */
	png_read_update_info(png, info);
	if (set_png(png, info, target, "") < 0)
		return -1;

	/* Sending info before quitting */
	if (width)
		*width = w;
	if (height)
		*height = h;

	return 0;
}
#endif

/* PNG image file reader */
int load_png_file(const char *filename, GLuint target, int *width, int *height)
{
#if HAVE_LIBPNG
	png_structp png;
	char header[8];
	int res;

	FILE *f = fopen(filename, "r");
	if (!f) {
		perror(filename);
		return -1;
	}

	/* Verifying if that's really a PNG file */
	if (fread(header, 8, 1, f) < 1) {
		if (ferror(f)) {
			/* Could be an error */
			perror(filename);
			goto err_close;
		}

		/* Or a small file (of some type?) of 8 bytes (?) */
		return 1;
	}

	if (png_sig_cmp((png_bytep)header, 0, 8)) {
		/* Doesn't seem to be PNG */
		fclose(f);
		return 1;
	}

	/* PNG reader hangle */
	png = png_create_read_struct(PNG_LIBPNG_VER_STRING, NULL, NULL, NULL);
	if (!png)
		goto err_close;

	/* Setting IO */
	png_init_io(png, f);

	res = load_png(png, target, width, height);

	fclose(f);
	png_destroy_read_struct(&png, NULL, NULL);

	return res;

err_close:
	fclose(f);
	return -1;
#else
	return 1;
#endif
}

/* PNG memory image reader */
int load_png_mem(const void *data, size_t size, GLuint target, 
		 int *width, int *height)
{
#if HAVE_LIBPNG
	png_structp png;
	int res;

	/* PNG header has at least 8 bytes */
	if (size < 8)
		return 1;

	if (png_sig_cmp((png_bytep)data, 0, 8))
		/* Doesn't seem to be PNG */
		return 1;

	struct png_src src = {
		.data = data,
		.size = size,
		.pos = 8
	};

	png = png_create_read_struct(PNG_LIBPNG_VER_STRING, NULL, NULL, NULL);
	if (!png)
		return -1;

	png_set_read_fn(png, &src, png_read_mem_data);

	res = load_png(png, target, width, height);
	png_destroy_read_struct(&png, NULL, NULL);

	return res;
#else
	return 1;
#endif
}

#if HAVE_LIBJPEG
/* JPEG load error manager */
struct err_mgr {
	struct jpeg_error_mgr err;
	jmp_buf jmpbuf;
};

/* JPEG read source manager */
struct source_mgr {
	struct jpeg_source_mgr	mgr;
	const void		*data;
	int			size;
};

/* JPEG error handler */
static void jpeg_err_exit(j_common_ptr cinfo)
{
	longjmp(((struct err_mgr*)cinfo->err)->jmpbuf, 1);
}

/* JPEG source init called before reading any data */
static void jpeg_init_mem_source(j_decompress_ptr cinfo)
{
	struct source_mgr* src = (struct source_mgr*)cinfo->src;
	/* All the data is available */
	src->mgr.next_input_byte = src->data;
	src->mgr.bytes_in_buffer = src->size;
}

/* Abord of operations */
static boolean jpeg_mem_abord(j_decompress_ptr cinfo)
{
	jpeg_abort_decompress(cinfo);
	return TRUE;
}

/* JPEG mem source read last notif */
static void jpeg_mem_noop(j_decompress_ptr cinfo)
{
	/* Nothing to do */
}

/* Common JPEG image loader */
static int load_jpeg(struct jpeg_decompress_struct *jpeg, GLuint target,
		     int *width, int *height)
{
	volatile int err_status = 1;
	int row_bytes;
	int fill_w = 0;
	int fill_bytes = 0;
	int fill_rows = 0;
	char *buffer;
	struct err_mgr err;

	jpeg->err = jpeg_std_error(&err.err);
	err.err.error_exit = jpeg_err_exit;
	if (setjmp(err.jmpbuf))
		return err_status;

	/* Verifying header for a valid JPEG */
	jpeg_read_header(jpeg, TRUE);
	
	/* This is a JPEG file, an error will now be fatal */
	err_status = -1;

	/* Setting output */
	jpeg->out_color_space = JCS_RGB;
	jpeg_start_decompress(jpeg);

	/* Allocating required memory */
	row_bytes = jpeg->output_width*3;
	if (!out_ctx.npot) {
		fill_w = to_pow2(jpeg->output_width)-jpeg->output_width;
		fill_rows = to_pow2(jpeg->output_height) - jpeg->output_height;
	}
	fill_bytes = (((jpeg->output_width+fill_w+1)*3) & (~3)) - row_bytes;

	buffer = (char*)malloc((jpeg->output_height+fill_rows)*
			       (row_bytes+fill_bytes));
	JSAMPROW rows[jpeg->output_height];
	rows[jpeg->output_height-1] = (JSAMPROW)buffer;
	memset(&buffer[row_bytes], 0, fill_bytes);

	int i, rowpx = row_bytes+fill_bytes;
	for (i = jpeg->output_height-2; i >= 0; i--) {
		rows[i] = rows[i+1]+row_bytes+fill_bytes;
		memset(&buffer[rowpx+row_bytes], 0, fill_bytes);
		rowpx += row_bytes+fill_bytes;
	}
	memset(&buffer[jpeg->output_height*(row_bytes+fill_bytes)], 0, 
	       fill_rows*(row_bytes+fill_bytes));

	/* And finally decompressing that file */
	while (jpeg->output_scanline < jpeg->output_height)
	    jpeg_read_scanlines(jpeg, rows+jpeg->output_scanline, 
			        jpeg->output_height-jpeg->output_scanline);

	jpeg_finish_decompress(jpeg);

	/* Sending data to GL */
	glPixelStorei(GL_UNPACK_ALIGNMENT, 4);
	glTexImage2D(target, 0, GL_RGB, jpeg->output_width+fill_w, 
		     jpeg->output_height+fill_rows, 0, 
		     GL_RGB, GL_UNSIGNED_BYTE, buffer);
	free(buffer);

	/* Setting dimensions */
	if (width)
		*width = jpeg->output_width;
	if (height)
		*height = jpeg->output_height;

	return 0;
}
#endif

/* In-Memory JPEG image loader */
int load_jpeg_mem(const void *data, size_t size, GLuint target, 
		  int *width, int *height)
{
#if HAVE_LIBJPEG
	int res;
	/* Using a custom source manager to read in mem */
	struct source_mgr src = {
		.mgr = {
			.init_source = jpeg_init_mem_source,
			.fill_input_buffer = jpeg_mem_abord,
			.skip_input_data = (void(*)(j_decompress_ptr cinfo, 
					   long num_bytes))jpeg_mem_noop,
			.resync_to_restart = jpeg_resync_to_restart,
			.term_source = jpeg_mem_noop
		},
		.data = data,
		.size = size
	};

	/* Needing a hangle */
	struct jpeg_decompress_struct jpeg;
	jpeg_create_decompress(&jpeg);

	jpeg.src = (struct jpeg_source_mgr*)&src;

	res = load_jpeg(&jpeg, target, width, height);

	jpeg_destroy_decompress(&jpeg);

	return res;
#else
	return 1;
#endif
}


/* JPEG file image loader */
int load_jpeg_file(const char *filename, GLuint target, int *width, int *height)
{
#if HAVE_LIBJPEG
	int res;
	struct jpeg_decompress_struct jpeg;

	/* Needing a hangle */
	jpeg_create_decompress(&jpeg);

	FILE *f = fopen(filename, "r");
	if (!f) {
		perror(filename);
		return -1;
	}

	/* Setting image source */
	jpeg_stdio_src(&jpeg, f);

	res = load_jpeg(&jpeg, target, width, height);

	jpeg_destroy_decompress(&jpeg);
	fclose(f);

	return res;
#else
	return 1;
#endif
}

/* Targa image reader 
 * Supports only unmapped RGB images */
int load_targa_file(const char *filename, GLuint target, 
		    int *width, int *height)
{
	struct stat st;
	uint8_t *data;
	int depth;
	int w, h;
	int format = 0;
	int gl_fmt = 0;
	int gl_type = 0;
	

	/* Opening file */
	int fd = open(filename, O_RDONLY);
	if (fd < 0) {
		perror(filename);
		return -1;
	}

	/* Header is at least 18 bytes */
	fstat(fd, &st);
	if (st.st_size < 18) {
		close(fd);
		return 1;
	}

	data = (uint8_t*)mmap(NULL, st.st_size, PROT_READ, MAP_SHARED, fd, 0);
	if (!data) {
		perror("mmap");
		goto err_close;
	}

	/* Dim specification */
	w = data[12] | ((int)data[13]) << 8;
	h = data[14] | ((int)data[15]) << 8;
	depth = data[16] >> 3;
	

	/* Now loading data */
	glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
	glPixelStorei(GL_UNPACK_LSB_FIRST, 1);
	switch(depth) {
		case 1:
			format = GL_LUMINANCE;
			gl_fmt = GL_LUMINANCE;
			gl_type = GL_UNSIGNED_BYTE;
			break;
		case 2:
			format = GL_BGRA;
			gl_fmt = GL_RGBA;
			gl_type = GL_UNSIGNED_SHORT_1_5_5_5_REV;
			break;
		case 3:
			format = GL_BGR;
			gl_fmt = GL_RGB;
			gl_type = GL_UNSIGNED_BYTE;
			break;
		case 4:
			format = GL_BGRA;
			gl_fmt = GL_RGBA;
			gl_type = GL_UNSIGNED_BYTE;
			break;
	}
	
	/* Loading image */
	glTexImage2D(target, 0, gl_fmt, w, h, 0, format, gl_type, data+18);

	/* No more need of that file */
	munmap(data, st.st_size);
	close(fd);

	if (width)
		*width = w;
	if (height)
		*height = h;

	return 0;

err_close:
	close(fd);
	return -1;
}

/* Raw inverted image load */
int load_raw_mem(const void *data, GLuint target, int width, int height, 
		 int stride, GLenum fmt, GLenum type)
{
	int ln, input_ln;
	int num_comp = 4;
	int comp_size = 1;
	int line_size;
	GLint gl_fmt = GL_RGBA;
	char *buffer;

	/* Getting number of pixel components */
	switch (fmt) {
		case GL_RED:
		case GL_GREEN:
		case GL_BLUE:
			num_comp = 1;
			break;
		case GL_ALPHA:
			num_comp = 1;
			gl_fmt = GL_ALPHA;
			break;
		case GL_LUMINANCE:
			num_comp = 1;
			gl_fmt = GL_LUMINANCE;
			break;
		case GL_LUMINANCE_ALPHA:
			num_comp = 2;
			gl_fmt = GL_LUMINANCE_ALPHA;
			break;
		case GL_RGB:
		case GL_BGR:
			num_comp = 3;
			gl_fmt = GL_RGB;
			break;
	}

	/* Getting size of components */
	switch (fmt) {
		case GL_UNSIGNED_SHORT:
		case GL_SHORT:
			comp_size = 2;
			break;
		case GL_FLOAT:
		case GL_UNSIGNED_INT:
		case GL_INT:
			comp_size = 4;
			break;
	}

	line_size = width*num_comp*comp_size;
	buffer = (char*)malloc(height*line_size);

	/* Inverting image */
	ln = line_size*(height - 1);
	input_ln = 0;
	for (int i = 0; i < height; i++) {
		memcpy(buffer + ln, (char*)data + input_ln, line_size);
		input_ln += line_size + stride;
		ln -= line_size;
	}

	/* Sending to GL */
	glTexImage2D(target, 0, gl_fmt, width, height, 0, 
		     fmt, type, buffer);

	/* Over */
	free(buffer);
	return 0;
}


