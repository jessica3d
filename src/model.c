/* Memory management */
#include <stdlib.h>

/* strlen(3) */
#include <string.h>

/* Those standard types */
#include <stdint.h>

#include "jessica.h"
#include "model.h"
#include "attrib.h"
#include "group.h"
#include "geometry.h"
#include "lighting.h"
#include "texture.h"
#include "text.h"
#include "lerp.h"
#include "sensor.h"

typedef uint_fast32_t	strhash_t;

#define OBJ_STATIC_MASK		(1<<(((sizeof(int)-1)<<3)+7))


static int obj_table_size;
static int num_obj;
struct obj_table_entry {
	strhash_t	hash;
	struct obj	*obj;
} static *obj_table;

/* Dan Bernstein string hash function */
static strhash_t hash_string(const char *str)
{
	int len = strlen(str);
	strhash_t hash = 5381;
	for (int i=0; i<len; ++i) 
		hash = 33*hash + str[i];
	return hash;
}
/* Object initialiation of the specified type */
static void obj_init(struct obj* obj)
{
	const struct obj_type *type = obj->type;
	for (int i = 0; type->comp[i]; i++)
		if (type->comp[i]->init)
			type->comp[i]->init(obj);
}

/* Object creation for a specific type */
struct obj * obj_create(const struct obj_type* type)
{
	struct obj *obj = (struct obj*)calloc(1, type->obj_size);
	obj->type = type;

	obj_ref(obj);
	obj_init(obj);

	return obj;
}

/* Static object initialisation */
void obj_static_create(struct obj* obj, const struct obj_type* type)
{
	/* Clearing object */
	memset(obj, 0, type->obj_size);
	obj->refs = OBJ_STATIC_MASK+1;
	obj->type = type;
	obj_init(obj);
}

/* Object destruction function */
void obj_destroy(struct obj* obj)
{
	/* Don't destroy static types */
	obj->refs &= OBJ_STATIC_MASK;

	const struct obj_type *type = obj->type;
	for (int i = type->num_comp; type->comp[i]; i--) {
		if (type->comp[i]->destroy)
			type->comp[i]->destroy(obj);
	}

	/* It's name might be registed */
	if (obj->name) {
		unregister_obj(obj->name);
		free(obj->name);
	}

	if (!(obj->refs))
		free(obj);
}

/* Object table initialization */
static void obj_table_init(void)
{
	obj_table_size = 8;
	obj_table = (struct obj_table_entry*)calloc(1, obj_table_size*
			sizeof(struct obj_table_entry));
}

/* Object table entry position calculation */
static int obj_pos(strhash_t hash, const char *name)
{
	int pos = hash & (obj_table_size-1);
	while (obj_table[pos].obj) {
		if (hash == obj_table[pos].hash && 
		    !strcmp(name, obj_table[pos].obj->name))
			return pos;
		pos = (pos+1)&(obj_table_size-1);
	}

	return pos;
}

/* Object hash table resize */
static void obj_table_resize(int new_size)
{
	int pos;
	int old_size = obj_table_size;
	struct obj_table_entry *old_table = obj_table;

	obj_table_size = new_size;
	obj_table = (struct obj_table_entry*)calloc(1, obj_table_size*
			sizeof(struct obj_table_entry));

	for (int i = 0; i < old_size; i++)
		if (old_table[i].obj) {
			pos = obj_pos(old_table[i].hash,old_table[i].obj->name);
			obj_table[pos].hash = old_table[i].hash;
			obj_table[pos].obj = old_table[i].obj;
		}

	free(old_table);
}

/* Object table get */
struct obj* fetch_obj(const char *name)
{
	strhash_t hash = hash_string(name);

	return obj_table[obj_pos(hash, name)].obj;
}

/* Object table addition, growing table when 75 % full */
void register_obj(struct obj *obj)
{
	strhash_t hash = hash_string(obj->name);

	int pos = obj_pos(hash, obj->name);
	/* Overwrite the entry when the object is already there */
	if (!obj_table[pos].obj) {
		if (++num_obj > obj_table_size - (obj_table_size>>2)) {
			obj_table_resize((obj_table_size<<1)+1);
			/* Position ain't good anymore */
			pos = obj_pos(hash, obj->name);
		}
	}

	obj_table[pos].hash = hash;
	obj_table[pos].obj = obj;
}

/* Object table addition, shrinking table when 25 % full */
struct obj* unregister_obj(const char *name)
{
	struct obj *obj;
	strhash_t hash = hash_string(name);

	int pos = obj_pos(hash, name);
	obj = obj_table[pos].obj;

	/* Object not in the table? */
	if (!obj)
		return NULL;

	obj_table[pos].hash = 0;
	obj_table[pos].obj = NULL;
	if (--num_obj < (obj_table_size>>2))
		obj_table_resize(obj_table_size>>1);

	return obj;
}

/* Verifies if an object has a component member of the specified type */
int obj_of_type(struct obj* obj, const struct obj_comp *comp)
{
	const struct obj_type *type = obj->type;

	for (int i = type->num_comp-1; i; i--)
		if (type->comp[i] == comp)
			return 1;

	return 0;
}

static void static_type_init(const struct obj_type *type)
{
	for (int i = 0; type->comp[i]; i++)
		if (type->comp[i]->dyn_setup)
			type->comp[i]->dyn_setup(type->dynt);
}

/* Init of the object model for static types with dynamic functions */
static void static_model_init(void)
{
	/* Grouping module */
	static_type_init(&group_type);

	/* Lighting module */
	static_type_init(&dirlight_type);

	/* Geometry module */
	static_type_init(&shape_type);
	static_type_init(&box_type);
	static_type_init(&cone_type);
	static_type_init(&cylinder_type);
	static_type_init(&sphere_type);
	static_type_init(&mesh_type);

	/* Texturing module */
	static_type_init(&texture2d_type);

	/* Text module */
	static_type_init(&text_type);
	static_type_init(&font_type);

	/* Sensor module */
	static_type_init(&time_sensor_type);

	/* Interpolator module */
	static_type_init(&quat_lerp_type);
}

/* Init of the object model */
void model_init(void)
{
	/* Object table */
	obj_table_init();

	/* Staticality defined types */
	static_model_init();
}

/* Object attribute query */
const struct obj_att* obj_attrib(struct obj *obj, const char *att_name)
{
	const struct obj_type *type = obj->type;
	for (int i = type->num_comp-1; i >= 0; i--) {
		if (!type->comp[i]->attribs)
			continue;
		for (const struct obj_att **att = type->comp[i]->attribs;
		     *att; att++)
			if (!strcmp((*att)->name, att_name))
				return *att;
	}

	return NULL;
}

/* Print GL error */
void pglerror(const char *prefix)
{
	const char* gl_str = (const char *)gluErrorString(glGetError());
	if (prefix)
		fprintf(stderr, "%s: %s", prefix, gl_str);
	else
		fputs(gl_str, stderr);
	fputc('\n', stderr);
}

