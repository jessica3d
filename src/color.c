#include "jessica.h"
#include "color.h"

color3 color3_black = { .v = COLOR3_BLACK };
color4 color4_black_transp = { .v = COLOR4_BLACK_TRANSP };
color4 color4_black_opaque = { .v = COLOR4_BLACK_OPAQUE };

color3 color3_white = { .v = COLOR3_WHITE };
color4 color4_white_transp = { .v = COLOR4_WHITE_TRANSP };
color4 color4_white_opaque = { .v = COLOR4_WHITE_OPAQUE };



void printcolor3(const color3 *c, const char *s)
{
	if (s)
		printf (" %s: [ %f %f %f ]\n", s, c->v[0], c->v[1], c->v[2]);
	else
		printf ("[ %f %f %f ]\n", c->v[0], c->v[1], c->v[2]);
}


