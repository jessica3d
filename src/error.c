/* strncpy(3) */
#include <string.h>


#include "error.h"

#define ERR_BUF_SIZE	512

/* Buffer used to store error strings */
static char err_buf[ERR_BUF_SIZE];

/* Default error string */
static const char err_default[] = "Error";

/* Setting of the error string */
void set_error(const char * err_str)
{
	strncpy(err_buf, err_str, ERR_BUF_SIZE);
	err_buf[ERR_BUF_SIZE-1] = 0;
}

/* Returns the error type of request */
const char * get_error(int err_code)
{
	if (err_code == ERR_STR)
		return err_buf;

	if (err_code == ERR_NOSTR)
		return err_default;

	if (err_code < 0)
		return strerror(-err_code);

	return NULL;
}


