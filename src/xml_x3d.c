/* XML Parsing */
#include <expat.h>

/* memcpy(3), strdup(3), strcmp(3) */
#include <string.h>

/* intptr_t */
#include <stdint.h>

/* fread(3) */
#include <stdio.h>

#include <assert.h>

#include "stack.h"
#include "scene.h"
#include "model.h"
#include "loader.h"
#include "geometry.h"
#include "jessica.h"
#include "shading.h"
#include "texture.h"
#include "text.h"
#include "sensor.h"
#include "lerp.h"

#define BUFFER_SIZE	8192

#define XML_DTD

enum parser_state { STATE_START=0, STATE_X3D, STATE_HEAD, STATE_SCENE,
		    STATE_USE, STATE_GROUP, STATE_SHAPE, STATE_APPEARANCE, 
		    STATE_IDX_MESH
};

struct parser_ctx {
	int		depth;	/* Depth in the XML hierarchy, root is 1 */
	int		skip;	/* Skip depth flag */
	char*		accum;	/* Accumulation buffer */
	struct stack	state;	/* Parsing state */
	struct stack	node;	/* Currently processed node */
	struct scene	*scene;
	XML_Parser	parser;
};

/* Attribute finder in an attrib list */
static int inline find_att(const XML_Char **atts, const char *needle)
{
	for (int i = 0; atts[i]; i+=2)
		if (!strcmp(needle, atts[i]))
			return i+1;
	return -1;
}

/* X3D document start */
static void x3d_element_start(struct parser_ctx *ctx, const XML_Char **atts)
{
	stack_push(&ctx->state, (void*)STATE_X3D);
}

/* State popping end of element handler */
static void state_pop(struct parser_ctx *ctx)
{
	stack_pop(&ctx->state);
}

/* State popping end of element handler */
static void state_node_pop(struct parser_ctx *ctx)
{
	int st = (intptr_t)stack_pop(&ctx->state);
	if (st != STATE_USE)
		stack_pop(&ctx->node);
}

/* Scene graph description start */
static void scene_element_start(struct parser_ctx *ctx, const XML_Char **atts)
{
	/* Scene is strictly at the next element */
	if ((intptr_t)stack_top(&ctx->state) != STATE_X3D) {
		XML_StopParser(ctx->parser, XML_FALSE);
		return;
	}

	/* Creating the transform hierarchy's root */
	obj_static_create((struct obj*)&ctx->scene->content, &group_type);

	stack_push(&ctx->state, (void*)STATE_SCENE);
}

static struct obj* verif_use(const XML_Char **atts, int *num_atts)
{
	int i;
	const XML_Char *ref = NULL;
	for (i=0; atts[i]; i+=2)
		if (!strcmp(atts[i], "USE")) {
			ref = atts[i+1];
			break;
		}
	while (atts[i])
		i+=2;
			
	*num_atts = i>>1;

	/* No USE statement? */
	if (!ref)
		return NULL;

	return fetch_obj((const char*)ref);
}

static void set_ref(struct obj *obj, const XML_Char **atts)
{
	int i;
	for (i=0; atts[i]; i+=2)
		if (strcmp(atts[i], "REF"))
			break;

	/* Maybe no ref */
	if (!atts[i])
		return;

	obj->name = strdup(atts[i+1]);
	register_obj(obj);
}

/* Viewpoint node start */
static void viewpoint_element_start(struct parser_ctx *ctx, 
				    const XML_Char **atts)
{
	int num_atts;
	struct viewpoint *vp = (struct viewpoint*)verif_use(atts, &num_atts);

	if (!vp) {
		vp = (struct viewpoint*)obj_create(&viewpoint_type);

		/* Viewpoint Description */
		for (int i = 0; atts[i]; i+=2) {
			if (!strcmp("description", atts[i])) {
				vp->desc = strdup(atts[i+1]);
				if (!--num_atts)
					goto end_parse;
				break;
			}
		}

		/* Orientation */
		for (int i = 0; atts[i]; i+=2) {
			if (!strcmp("orientation", atts[i])) {
				const char *values = atts[i+1];
				loader_fill_coord(vp->orientation.v,&values, 3);
				loader_fill_angle(&vp->rot, &values, 1);
				if (!--num_atts)
					goto end_parse;
				break;
			}
		}

		/* Viewer Position relative to scene origin */
		for (int i = 0; atts[i]; i+=2) {
			if (!strcmp("position", atts[i])) {
				const char *values = atts[i+1];
				loader_fill_coord(vp->pos.v, &values, 3);
				if (!--num_atts)
					goto end_parse;
				break;
			}
		}
	} else {
		obj_ref((struct obj*)vp);
	}

	if (num_atts)
		set_ref((struct obj*)vp, atts);
end_parse:
	
	stack_push(&ctx->scene->viewpoints, vp);
}

/* Model hierarchy node start */
static void transform_element_start(struct parser_ctx *ctx, 
				    const XML_Char **atts)
{
	int num_atts;
	struct group *parent;
	struct group *grp = (struct group*)verif_use(atts, &num_atts);

	/* So a new instance */
	if (!grp) {
		grp = (struct group*)obj_create(&group_type);

		/* Translation relative to parent */
		for (int i = 0; atts[i]; i+=2) {
			if (!strcmp("translation", atts[i])) {
				const char *parse = atts[i+1];
				loader_fill_vec3(&grp->transl, &parse);
				if (!--num_atts)
					goto end_parse;
				break;
			}
		}

		/* Rotation relative to parent */
		for (int i = 0; atts[i]; i+=2) {
			if (!strcmp("rotation", atts[i])) {
				const char *parse = atts[i+1];
				loader_fill_rot(&grp->rotation, &parse);
				if (!--num_atts)
					goto end_parse;
				break;
			}
		}

		/* Scaling transform */
		for (int i = 0; atts[i]; i+=2) {
			if (!strcmp("scale", atts[i])) {
				const char *parse = atts[i+1];
				loader_fill_vec3(&grp->scale, &parse);
				if (!--num_atts)
					goto end_parse;
				break;
			}
		}

		/* Rotation to apply before scaling */
		for (int i = 0; atts[i]; i+=2) {
			if (!strcmp("scaleOrientation", atts[i])) {
				const char *parse = atts[i+1];
				loader_fill_rot(&grp->scale_rot, &parse);
				if (!--num_atts)
					goto end_parse;
				break;
			}
		}

		/* Rotation center (translation to apply before rotation) */
		for (int i = 0; atts[i]; i+=2) {
			if (!strcmp("center", atts[i])) {
				const char *parse = atts[i+1];
				loader_fill_vec3(&grp->center, &parse);
				if (!--num_atts)
					goto end_parse;
				break;
			}
		}

	} else {
		obj_ref((struct obj*)grp);
	}

	if (num_atts)
		set_ref((struct obj*)grp, atts);
	
end_parse:
	/* Could be the transform hierarchy root */
	if ((intptr_t)stack_top(&ctx->state) == STATE_SCENE) {
		parent = &ctx->scene->content;
	} else
		parent = (struct group*)stack_top(&ctx->node);

	list_insert(&parent->children, grp);

	if (obj_refcnt((struct obj*)grp) > 1) {
		stack_push(&ctx->state, (void*)STATE_USE);
	} else {
		stack_push(&ctx->state, (void*)STATE_GROUP);
		stack_push(&ctx->node, grp);
	}
}

/* Shape element start of parsing */
static void shape_element_start(struct parser_ctx *ctx, const XML_Char **atts)
{
	/* Need to know where to go in the rendering hierarchy */
	int num_atts;
	struct group *parent;
	struct shape *shape = (struct shape*)verif_use(atts, &num_atts);

	int state = (intptr_t)stack_top(&ctx->state);
	if (state == STATE_SCENE)
		parent = &ctx->scene->content;
	else
		parent = (struct group*)stack_top(&ctx->node);

	if (!shape)
		shape = (struct shape*)obj_create(&shape_type);
	else
		obj_ref((struct obj*)shape);

	if (num_atts)
		set_ref((struct obj*)shape, atts);

	list_insert(&parent->children, shape);

	stack_push(&ctx->state, (void*)STATE_SHAPE);
	stack_push(&ctx->node, shape);
}

static void geometry_parse(struct geom* geom, const XML_Char **atts, 
			   int *num_atts)
{
	for (int i = 0; atts[i]; i+=2) {
		if (!strcmp(atts[i], "solid")) {
			if (!strcmp(atts[i], "FALSE"))
				((struct geom*)geom)->cull = 1;
			else if (!strcmp(atts[i], "TRUE"))
				((struct geom*)geom)->cull = 0;
			--*num_atts;
			return;
		}
	}
}

static void sensor_parse(struct sensor* sens, const XML_Char **atts, 
			 int *num_atts)
{
	int res;
	const char *parse;

	res = find_att(atts, "enabled");
	if (res >= 0) {
		parse = atts[res];
		loader_fill_bool(&sens->enabled, &parse, 1);
		--*num_atts;
	}
}

static void interp_parse(struct lerp* lrp, const XML_Char **atts, 
			 int *num_atts)
{
	int res;

	res = find_att(atts, "key");
	if (res >= 0) {
		lrp->num_key = loader_fill_double_array(&lrp->keys, 
							atts[res], 1);
		--*num_atts;
	}
}

/* Box element start of parsing */
static void box_element_start(struct parser_ctx *ctx, const XML_Char **atts)
{
	int num_atts, res;
	struct shape *shape = (struct shape*)stack_top(&ctx->node);
	struct box *box = (struct box*)verif_use(atts, &num_atts);
	const char *parse;
	
	if (!box) {
		box = (struct box*)obj_create(&box_type);

		for (int i=0; atts[i]; i+=2) {
			if (!strcmp(atts[i], "size")) {
				parse = atts[i+1];
				res = loader_fill_coord(box->size.v, &parse, 3);

				if (!--num_atts)
					goto end_parse;
			}
		}

		geometry_parse((struct geom*)box, atts, &num_atts);
	} else
		obj_ref((struct obj*)box);

	if (num_atts)
		set_ref((struct obj*)box, atts);

end_parse:
	shape->geom = (struct geom*)box;
}

/* Cone element start of parsing */
static void cone_element_start(struct parser_ctx *ctx, const XML_Char **atts)
{
	int num_atts;
	struct shape *shape = (struct shape*)stack_top(&ctx->node);
	struct cone *cone = (struct cone*)verif_use(atts, &num_atts);
	const char *parse;
	
	if (!cone) {
		cone = (struct cone*)obj_create(&cone_type);

		for (int i=0; atts[i]; i+=2) {
			if (!strcmp(atts[i], "bottomRadius")) {
				parse = atts[i+1];
				loader_fill_coord(&cone->radius, &parse, 1);

				if (!--num_atts)
					goto end_parse;
			}
		}

		for (int i=0; atts[i]; i+=2) {
			if (!strcmp(atts[i], "height")) {
				parse = atts[i+1];
				loader_fill_coord(&cone->height, &parse, 1);

				if (!--num_atts)
					goto end_parse;
			}
		}

		for (int i=0; atts[i]; i+=2) {
			if (!strcmp(atts[i], "side")) {
				if (!strcmp(atts[i+1], "FALSE"))
					cone->side = 1;
				
				if (!--num_atts)
					goto end_parse;
			}
		}

		for (int i=0; atts[i]; i+=2) {
			if (!strcmp(atts[i], "bottom")) {
				if (!strcmp(atts[i+1], "FALSE"))
					cone->bottom = 1;
				
				if (!--num_atts)
					goto end_parse;
			}
		}

		geometry_parse((struct geom*)cone, atts, &num_atts);
	} else
		obj_ref((struct obj*)cone);

	if (num_atts)
		set_ref((struct obj*)cone, atts);

end_parse:
	shape->geom = (struct geom*)cone;
}

/* Cylinder element start of parsing */
static void cylinder_element_start(struct parser_ctx *ctx, 
				   const XML_Char **atts)
{
	int num_atts, res;
	struct shape *shape = (struct shape*)stack_top(&ctx->node);
	struct cylinder *cyl = (struct cylinder*)verif_use(atts, &num_atts);
	const char *parse;
	
	if (!cyl) {
		cyl = (struct cylinder*)obj_create(&cylinder_type);

		for (int i=0; atts[i]; i+=2) {
			if (!strcmp(atts[i], "radius")) {
				parse = atts[i+1];
				loader_fill_coord(&cyl->radius, &parse, 1);

				if (!--num_atts)
					goto end_parse;
			}
		}

		for (int i=0; atts[i]; i+=2) {
			if (!strcmp(atts[i], "height")) {
				parse = atts[i+1];
				res = loader_fill_coord(&cyl->height, 
							&parse, 1);

				if (!--num_atts)
					goto end_parse;
			}
		}

		for (int i=0; atts[i]; i+=2) {
			if (!strcmp(atts[i], "side")) {
				if (!strcmp(atts[i+1], "FALSE"))
					cyl->side = 1;
				
				if (!--num_atts)
					goto end_parse;
			}
		}

		for (int i=0; atts[i]; i+=2) {
			if (!strcmp(atts[i], "top")) {
				if (!strcmp(atts[i+1], "FALSE"))
					cyl->top = 1;
				
				if (!--num_atts)
					goto end_parse;
			}
		}

		for (int i=0; atts[i]; i+=2) {
			if (!strcmp(atts[i], "bottom")) {
				if (!strcmp(atts[i+1], "FALSE"))
					cyl->bottom = 1;
				
				if (!--num_atts)
					goto end_parse;
			}
		}

		geometry_parse((struct geom*)cyl, atts, &num_atts);
	} else
		obj_ref((struct obj*)cyl);

	if (num_atts)
		set_ref((struct obj*)cyl, atts);

end_parse:
	shape->geom = (struct geom*)cyl;
}


/* Sphere element start of parsing */
static void sphere_element_start(struct parser_ctx *ctx, 
				 const XML_Char **atts)
{
	int num_atts;
	struct shape *shape = (struct shape*)stack_top(&ctx->node);
	struct sphere *sphere = (struct sphere*)verif_use(atts, &num_atts);
	const char *parse;
	
	if (!sphere) {
		sphere = (struct sphere*)obj_create(&sphere_type);

		for (int i=0; atts[i]; i+=2) {
			if (!strcmp(atts[i], "radius")) {
				parse = atts[i+1];
				loader_fill_coord(&sphere->radius, &parse, 1);

				if (!--num_atts)
					goto end_parse;
			}
		}

		geometry_parse((struct geom*)sphere, atts, &num_atts);
	} else
		obj_ref((struct obj*)sphere);

	if (num_atts)
		set_ref((struct obj*)sphere, atts);

end_parse:
	shape->geom = (struct geom*)sphere;
}

/* Text element start of parsing */
static void text_element_start(struct parser_ctx *ctx, 
			       const XML_Char **atts)
{
	int num_atts;
	struct shape *shape = (struct shape*)stack_top(&ctx->node);
	struct text *text = (struct text*)verif_use(atts, &num_atts);
	const char *parse;
	
	if (!text) {
		text = (struct text*)obj_create(&text_type);

		for (int i=0; atts[i]; i+=2) {
			if (!strcmp(atts[i], "string")) {
				parse = atts[i+1];
				text->num_string = loader_fill_string_array(
					&text->strings, parse);

				if (!--num_atts)
					goto end_parse;
			}
		}

		geometry_parse((struct geom*)text, atts, &num_atts);
	} else
		obj_ref((struct obj*)text);

	if (num_atts)
		set_ref((struct obj*)text, atts);

end_parse:
	shape->geom = (struct geom*)text;
}

/* TimeSensor element start of parsing */
static void time_sensor_element_start(struct parser_ctx *ctx, 
			              const XML_Char **atts)
{
	int res;
	int num_atts;
	struct time_sensor *sens = (struct time_sensor*)verif_use(atts, 
								  &num_atts);
	const char *parse;
	
	if (!sens) {
		sens = (struct time_sensor*)obj_create(&time_sensor_type);

		/* Cycle interval time */
		res = find_att(atts, "cycleInterval");
		if (res >= 0) {
			parse = atts[res];
			loader_fill_timens(&sens->cycle_interval, &parse, 1);

			if (!--num_atts)
				goto end_parse;
		}

		/* Restart at time exhaustion */
		res = find_att(atts, "loop");
		if (res >= 0) {
			parse = atts[res];
			loader_fill_bool(&sens->loop, &parse, 1);

			if (!--num_atts)
				goto end_parse;
		}

		sensor_parse((struct sensor*)sens, atts, &num_atts);
	} else
		obj_ref((struct obj*)sens);

	if (num_atts)
		set_ref((struct obj*)sens, atts);

end_parse:
	list_insert(&ctx->scene->sensors, sens);
}

/* OrientationInterpolator element start of parsing */
static void orien_interp_element_start(struct parser_ctx *ctx, 
			               const XML_Char **atts)
{
	int res;
	int num_atts;
	struct quat_lerp *lrp = (struct quat_lerp*)verif_use(atts, &num_atts);
	const char *parse;
	
	if (!lrp) {
		lrp = (struct quat_lerp*)obj_create(&quat_lerp_type);

		/* Cycle interval time */
		res = find_att(atts, "keyValue");
		if (res >= 0) {
			parse = atts[res];
			loader_fill_rot_array(&lrp->values, parse);

			if (!--num_atts)
				goto end_parse;
		}

		interp_parse((struct lerp*)lrp, atts, &num_atts);
	} else
		obj_ref((struct obj*)lrp);

	if (num_atts)
		set_ref((struct obj*)lrp, atts);

end_parse:
	list_insert(&ctx->scene->lerps, lrp);
}



/* ROUTE element start of parsing */
static void route_element_start(struct parser_ctx *ctx, 
			        const XML_Char **atts)
{
	int res;

	/* Creating route node */
	struct route *rt = (struct route*)malloc(sizeof(struct route));

	/* Getting refered objects */
	/* Source */
	res = find_att(atts, "fromNode");
	assert(res >= 0);
	if (res < 0)
		goto err_free;
	rt->src = fetch_obj(atts[res]);

	/* Destination */
	res = find_att(atts, "toNode");
	assert(res >= 0);
	if (res < 0)
		goto err_free;
	rt->dst = fetch_obj(atts[res]);

	/* Setting fields */
	res = find_att(atts, "fromField");
	assert(res >= 0);
	if (res < 0)
		goto err_free;
	rt->src_att = obj_attrib(rt->src, atts[res]);
	if (!rt->src_att || !rt->src_att->get)
		goto err_free;

	res = find_att(atts, "toField");
	assert(res >= 0);
	if (res < 0)
		goto err_free;
	rt->dst_att = obj_attrib(rt->dst, atts[res]);
	if (!rt->dst_att || !rt->dst_att->set)
		goto err_free;

	assert(rt->src_att->size == rt->dst_att->size);

	/* Registering */
	list_insert(&ctx->scene->routes, rt);
	return;

err_free:
	assert(0);
	free(rt);
}

/* IndexedFaceSet element start of parsing */
static void idx_mesh_element_start(struct parser_ctx *ctx, 
				   const XML_Char **atts)
{
	int res;
	int num_atts;
	struct shape *shape = (struct shape*)stack_top(&ctx->node);
	struct mesh *mesh = (struct mesh*)verif_use(atts, &num_atts);
	const char *parse;
	
	if (!mesh) {
		mesh = (struct mesh*)obj_create(&mesh_type);

		for (int i=0; atts[i]; i+=2) {
			if (!strcmp(atts[i], "coordIndex")) {
				parse = atts[i+1];
				res =loader_fill_int_array((int**)(void*)
						&mesh->faces, parse, 3);
				mesh->num_face = res;

				if (!--num_atts)
					goto end_parse;
			}
		}

		geometry_parse((struct geom*)mesh, atts, &num_atts);
	} else
		obj_ref((struct obj*)mesh);

	if (num_atts)
		set_ref((struct obj*)mesh, atts);

end_parse:
	shape->geom = (struct geom*)mesh;
	
	/* Comming is the data feeding faces */
	stack_push(&ctx->state, (void*)STATE_IDX_MESH);
	stack_push(&ctx->node, mesh);
}

/* Coordinate element start of parsing */
static void coordinate_element_start(struct parser_ctx *ctx, 
				     const XML_Char **atts)
{
	int res;
	int num_atts;
	/* TODO: Currently not supporting refed coordinates */
	assert(!verif_use(atts,&num_atts));
	struct mesh *mesh = (struct mesh*)stack_top(&ctx->node);
	const char *parse;
	
	for (int i=0; atts[i]; i+=2) {
		if (!strcmp(atts[i], "point")) {
			parse = atts[i+1];
			res = loader_fill_vec3_array(&mesh->vertices, parse)/3;
			mesh->num_vertex = res;

			if (!--num_atts)
				return;
		}
	}
}

/* Appearance element start of parsing */
static void appearance_element_start(struct parser_ctx *ctx, 
				     const XML_Char **atts)
{
	int num_atts;
	struct shape *shape = (struct shape*)stack_top(&ctx->node);
	struct shading *shading = (struct shading*)verif_use(atts, &num_atts);

	/* Creation appearance on spec */
	if (!shading) {
		shading = (struct shading*)obj_create(&shading_type);
	} else
		obj_ref((struct obj*)shading);

	shape->shading = shading;
	if (obj_refcnt((struct obj*)shading) > 1) {
		stack_push(&ctx->state, (void*)STATE_USE);
	} else {
		stack_push(&ctx->state, (void*)STATE_APPEARANCE);
		stack_push(&ctx->node, shading);
	}
}

/* Material element start of parsing */
static void material_element_start(struct parser_ctx *ctx, 
				   const XML_Char **atts)
{
	int num_atts;
	struct shading *shading = (struct shading*)stack_top(&ctx->node);
	struct material *material = (struct material*)verif_use(atts,&num_atts);
	const char *parse;
	
	if (!material) {
		material = (struct material*)obj_create(&material_type);

		for (int i=0; atts[i]; i+=2) {
			if (!strcmp(atts[i], "diffuseColor")) {
				parse = atts[i+1];
				loader_fill_color(material->color.v, &parse, 3);

				if (!--num_atts)
					goto end_parse;
			}
		}

		for (int i=0; atts[i]; i+=2) {
			if (!strcmp(atts[i], "specularColor")) {
				parse = atts[i+1];
				loader_fill_color(material->specular.v, &parse, 
						  3);

				if (!--num_atts)
					goto end_parse;
			}
		}
	} else
		obj_ref((struct obj*)material);

	if (num_atts)
		set_ref((struct obj*)material, atts);

end_parse:
	shading->material = material;
}

/* ImageTexture element start of parsing */
static void image_texture_element_start(struct parser_ctx *ctx, 
				        const XML_Char **atts)
{
	int num_atts;
	int res;
	struct shading *shading = (struct shading*)stack_top(&ctx->node);
	struct texture2d *tex = (struct texture2d*)verif_use(atts, &num_atts);
	const char *parse;
	
	if (!tex) {
		tex = (struct texture2d*)obj_create(&texture2d_type);

		res = find_att(atts, "url");
		if (res >= 0) {
			parse = atts[res];
			((struct texture*)tex)->num_loc = 
				loader_fill_string_array(
					&((struct texture*)tex)->locs, parse);
		}

		if (!--num_atts)
			goto end_parse;
	} else
		obj_ref((struct obj*)tex);

	if (num_atts)
		set_ref((struct obj*)tex, atts);

end_parse:
	shading->texture = (struct texture *)tex;
}

struct element_handler {
	const char	*element;
	void		(*start_hdl)(struct parser_ctx*, const XML_Char**);
	void		(*end_hdl)(struct parser_ctx*);
} static const handlers[] = {
	{ "X3D", x3d_element_start, state_pop },
	{ "Scene", scene_element_start, state_pop },
	{ "Viewpoint", viewpoint_element_start, NULL },
	{ "Transform", transform_element_start, state_node_pop },
	/* Using same for group and transform, as a Transform without any
	 * operation is the same as group */
	{ "Group", transform_element_start, state_node_pop },
	{ "Shape", shape_element_start, state_node_pop },
	{ "Box", box_element_start, NULL },
	{ "Cone", cone_element_start, NULL },
	{ "Cylinder", cylinder_element_start, NULL },
	{ "Sphere", sphere_element_start, NULL },
	{ "IndexedFaceSet", idx_mesh_element_start, state_node_pop },
	{ "Coordinate", coordinate_element_start, NULL },
	{ "Appearance", appearance_element_start, state_node_pop },
	{ "Material", material_element_start, NULL },
	{ "ImageTexture", image_texture_element_start, NULL },
	{ "Text", text_element_start, NULL },
	{ "TimeSensor", time_sensor_element_start, NULL },
	{ "OrientationInterpolator", orien_interp_element_start, NULL },
	{ "ROUTE", route_element_start, NULL }
};

/* XML element start */
static void XMLCALL element_start(void *data, const XML_Char *name,
				  const XML_Char **atts)
{
	struct parser_ctx *ctx = (struct parser_ctx*)data;

	ctx->depth++;

	if (ctx->skip && ctx->depth > ctx->skip)
		return;

	const struct element_handler *hdl = NULL;
	for (int i = 0; i < sizeof(handlers)/sizeof(handlers[0]); i++)
		if (!strcmp(name, handlers[i].element)) {
			hdl = &handlers[i];
			break;
		}

	/* Got an unhandled element */
	if (!hdl) {
		printf("skipping: %s\n", name);
		ctx->skip = ctx->depth;
		return;
	}

	if (hdl->start_hdl)
		hdl->start_hdl(ctx, atts);
}

/* XML element end */
static void XMLCALL element_end(void *data, const XML_Char *name)
{
	struct parser_ctx *ctx = (struct parser_ctx*)data;

	if (ctx->skip && ctx->depth > ctx->skip) {
		ctx->depth--;
		return;
	} else if (ctx->depth == ctx->skip) {
		ctx->skip = 0;
		ctx->depth--;
		return;
	}

	const struct element_handler *hdl = NULL;
	for (int i = 0; i < sizeof(handlers)/sizeof(handlers[0]); i++)
		if (!strcmp(name, handlers[i].element)) {
			hdl = &handlers[i];
			break;
		}

	/* Got an unhandled element */
	if (!hdl) {
		ctx->skip = ctx->depth;
		return;
	}

	if (hdl->end_hdl)
		hdl->end_hdl(ctx);

	ctx->depth--;
}

/* XML CDATA section start */
static void XMLCALL cdata_start(void *data)
{
}

/* XML CDATA section data */
static void XMLCALL cdata_data(void *data, const XML_Char *str, int len)
{
}

/* XML CDATA section end */
static void XMLCALL cdata_end(void *data)
{
}

static int file_continue(XML_Parser parser, FILE *f)
{
	void *buf;
	size_t bytes;
	enum XML_Status res;

	/* Parsing loop */
	while (!feof(f)) {
		buf = XML_GetBuffer(parser, BUFFER_SIZE);
		if (!buf) {
			set_error(XML_ErrorString(XML_GetErrorCode(parser)));
			return ERR_STR;
		}

		/* Reading */
		bytes = fread(buf, 1, BUFFER_SIZE, f);
		if (bytes < BUFFER_SIZE && ferror(f))
			return -errno;
			/* Then it's eof */

		res = XML_ParseBuffer(parser, bytes, feof(f));
		if (res == XML_STATUS_ERROR) {
			set_error(XML_ErrorString(XML_GetErrorCode(parser)));
			return ERR_STR;
		} else if (res == XML_STATUS_SUSPENDED)
			return 1;
	}

	return 0;
}

/* Parsing of an X3D XML document from a file */
int xml_x3d_file_parse(const char * filename, struct scene *scene)
{
	struct parser_ctx ctx = { .scene = scene };
	int res;
	setlinebuf(stdout);

	FILE *f = fopen(filename, "r");
	if (!f)
		return -errno;

	/* Using document's encoding */
	ctx.parser = XML_ParserCreate(NULL);
	if (!ctx.parser) {
		res = ERR_NOSTR;
		goto err_close;
	}

	XML_SetUserData(ctx.parser, &ctx);
	XML_SetElementHandler(ctx.parser, element_start, element_end);
	XML_SetCharacterDataHandler(ctx.parser, cdata_data);
	XML_SetCdataSectionHandler(ctx.parser, cdata_start, cdata_end);

	res = file_continue(ctx.parser, f);
	if (res)
		goto err_destroy;

	/* Over with parsing */
	XML_ParserFree(ctx.parser);
	fclose(f);

	return 0;
	
err_destroy:
	XML_ParserFree(ctx.parser);
err_close:
	fclose(f);
	return res;
}

