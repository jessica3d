/* Memory management */
#include <stdlib.h>

#include "list.h"

void list_insert(struct list** lst, void *data)
{
	struct list *ilst = (struct list*)calloc(1, sizeof(struct list));
	ilst->data = data;

	if (*lst) {
		ilst->prev = (*lst)->prev;
		if (ilst->prev)
			ilst->prev->next = ilst;
		(*lst)->prev = ilst;
		ilst->next = *lst;
		*lst = ilst;
	} else
		*lst = ilst;
}

void list_insert_list(struct list** lst, struct list* add)
{
	if (*lst) {
		add->prev = (*lst)->prev;
		if (add->prev)
			add->prev->next = add;
		struct list *ilst = add;
		while (ilst->next)
			ilst = ilst->next;
		ilst->next = *lst;
		(*lst)->prev = ilst;
	} else
		*lst = add;
}


void* list_remove(struct list** lst)
{
	struct list *ilst = *lst;

	if (ilst->prev)
		ilst->prev->next = ilst->next;
	if (ilst->next) {
		ilst->next->prev = ilst->prev;
		*lst = ilst->next;
	} else
		*lst = ilst->prev;

	void *d = ilst->data;
	free(ilst);
	return d;
}

void list_append(struct list** lst, void *data)
{
	if (!*lst)
		return list_insert(lst, data);

	struct list *ilst = (struct list*)calloc(1, sizeof(struct list));

	ilst->next = (*lst)->next;
	if (ilst->next)
		ilst->next->prev = ilst;
	(*lst)->next = ilst;
	ilst->prev = *lst;
}

void list_append_list(struct list** lst, struct list* add)
{
	if (!*lst)
		return list_insert_list(lst, add);

	struct list *ilst = add;
	while (ilst->next)
		ilst = ilst->next;
	ilst->next = (*lst)->next;
	if (ilst->next)
		ilst->next->prev = ilst;
	(*lst)->next = ilst;
	add->prev = *lst;
}

