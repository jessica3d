#ifndef COLOR_H
#define COLOR_H

typedef float		color_t;

#define COLOR_EPSILON	FLT_EPSILON

/* Color manipulation structures */
typedef union {
	color_t	v[3];
	struct {
		color_t	r;
		color_t	g;
		color_t	b;
	} c;
} color3;

typedef union {
	color_t	v[4];
	struct {
		color_t	r;
		color_t	g;
		color_t	b;
		color_t	a;
	} c;
} color4;

/* Useful colors */
#define COLOR3_BLACK		{ 0, 0, 0 }
extern color3 color3_black;
#define COLOR4_BLACK_TRANSP	{ 0, 0, 0, 0 }
extern color4 color4_black_transp;
#define COLOR4_BLACK_OPAQUE	{ 0, 0, 0, 1 }
extern color4 color4_black_opaque;

#define COLOR3_WHITE		{ 1, 1, 1 }
extern color3 color3_white;
#define COLOR4_WHITE_TRANSP	{ 1, 1, 1, 0 }
extern color4 color4_white_transp;
#define COLOR4_WHITE_OPAQUE	{ 1, 1, 1, 1 }
extern color4 color4_white_opaque;

/* Grey color R = G = B */
static inline color3 color3_grey(color_t v)
{ color3 col = { .v = { v, v, v } }; return col; }

static inline color4 color4_grey(color_t v)
{ color4 col = { .v = { v, v, v, 1 } }; return col; }

/* Color settings */
static inline color3 color3_c(color_t r, color_t g, color_t b)
{ color3 col = { .v = { r, g, b } };  return col; }

static inline color3 color3_v(color_t v[])
{ color3 col = { .v = { v[0], v[1], v[2] } }; return col; }

static inline color4 color4_c(color_t r, color_t g, color_t b, color_t a)
{ color4 col = { .v = { r, g, b, a } };  return col; }

static inline color4 color4_v(color_t v[])
{ color4 col = { .v = { v[0], v[1], v[2], v[3] } }; return col; }

/* Color modification */
static inline void color3_scale_self(color3 *col, float scale)
{ for (int i = 0; i < 3; i++) col->v[i] *= scale; }

static inline void color4_scale_self(color4 *col, float scale)
{ color3_scale_self((color3*)col, scale); }

static inline color3 color3_scale(const color3 col, float scale)
{ return color3_c(col.c.r*scale, col.c.g*scale, col.c.b*scale); }

static inline color4 color4_scale(const color4 col, float scale)
{ return color4_c(col.c.r*scale, col.c.g*scale, col.c.b*scale, col.c.a); }

/* Debug routines */
void print_color3(const color3* c, const char *s);

#endif	/* COLOR_H */
