#include "sensor.h"
#include "manager.h"
#include "event.h"

/* Base sensor initialisation */
static void sensor_init(struct sensor *sens)
{
	/* X3D Specification 7.3.6 */
	sens->enabled = 1;
}

/* Time sensor initialisation */
static void time_sensor_init(struct time_sensor *sens)
{
	/* X3D Specification 7.3.6 */
	sens->cycle_interval = 1;
}

/* Time sensor event generation */
static void time_sensor_eval(struct time_sensor *sens)
{
	/* Setting new sensor value */
	double integ;
	sens->value = modf(((render_ctx.time.tv_sec - scene->setup_time.tv_sec)+
			(render_ctx.time.tv_nsec - scene->setup_time.tv_nsec)/
			 1000000000.0)/sens->cycle_interval, &integ);

	/* Generating an event about this change */
	event_emit((struct obj*)sens, &time_sensor_fraction_changed_att);
}

/* Box dynamic table setup */
static void time_sensor_dyn_setup(struct time_sensor_dynt *dynt)
{
	((struct sensor_dynt*)dynt)->eval = (sensor_eval_func)
						  time_sensor_eval;
}

/* Object type related info */
const struct obj_att sensor_enabled_att = {	
	.name = "enabled",
	.off = offsetof(struct sensor, enabled),
	.size = sizeof(int),
	.get = get_int,
	.set = set_int
};

const struct obj_att *sensor_atts[] = {
	&sensor_enabled_att,
	NULL
};

const struct obj_comp sensor_comp = {
	.init = (init_func)sensor_init,
	.attribs = sensor_atts
};

const struct obj_att time_sensor_fraction_changed_att = {
	.name = "fraction_changed",
	.off = offsetof(struct time_sensor, value),
	.size = sizeof(double),
	.get = get_double
};

const struct obj_att time_sensor_cycle_interval_att = {
	.name = "cycleInterval",
	.off = offsetof(struct time_sensor, cycle_interval),
	.size = sizeof(timens_t),
	.get = get_timens,
	.set = set_timens
};

const struct obj_att time_sensor_loop_att = {
	.name = "loop",
	.off = offsetof(struct time_sensor, loop),
	.size = sizeof(int),
	.get = get_int,
	.set = set_int
};

const struct obj_att *time_sensor_atts[] = {
	&time_sensor_fraction_changed_att,
	&time_sensor_cycle_interval_att,
	&time_sensor_loop_att,
	NULL
};

const struct obj_comp time_sensor_comp = {
	.init = (init_func)time_sensor_init,
	.dyn_setup = (dyn_setup_func)time_sensor_dyn_setup,
	.attribs = time_sensor_atts
};

/* Dynamic tables */
struct time_sensor_dynt time_sensor_dynt;

