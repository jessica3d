#include <gtk/gtk.h>

#include "jessica.h"
#include "manager.h"

GtkWidget*	app_win;
GtkWidget*	render_win;

GtkAction*	ac_open_file;
GtkAction*	ac_quit;
GtkAction*	ac_about;

/* Legal strings */
const char copyright_str[] = "Copyright (C) 2007  " PACKAGE_NAME;

const char license_str[] = 
"Copyright (C) 2007  " PACKAGE_NAME "\n"
"\n"
"This program is free software; you can redistribute it and/or "
"modify it under the terms of the GNU General Public License "
"as published by the Free Software Foundation; either version 2 "
"of the License, or (at your option) any later version.\n"
"\n"
"This program is distributed in the hope that it will be useful, "
"but WITHOUT ANY WARRANTY; without even the implied warranty of "
"MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the "
"GNU General Public License for more details.\n"
"\n"
"You should have received a copy of the GNU General Public License "
"along with this program; if not, write to the Free Software "
"Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.";


/* Destruction of the application window */
static gboolean app_win_delete_handler(GtkWidget * widget, GdkEvent *event, 
				       gpointer data)      
{
	/* Quit on destruction */
	gtk_main_quit();
	return FALSE;
}

static void quit_program(GtkAction * action, gpointer data)      
{
	/* Quit on destruction */
	gtk_main_quit();
}



/* Credits dialog display */
static void about_display(GtkAction *action, gpointer data)
{
	GtkWidget *dialog = gtk_about_dialog_new();

	/* Program name */
	gtk_about_dialog_set_name(GTK_ABOUT_DIALOG(dialog), PACKAGE_NAME);
	/* Version */
	gtk_about_dialog_set_version(GTK_ABOUT_DIALOG(dialog), PACKAGE_VERSION);
	/* Copyright */
	gtk_about_dialog_set_copyright(GTK_ABOUT_DIALOG(dialog), copyright_str);
	/* License, which is long so word wrap */
	gtk_about_dialog_set_license(GTK_ABOUT_DIALOG(dialog), license_str);
	gtk_about_dialog_set_wrap_license(GTK_ABOUT_DIALOG(dialog), TRUE);

	/* Displaying */
	gtk_dialog_run(GTK_DIALOG(dialog));
	gtk_widget_destroy(dialog);
}

/* Error loading file display */
static void dialog_load_error(int err_code, const char *filename)
{
	GtkWidget *dialog = gtk_message_dialog_new(GTK_WINDOW(app_win), 
			GTK_DIALOG_MODAL,
			GTK_MESSAGE_ERROR, GTK_BUTTONS_CLOSE,
			"Error loading %s: %s", filename, get_error(err_code));


	gtk_dialog_run(GTK_DIALOG(dialog));
	gtk_widget_destroy(dialog);
}

/* Open file dialog display */
static void dialog_file_open(GtkAction *action, gpointer data)
{
	char *filename;
	int res;

	/* Using stock GTK filechooser widget */
	GtkWidget *dialog = gtk_file_chooser_dialog_new("Open file", 
			GTK_WINDOW(app_win),
			GTK_FILE_CHOOSER_ACTION_OPEN,
			GTK_STOCK_CANCEL, GTK_RESPONSE_CANCEL,
			GTK_STOCK_OPEN, GTK_RESPONSE_ACCEPT, NULL);

	if (gtk_dialog_run(GTK_DIALOG(dialog)) == GTK_RESPONSE_ACCEPT) {
		filename = gtk_file_chooser_get_filename(GTK_FILE_CHOOSER(
							 dialog));
	
		/* Try to load that file */
		res = manager_load_file(filename);
		if (res < 0)
			dialog_load_error(res, filename);

		g_free (filename);
	}

	gtk_widget_destroy(dialog);
}

/* UI actions creation */
static void create_actions(void)
{
	/* File input selection */
	ac_open_file = gtk_action_new("file/open", "Open", NULL, 
				      GTK_STOCK_OPEN);
	g_signal_connect(ac_open_file, "activate",
			 G_CALLBACK(dialog_file_open), NULL);

	/* Program exit */
	ac_quit = gtk_action_new("exit", "Quit", NULL, GTK_STOCK_QUIT);
	g_signal_connect(ac_quit, "activate", G_CALLBACK(quit_program), NULL);

	/* About dialog display */
	ac_about = gtk_action_new("credits", "About", NULL, GTK_STOCK_ABOUT);
	g_signal_connect(ac_about, "activate", G_CALLBACK(about_display), NULL);
}

/* Menubar creation */
static GtkWidget * create_menu(void)
{
	GtkWidget *item;
	GtkWidget *bar = gtk_menu_bar_new();

	/* File menu */
	item = gtk_menu_item_new_with_label("File");
	GtkWidget *file_menu = gtk_menu_new();

	gtk_menu_shell_append(GTK_MENU_SHELL(file_menu), 
			      gtk_action_create_menu_item(ac_open_file));
	gtk_menu_shell_append(GTK_MENU_SHELL(file_menu), 
			      gtk_action_create_menu_item(ac_quit));
	gtk_menu_item_set_submenu(GTK_MENU_ITEM(item), file_menu);

	gtk_menu_shell_append(GTK_MENU_SHELL(bar), item);

	/* Help menu */
	item = gtk_menu_item_new_with_label("Help");
	GtkWidget *help_menu = gtk_menu_new();

	gtk_menu_shell_append(GTK_MENU_SHELL(help_menu), 
			      gtk_action_create_menu_item(ac_about));
	gtk_menu_item_set_submenu(GTK_MENU_ITEM(item), help_menu);

	gtk_menu_shell_append(GTK_MENU_SHELL(bar), item);

	/* Done */
	return bar;
}

/* Creation of the main application window */
GtkWidget* create_app_win(void)
{
	GtkWidget *menu;
	GtkWidget *sep;

	/* Using it as a global */
	app_win = gtk_window_new(GTK_WINDOW_TOPLEVEL);

	if (!app_win) {
		 fputs("Can't create application window\n", stderr);
		 return NULL;
	}

	/* Can now setup this window */
	create_actions();

	/* Creating menu */
	menu = create_menu();

	/* Separating menu bar from rendering area */
	sep = gtk_vbox_new(FALSE, 0);
	gtk_box_pack_start(GTK_BOX(sep), menu, FALSE, TRUE, 0);
	gtk_widget_show_all(sep);

	/* Some app window signal configs */
	g_signal_connect(app_win, "delete-event", 
			 G_CALLBACK(app_win_delete_handler), NULL);

	/* Adding everything in the main window */
	gtk_container_add(GTK_CONTAINER(app_win), sep);

	return sep;
}
