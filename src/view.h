#ifndef VIEW_H
#define VIEW_H

#include "model.h"
#include "primitives.h"

#define VIEW_NEAR	0.125
#define VIEW_FAR	((1<<16)-1.0)
#define ZOOM_FACTOR	0.8

struct view {
	vec3	center;			/* View target */
	vec3	eye;			/* Viewer position */
	
	angle_t	angle;			/* Orientation angle */
	vec3	axis;			/* Orientation rotation */
	coord_t	target_dist;		/* Target distance from center */

	/* Viewer Axis */
	vec3	side;			/* X local dir */
	vec3	up;			/* Y local dir */
	vec3	dir;			/* Front direction (-Z local dir) */
};

/* Defined viewpoint for navigation */
struct viewpoint {
	struct obj	obj;
	vec3		center;		/* Rotation center */
	vec3		orientation;	/* Rotation axis */
	vec3		pos;		/* Viewer's position */
	angle_t		rot;		/* Rotation relative to orientation */
	char		*desc;		/* Description */
	angle_t		fovy;		/* Field of View */
	int		nojump;
};

/* Update of the view projection */
void viewpoint_update(struct viewpoint *vp);
/* Setting of view parameters for the specified viewpoint */
void viewpoint_set(struct viewpoint *vp);

/* Setting of the view from the current scene descritpion */
void view_set(void);
void view_pan(double dx, double dy);
void view_spin(double x0, double y0, double x1, double y1);
void view_zoom(double fact);

extern const struct obj_comp viewpoint_comp;

extern const struct obj_type viewpoint_type;

#endif	/* VIEW_H */
