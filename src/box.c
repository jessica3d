#include "jessica.h"
#include "geometry.h"
#include "rendering.h"

/* Box display list */
static GLuint disp_list;

/* Rendering of a box face */
static void render_face()
{
	glBegin(GL_QUADS);
		glNormal3d(0, 0, 1);
		glTexCoord2d(0, 0);
		glVertex3d(-1, -1, 1);

		glTexCoord2d(1, 0);
		glVertex3d(1, -1, 1);

		glTexCoord2d(1, 1);
		glVertex3d(1, 1, 1);

		glTexCoord2d(0, 1);
		glVertex3d(-1, 1, 1);
	glEnd();
}

/* Rendering of the box object */
void box_rendergeom(struct box *box)
{
	glPushMatrix();

	/* Size adjust */
	glScaled(box->size.c.x/2, box->size.c.y/2, box->size.c.z/2);

	glCallList(disp_list);

	/* Done */
	glPopMatrix();
}

/* Box default value settings */
static void box_init(struct box *bx)
{
	bx->size = vec3_c(2, 2, 2);
}

/* Rendering setup */
static void box_setup(struct box *bx)
{
	/* All boxes use the same display list */
	if (disp_list)
		return;

	/* Box display list creation */
	disp_list = glGenLists(1);
	glNewList(disp_list, GL_COMPILE);

	/* Drawing box */
	render_face();
	glPushMatrix();
	for (int i = 0; i < 3; i++) {
		glRotated(90, 0, 1, 0);
		render_face();
	}

	glPopMatrix();

	/* Top Face */
	glPushMatrix();
	glRotated(-90, 1, 0, 0);
	render_face();

	/* Bottom Face */
	glPopMatrix();
	glRotated(90, 1, 0, 0);
	render_face();
	glPopMatrix();

	glEndList();
}

/* Box dynamic table setup */
static void box_dyn_setup(struct box_dynt *dynt)
{
	((struct geom_dynt*)dynt)->setup = 
			(geom_setup_func)box_setup;
	((struct geom_dynt*)dynt)->rendergeom = 
			(geom_rendergeom_func)box_rendergeom;
}

/* Box component */
const struct obj_comp box_comp = {
	.init = (init_func)box_init,
	.dyn_setup = (dyn_setup_func)box_dyn_setup
};

/* Box dynamic functions */
struct box_dynt box_dynt;


