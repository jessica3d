#ifndef INTERP_H
#define INTERP_H

/* DBL_EPSILON */
#include <float.h>

#include "event.h"

/* Linear lerpolator base type */
struct lerp {
	struct obj	obj;
	int		num_key;	/* Number of lerpolating keys */
	double		*keys;		/* Interpolating keys */

	double		key;		/* Current key value */
};

struct lerp_dynt {
	void (*eval)(struct lerp* lrp);
};

/* Interpolator value evaluation */
static inline void lerp_eval(struct lerp *lrp)
{ ((struct lerp_dynt*)((struct obj*)lrp)->type->dynt)->eval(lrp); 
  event_emit((struct obj*)lrp, obj_attrib((struct obj*)lrp, "value_changed")); }
typedef void(*lerp_eval_func)(struct lerp* lrp);

/* Orientation/Rotation lerpolator */
struct quat_lerp {
	struct lerp	lerp;		/* Base type */
	quat		*values;	/* List of quaternion values */

	quat		value;		/* Current interpolation value */
};

struct quat_lerp_dynt {
	struct lerp_dynt	lerp_dynt;
} extern quat_lerp_dynt;

/* 3D Coordinate lerpolator */
struct vec3_lerp {
	struct lerp	lerp;
	vec3		*values;
};

struct vec3_lerp_dynt {
	struct lerp_dynt	lerp_dynt;
} extern vec3_lerp_dynt;

/* 2D Coordinate lerpolator */
struct vec2_lerp {
	struct lerp	lerp;
	vec2		*values;
};

struct vec2_lerp_dynt {
	struct lerp_dynt	lerp_dynt;
} extern vec2_lerp_dynt;

/* Normal lerpolator */
struct normal_lerp {
	struct lerp	lerp;
	vec3		*values;
};

struct normal_lerp_dynt {
	struct lerp_dynt	lerp_dynt;
} extern normal_lerp_dynt;

/* Scalar lerpolator */
struct scalar_lerp {
	struct lerp	lerp;
	double		*values;
};

struct scalar_lerp_dynt {
	struct lerp_dynt	lerp_dynt;
} extern scalar_lerp_dynt;

/* Types and components */
extern const struct obj_type quat_lerp_type;
extern const struct obj_type normal_lerp_type;
extern const struct obj_type vec3_lerp_type;
extern const struct obj_type vec2_lerp_type;
extern const struct obj_type scalar_lerp_type;

extern const struct obj_comp lerp_comp;
extern const struct obj_comp quat_lerp_comp;
extern const struct obj_comp normal_lerp_comp;
extern const struct obj_comp vec3_lerp_comp;
extern const struct obj_comp vec2_lerp_comp;
extern const struct obj_comp scalar_lerp_comp;

#endif	/* INTERP_H */
