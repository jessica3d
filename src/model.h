#ifndef MODEL_H
#define MODEL_H

/* size_t, NULL */
#include <stddef.h>

/* strcmp(3) */
#include <string.h>

#include "list.h"
#include "primitives.h"

#define ARRAY(t)	struct { int num; t arr; }

#define OBJ_STATIC_MASK		(1<<(((sizeof(int)-1)<<3)+7))

/* Object Model */
/* Terminal type specification structure  */
struct obj_type {
	const char		*name;		/* Object type name */
	size_t			obj_size;	/* Object data size */
	void			*dynt;		/* Dynamic functions table */
	int			num_comp;	/* Number of components */
	const struct obj_comp	*comp[];	/* Components */
};

/* Base members of all object-derived types */
struct obj {
	char			*name;	/* Object name, _if_ named... */
	const struct obj_type	*type;	/* Object type, required */
	int			refs;	/* Number of references, when named */
};

/* Object type component specification */
struct obj_comp {
	void (*init)(struct obj *obj);
	void (*destroy)(struct obj *obj);
	void (*dyn_setup)(void *dynt);
	const struct obj_att	**attribs;	/* Attributes table */
};

typedef void(*init_func)(struct obj *);
typedef void(*destroy_func)(struct obj *);
typedef void(*dyn_setup_func)(void *);

/* Base component of all object-derived types */
extern const struct obj_comp base_comp;
void model_init(void);

/* Basic Object manipulation routines, calling initialisation 
 * and destroy routines */
struct obj*	obj_create(const struct obj_type* type);
void		obj_static_create(struct obj* obj, const struct obj_type* type);
void		obj_destroy(struct obj* obj);


/* Object references: used only for X3D types manipulation */
static inline int obj_refcnt(const struct obj* obj)
{ return obj->refs & OBJ_STATIC_MASK; }

static inline void obj_ref(struct obj* obj)
{ obj->refs++; }

static inline struct obj* obj_unref(struct obj *obj)
{ 
	int ref_cnt = (obj->refs & (~OBJ_STATIC_MASK))-1;
	if (ref_cnt) {
		obj->refs--;
		return obj;
	}

	obj_destroy(obj);
	return NULL;
}

/* ... Returns true when when the object has a component of the type comp */
int obj_of_type(struct obj* obj, const struct obj_comp *comp);

/* Object table to find those which are named */
struct obj*	fetch_obj(const char *name);
void		register_obj(struct obj *obj);
struct obj*	unregister_obj(const char *name);

/* Object attribute query */
const struct obj_att* obj_attrib(struct obj *obj, const char *att_name);

#endif	/* MODEL_H */
