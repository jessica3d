/* Output */
#include <stdio.h>

#include "primitives.h"

/* Product of a quaternion by a vector */
vec3 quat_mult_vec3(const quat *a, const vec3 *v)
{
	quat qv = quat_c(v->c.x, v->c.y, v->c.z, 0);
	quat res = quat_grassman(*a, quat_grassman(qv, quat_inv(*a)));
	return vec3_c(res.c.x, res.c.y, res.c.z);
}

/* Quaternion spherical linear interpolation (slerp) 
 * Reference:	Jonathan Blow
 *		Understanding Slerp, Then Not Using It
 *		The Inner Product, April 2004 */
quat quat_slerp(const quat *a, const quat *b, double t)
{
	quat r;
	/* Dot product
	 * As the base is orthonormal, dot is the cosine of angle between */
	double dot = quat_dot(*a, *b);
	double sine = sqrt(1 - dot * dot);

	/* Linear interpolation when nearly colinear */
	if (fabs(dot) > 0.9995) {
		r = quat_add(*a, quat_scale(quat_sub(*b, *a), t));
		return quat_normalize(r);
	}

	/* New angle between quats */
	double ang = acos(dot);

	/* Interpolating */
	double scales[2];
	scales[0] = sin((1 - t) * ang) / sine;
	scales[1] = sin(t * ang) / sine;

	/* Rotating the first */
	r = quat_add(quat_scale(*a, scales[0]), quat_scale(*b, scales[1]));
	return quat_normalize(r);
}

void printquat(const quat v, const char *s)
{
	if (s)
		printf ("%s: [ %g %g %g %g ]\n", s, v.v[0], v.v[1], v.v[2], 
			v.v[3]);
	else
		printf ("[ %g %g %g %g ]\n", v.v[0], v.v[1], v.v[2], v.v[3]);
}

void printrot(const quat v, const char *s)
{
	vec3 ax = quat_axis(v);
	angle_t ang = quat_angle(v);
	if (s)
		printf ("%s: ( %g [ %g %g %g ])\n", s, ang, ax.v[0], ax.v[1], 
				ax.v[2]);
	else
		printf ("( %g [ %g %g %g ])\n", ang, ax.v[0], ax.v[1], 
				ax.v[2]);
}
