#ifndef GEOMETRY_H
#define GEOMETRY_H

/* intptr_t */
#include <stdint.h>

#include "model.h"
#include "primitives.h"
#include "color.h"
#include "rendering.h"

struct geom {
	struct obj	obj;	/* Base */
	int		cull;	/* Backface culling */
	int		ready;	/* Setup status */
};

struct geom_dynt {
	void	(*setup)(struct geom*);		/* Setup routine */
	void	(*rendergeom)(struct geom*);	/* Render function */
};

/* Common rendering settings */
void geom_begin(struct geom* gm);
void geom_end(struct geom* gm);

/* Render function to be called by external entities */
static inline void geom_render(struct geom* gm)
{	
	geom_begin(gm);
	((struct geom_dynt*)((struct obj*)gm)->type->dynt)->rendergeom(gm);
	geom_end(gm);
}

typedef void(*geom_rendergeom_func)(struct geom *);

static inline void geom_setup(struct geom* gm)
{ if (!gm->ready) ((struct geom_dynt*)((struct obj*)gm)->type->dynt)->setup(gm);
	gm->ready = 1; }
typedef void(*geom_setup_func)(struct geom *);

/* 3D Geometry types */
struct box {
	struct geom	geom;
	vec3		size;
};

struct box_dynt {
	struct geom_dynt	geom_dynt;
} extern box_dynt;

struct cone {
	struct geom	geom;
	coord_t		radius;
	coord_t		height;
	int		bottom;
	int		side;
};

struct cone_dynt {
	struct geom_dynt	geom_dynt;
} extern cone_dynt;

struct cylinder {
	struct geom	geom;
	coord_t		radius;		/* Side radius */
	coord_t		height;		/* Cylinder height */
	int		bottom;		/* Render bottom flag */
	int		top;		/* Render top flag */
	int		side;		/* Render side flag */
};

struct cylinder_dynt {
	struct geom_dynt	geom_dynt;
} extern cylinder_dynt;

struct sphere {
	struct geom	geom;
	coord_t		radius;
};

struct sphere_dynt {
	struct geom_dynt	geom_dynt;
} extern sphere_dynt;

enum mesh_attribs { MESH_NORMALS, MESH_COLORS, MESH_TEX_COORDS, 
		    MESH_FOG_COORDS };

/* Only supporting triangle face sets 
 * TODO: Figure a way to render every X3D mesh efficiently */
struct face {
	int	v[3];	
};

struct mesh {
	struct geom	geom;
	unsigned	attribs;	/* Enabled vertex attributes */
	struct face	*faces;
	vec3		*vertices;
	vec3		*normals;
	vec2		*tex_coords;
	color3		*colors;
	coord_t		*fog_coords;
	int		num_vertex;
	int		num_face;
	GLuint		elements_vbo;
	GLuint		data_vbo;
	intptr_t	normal_off;
	intptr_t	tex_coord_off;
	intptr_t	color_off;
	intptr_t	fog_coord_off;
};

struct mesh_dynt {
	struct geom_dynt	geom_dynt;
} extern mesh_dynt;

/* Geometry terminal types */
extern const struct obj_type box_type;
extern const struct obj_type cone_type;
extern const struct obj_type cylinder_type;
extern const struct obj_type sphere_type;
extern const struct obj_type mesh_type;

/* Geometry components */
extern const struct obj_comp geom_comp;
extern const struct obj_comp box_comp;
extern const struct obj_comp cone_comp;
extern const struct obj_comp cylinder_comp;
extern const struct obj_comp sphere_comp;
extern const struct obj_comp mesh_comp;



#endif	/* GEOMETRY_H */
