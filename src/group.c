/* intptr_t */
#include <stdint.h>

#include "jessica.h"
#include "group.h"
#include "rendering.h"
#include "manager.h"
#include "lighting.h"
#include "attrib.h"

/* Group initialization */
static void group_init(struct group *grp)
{
	/* Default is no transform */
	grp->rotation = quat_rot(vec3_k, 0);
	grp->scale = vec3_c(1, 1, 1);
	grp->scale_rot = quat_rot(vec3_k, 0);
}

/* Setting of ligths of the render hierarchy */
static void update_lights(struct group *grp)
{
	unsigned mine = grp->lights;
	for (int i = 0; mine; i++, mine>>=1) {
		if (!(mine & 1))
			continue;

		if (!scene->lights[i]->enabled)
			continue;

		/* Updating position */
		light_update(scene->lights[i], i);
		/* Some lights are only enabled on a render hierarchy */
		if (scene->lights[i]->local)
			glEnable(GL_LIGHT0+i);
	}
}

/* Disabling of local lights */
static void close_lights(struct group *grp)
{
	unsigned mine = grp->lights;
	for (int i = 0; mine; i++, mine>>=1) {
		if (!(mine & 1))
			continue;

		if (scene->lights[i]->enabled && scene->lights[i]->local)
			continue;

		glDisable(GL_LIGHT0+i);
	}
}

/* Rendering of a transform hierarchy */
void group_render(struct group *grp)
{
	stack_push(&render_ctx.hierarchy, grp);

	struct list *lst = grp->children;

	/* Setting the transform */
	glPushMatrix();
	
	/* Rotation center removal */
	glTranslated(-grp->center.c.x, -grp->center.c.y, -grp->center.c.z);

	/* Scale orientation removal */
	angle_t sang = quat_angle(grp->scale_rot)*180/M_PI;
	vec3 saxis = quat_axis(grp->scale_rot);
	glRotated(-sang, saxis.c.x, saxis.c.y, saxis.c.z);

	/* Scale */
	glScaled(grp->scale.c.x, grp->scale.c.y, grp->scale.c.z);

	/* Scale orientation */
	glRotated(sang, saxis.c.x, saxis.c.y, saxis.c.z);

	/* Rotation */
	vec3 axis = quat_axis(grp->rotation);
	glRotated(quat_angle(grp->rotation)*180/M_PI, 
		  axis.c.x, axis.c.y, axis.c.z);

	/* Rotation center */
	glTranslated(grp->center.c.x, grp->center.c.y, grp->center.c.z);

	/* Translation */
	glTranslated(grp->transl.c.x, grp->transl.c.y, grp->transl.c.z);

	/* Setting lighting */
	if (grp->lights)
		update_lights(grp);

	/* Render all of its element */
	while(lst) {
		renderable_render((struct renderable*)lst->data);
		lst = lst->next;
	}

	/* Over with local lights */
	if (grp->lights)
		close_lights(grp);

	glPopMatrix();

	stack_pop(&render_ctx.hierarchy);
}

/* Setup of a transform hierarchy */
void group_setup(struct group *grp)
{
	/* Setting up all children */
	struct list *lst = grp->children;
	while (lst) {
		renderable_setup((struct renderable*)lst->data);
		lst = lst->next;
	}
}

/* Group dynamic table setup */
void group_dyn_setup(struct group_dynt *dynt)
{
	dynt->renderable_dynt.render = (renderable_render_func)group_render;
	dynt->renderable_dynt.setup = (renderable_setup_func)group_setup;
}

const struct obj_att group_translation_att = {
	.name = "translation",
	.off = offsetof(struct group, transl),
	.size = sizeof(vec3),
	.get = get_vec3,
	.set = set_vec3
};

const struct obj_att group_center_att = {
	.name = "center",
	.off = offsetof(struct group, center),
	.size = sizeof(vec3),
	.get = get_vec3,
	.set = set_vec3
};

const struct obj_att group_rotation_att = {
	.name = "rotation",
	.off = offsetof(struct group, rotation),
	.size = sizeof(quat),
	.get = get_quat,
	.set = set_quat
};

const struct obj_att group_scale_orientation_att = {
	.name = "scaleOrientation",
	.off = offsetof(struct group, scale_rot),
	.size = sizeof(quat),
	.get = get_quat,
	.set = set_quat
};

const struct obj_att group_scale_att = {
	.name = "scale",
	.off = offsetof(struct group, scale),
	.size = sizeof(vec3),
	.get = get_vec3,
	.set = set_vec3
};

static const struct obj_att *group_atts[] = {
	&group_center_att,
	&group_rotation_att,
	&group_scale_att,
	&group_scale_orientation_att,
	&group_translation_att,
	NULL
};

/* Group component */
const struct obj_comp group_comp = {
	.init = (init_func)group_init,
	.dyn_setup = (dyn_setup_func)group_dyn_setup,
	.attribs = group_atts
};

/* Dynamic functions */
struct group_dynt group_dynt;

const struct obj_comp renderable_comp;

