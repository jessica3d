#include "jessica.h"
#include "scene.h"
#include "color.h"
#include "lighting.h"
#include "sensor.h"

/* Scene structures initialisation */
void scene_init(struct scene *scn)
{
}

/* Scene structures destruction */
void scene_destroy(struct scene *scn)
{
	obj_destroy((struct obj*)&scn->content);
}

/* Rendering of the scene in the current context */
void scene_render(struct scene *scn)
{
	/* Start the hierarchy traversal */
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	
	group_render(&scn->content);
}

/* Scene setup for the current rendering context */
void scene_setup(struct scene *scn)
{
	int got_light = 0;
	pglerror ("scene setup");

	/* First setting size */
	scene_resize(scn);
	glClearColor(0.0, 0.0, 0.0, 0.0);

	/* Using default viewpoint when none defined */
	if (!stack_top(&scn->viewpoints))
		stack_push(&scn->viewpoints, obj_create(&viewpoint_type));

	/* Configuring view */
	viewpoint_set((struct viewpoint*)stack_top(&scn->viewpoints));
	view_set();

	/* Enabling global lights */
	for (int i = 0; i < MAX_LIGHT; i++) {
		if (!scn->lights[i])
			continue;

		got_light = 1;
		light_setup(scn->lights[i], i);
		if (!scn->lights[i]->local)
			glEnable(GL_LIGHT0+i);
	}

	/* Use default headlight when none is defined */
	if (!got_light) {
		static const GLfloat default_pos[] = { 0, 0, 1, 0 };
		glPushMatrix();
		glLoadIdentity();
		glLightfv(GL_LIGHT0, GL_POSITION, default_pos);
		glPopMatrix();
		glLightfv(GL_LIGHT0, GL_AMBIENT, color4_black_opaque.v);
		glLightfv(GL_LIGHT0, GL_DIFFUSE, color4_white_opaque.v);
		glLightfv(GL_LIGHT0, GL_SPECULAR, color4_white_opaque.v);
		glEnable(GL_LIGHT0);
	}

	/* Configuring the transform hierarchy */
	group_setup(&scn->content);
	pglerror("aff setup");

	/* Noting time */
	clock_gettime(CLOCK_REALTIME, &scn->setup_time);
}

/* Framebuffer resize notification */
void scene_resize(struct scene *scn)
{
	/* Noting new dimensions */
	glGetIntegerv(GL_VIEWPORT, (GLint*)&scn->vp);

	/* Updating the projection */
	if (stack_top(&scn->viewpoints)) 
		viewpoint_update((struct viewpoint*)stack_top(
					&scn->viewpoints));
}

/* Sensors query for requiered events generation */
void scene_eval_sensors(struct scene *scn)
{
	struct list *lst = scn->sensors;
	while (lst) {
		if (((struct sensor*)lst->data)->enabled)
			sensor_eval((struct sensor*)lst->data);
		lst = lst->next;
	}
}

