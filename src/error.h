#ifndef ERROR_H
#define ERROR_H

#include <limits.h>
#include <errno.h>

void set_error(const char *err_str);
const char* get_error(int err_code);

#define ERR_STR		INT_MIN
#define ERR_NOSTR	(INT_MIN+1)


#endif	/* ERROR_H */
