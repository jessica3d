#ifndef FILE_H
#define FILE_H

/* size_t */
#include <stddef.h>

/* Sets the prefix location for all local-loaded files */
void file_set_location(const char *);

/* Loads a file, returning NULL on error */
int file_load(const char *loc, char **data, size_t *size);

#endif	/* FILE_H */
