#include "jessica.h"
#include "group.h"
#include "shading.h"
#include "geometry.h"
#include "manager.h"


/* Rendering of a shape */
void shape_render(struct shape *shp)
{
	/* Registering the renderable */
	stack_push(&render_ctx.hierarchy, shp);

	/* Enabling shape-specific shading */
	if (shp->shading)
		shading_begin(shp->shading);
	else
		glDisable(GL_LIGHTING);

	/* Rendering the actual shape */
	geom_render(shp->geom);
	
	/* Restoring shading to default state */
	if (shp->shading)
		shading_end(shp->shading);
	else
		glEnable(GL_LIGHTING);

	stack_pop(&render_ctx.hierarchy);
}

/* Setup of a shape geometry */
void shape_setup(struct shape *shp)
{
	if (shp->shading)
		shading_setup(shp->shading);

	geom_setup(shp->geom);
}

/* Shape dynamic table setup */
static void shape_dyn_setup(struct shape_dynt *dynt)
{
	dynt->renderable_dynt.render = (renderable_render_func)shape_render;
	dynt->renderable_dynt.setup = (renderable_setup_func)shape_setup;
}

/* Shape component */
const struct obj_comp shape_comp = {
	.dyn_setup = (dyn_setup_func)shape_dyn_setup
};

/* Dynamic functions */
struct shape_dynt shape_dynt;
