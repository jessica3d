#include "jessica.h"
#include "lighting.h"
#include "rendering.h"

/* Base setup for all lights */
static void base_light_setup(struct light* lgt, int num)
{
	/* Setting light color parameters */
	/* Ambient */
	color4 col = color4_scale(lgt->color, lgt->ambient);
	glLightfv(GL_LIGHT0+num, GL_AMBIENT, col.v);

	/* Diffuse */
	col = color4_scale(lgt->color, lgt->intensity);
	glLightfv(GL_LIGHT0+num, GL_DIFFUSE, col.v);
}

/* Base light parameters initialization */
static void light_init(struct light *lgt)
{
	/* X3D Specification 17.3.1 */
	lgt->color = color4_white_opaque;
	lgt->intensity = 1;
	lgt->local = 1;
	lgt->enabled = 1;
}

/* Base light dynamic setup */
void light_dyn_setup(struct light_dynt *dynt)
{
	dynt->setup = base_light_setup;
}

/* Directional light parameters initialization */
static void dirlight_init(struct dirlight *lgt)
{
	/* X3D Specification 17.4.1 */
	lgt->dir = vec3_c(0, 0, -1);
}

/* Directional light position update */
static void dirlight_update(struct dirlight *lgt, int num)
{
	puts ("updateing");
	GLfloat pos[4] = { [3]=0 };
	for (int i = 0; i < 3; i++)
		pos[i] = lgt->dir.v[i];

	glLightfv(GL_LIGHT0+num, GL_POSITION, pos);
};

/* Directional light dynamic setup */
void dirlight_dyn_setup(struct dirlight_dynt *dynt)
{
	((struct light_dynt*)dynt)->update = (light_update_func)dirlight_update;
}

const struct obj_comp light_comp = {
	.init = (init_func)light_init,
	.dyn_setup = (dyn_setup_func)light_dyn_setup
};

const struct obj_comp dirlight_comp = {
	.init = (init_func)dirlight_init,
	.dyn_setup = (dyn_setup_func)dirlight_dyn_setup
};

const struct obj_comp spotlight_comp;
const struct obj_comp omnilight_comp;

struct dirlight_dynt dirlight_dynt;
struct spotlight_dynt spotlight_dynt;
struct omnilight_dynt omnilight_dynt;
