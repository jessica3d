#ifndef ATTRIB_H
#define ATTRIB_H

/* size_t */
#include <stddef.h>

#include "model.h"

typedef void(*get_func)(struct obj* src, size_t src_off, void* result);
typedef void(*set_func)(struct obj* dst, size_t dst_off, void* val);

struct obj_att {
	const char	*name;
	size_t		off;
	size_t		size;
	get_func	get;
	set_func	set;
};

/* Basic getters and setters */
void get_int(struct obj *src, size_t src_off, void *res);
void set_int(struct obj *dst, size_t dst_off, void *val);

void get_double(struct obj *src, size_t src_off, void *res);
void set_double(struct obj *dst, size_t dst_off, void *val);

#define get_timens	get_double
#define set_timens	set_double

void get_vec3(struct obj *src, size_t src_off, void *res);
void set_vec3(struct obj *dst, size_t dst_off, void *val);

void get_quat(struct obj *src, size_t src_off, void *res);
void set_quat(struct obj *dst, size_t dst_off, void *val);

#endif	/* ATTRIB_H */
