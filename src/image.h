#ifndef IMAGE_H
#define IMAGE_H

#include <GL/gl.h>
/* size_t */
#include <stddef.h>

#ifdef __cplusplus
extern "C" {
#endif

/* Image loading facilities */
/* PNG files */
int load_png_file(const char *filename, GLuint target, int *width, int *height);
int load_png_mem(const void *data, size_t size, GLuint target, 
		 int *width, int *height);

/* JPEG files */
int load_jpeg_file(const char *filename, GLuint target, 
		   int *width, int *height);
int load_jpeg_mem(const void *data, size_t size, GLuint target, 
		  int *width, int *height);

/* Targa files */
int load_targa_file(const char *filename, GLuint target, 
		    int *width, int *height);

/* Raw image, y-inverted (in OpenGL ref) */
int load_raw_mem(const void *data, GLuint target, int width, int height, 
		 int stride, GLenum gl_fmt, GLenum type);

#ifdef __cplusplus
}
#endif

#endif	/* IMAGE_H */
