/* Memory management */
#include <stdlib.h>

#include "jessica.h"
#include "fifo.h"

void* fifo_top(struct fifo *ff)
{
	if (!ff->num_data)
		return NULL;

	return ((void**)ff->chks->data)[ff->start_data&FIFO_CHK_MASK];
}

void* fifo_pop(struct fifo *ff)
{
	if (!ff->num_data)
		return NULL;

	/* Noting the top fifo value */
	void *val = ((void**)ff->chks->data)[ff->start_data++&FIFO_CHK_MASK];
	ff->num_data--;
	
	/* Resetting on last value read, keeping the used chunk */
	if (!ff->num_data) {
		ff->start_data = 0;
		return val;
	}

	/* Checking block complete use */
	ff->start_data &= FIFO_CHK_MASK;
	if (!ff->start_data) {
		/* Reuse only one chunk */
		if (ff->last_chk->next)
			free(list_remove(&ff->chks));
		else
			list_append(&ff->last_chk, list_remove(&ff->chks));
	}

	return val;
}

void fifo_push(struct fifo *ff, void *data)
{
	if (!ff->chks) {
		list_insert(&ff->chks, malloc(sizeof(void*)<<FIFO_CHK_EXP));
		ff->last_chk = ff->chks;
	}

	/* Need another chunk? */
	if (ff->num_data && 
	    !((ff->num_data + 1 + ff->start_data) & FIFO_CHK_MASK)) {
		if (!ff->last_chk->next)
			list_append(&ff->last_chk, 
				    malloc(sizeof(void*)<<FIFO_CHK_EXP));
		ff->last_chk = ff->last_chk->next;
	}

	((void**)ff->last_chk->data)[(ff->num_data++ + ff->start_data)
				     & FIFO_CHK_MASK] = data;
}

