#ifndef FONT_H
#define FONT_H

#include "jessica.h"
#if HAVE_PANGOCAIRO
#include <pango/pangocairo.h>
#endif

#include "geometry.h"
#include "rendering.h"

enum text_justifies { FONT_JUSTIFY_BEGIN=0, FONT_JUSTIFY_END, 
		      FONT_JUSTIFY_FIRST, FONT_JUSTIFY_MIDDLE };

enum text_styles { FONT_STYLE_PLAIN=0, FONT_STYLE_BOLD, FONT_STYLE_ITALIC,
		   FONT_STYLE_BOLDITALIC };

/* Text rendering specification */
struct text {
	struct geom	geom;

	int		num_string;	/* Number of text strings */
	char		**strings;	/* String data */
	coord_t		*lengths;	/* String lengths <=0 is no restrict*/

	coord_t		max_extent;	/* String Maximum length at which
					   every strings must be compressed 
					   <=0 is no restrict */
	coord_t		max_length;	/* Maximum render length */

	struct font	*font;		/* Used font specification */

	coord_t		width;		/* Display width */
	coord_t		height;		/* Display height */
	GLuint		tid;		/* GL texture object */
};

/* Font style specification */
struct font {
	struct obj	obj;
	char		lang[9];	/* Language */

	int		num_family;	/* Number of font families */
	char		**families;	/* Font family list */
	int		style;		/* Font style */
	int		justify[2];	/* Text justification in S and T */
	
	coord_t		size;		/* Rendering height size */
	coord_t		spacing;	/* Interval between baselines */

	int		horizontal;	/* Text orientation */
	int		right_to_left;	/* S Text direction */
	int		top_to_bottom;	/* T Text direction */

	/* Internal structures for Pango layout */
#ifdef HAVE_PANGOCAIRO
	PangoFontDescription	*pango_ft;
	PangoLanguage		*pango_lang;
#endif
};

int font_setup(struct font *ft);

/* Text dynamic functions */
struct text_dynt {
	struct geom_dynt	geom_dynt;
} extern text_dynt;

extern const struct obj_comp text_comp;
extern const struct obj_type text_type;

extern const struct obj_comp font_comp;
extern const struct obj_type font_type;

#endif	/* FONT_H */
