#include "view.h"
#include "manager.h"
#include "rendering.h"

/* Viewpoint default setting */
void viewpoint_init(struct viewpoint* vp)
{
	/* X3D Specification 23.3.5 */
	vp->fovy = M_PI/4;
	vp->orientation = vec3_k;
	vp->pos = vec3_c(0, 0, 10);
}

/* Update of a viewpoint projection, without jumping */
void viewpoint_update(struct viewpoint *vp)
{
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluPerspective(vp->fovy*180/M_PI, scene->vp.size[0] / 
			(double)scene->vp.size[1], VIEW_NEAR, VIEW_FAR);

	glMatrixMode(GL_MODELVIEW);
}

/* Setting of a viewpoint on the active view */
void viewpoint_set(struct viewpoint *vp)
{
	/* Setting the projection for viewpoint info */
	viewpoint_update(vp);

	/* Setting view parameters */
	mat4 disp = mat4_transl(&vp->center);
	mat4 rot = mat4_rot(vp->rot, &vp->orientation);
	rot = mat4_mult(&rot, &disp);
	vec3 negcenter = vec3_neg(vp->center);
	disp = mat4_transl(&negcenter);
	rot = mat4_mult(&disp, &rot);

	scene->view.center = vp->center;
	scene->view.eye = vp->pos;

	scene->view.angle = vp->rot;
	scene->view.axis = vp->orientation;

	scene->view.side = mat4_mult_vec3(&rot, &vec3_i);
	scene->view.up = mat4_mult_vec3(&rot, &vec3_j);
	vec3 negz = vec3_c(0, 0, -1);
	scene->view.dir = mat4_mult_vec3(&rot, &negz);

	scene->view.target_dist = vec3_norm(vec3_sub(scene->view.center,
						scene->view.eye ));
}

/* Setting of the view in the GL context */
void view_set()
{
	struct view *vw = &scene->view;
	//printvec3(&vw->eye, "eye");
	//printvec3(&vw->center, "center");
	//printvec3(&vw->up, "up");

	/* Only setting the camera's position */
	glLoadIdentity();
	//glRotated(-vw->angle*(180/M_PI), vw->axis.c.x, vw->axis.c.y, 
		  //vw->axis.c.z);
	//glTranslated(-vw->eye.c.x, -vw->eye.c.y, -vw->eye.c.z);
	gluLookAt(vw->eye.c.x, vw->eye.c.y, vw->eye.c.z,
		  vw->center.c.x, vw->center.c.y, vw->center.c.z,
		  vw->up.c.x, vw->up.c.y, vw->up.c.z);
}

/* View translation from the user's point of view */
void view_pan(double dx, double dy)
{
	struct view *vw = &scene->view;

	/* Taking distance from rotation center as displacement ref */
	double disp_fact = vec3_norm(vec3_sub(vw->center, vw->eye)) /
				scene->vp.size[1];

	/* Panning */
	vec3 disp = vec3_add(vec3_scale(vw->side, dx*disp_fact),
			     vec3_scale(vw->up, dy*disp_fact));
	vec3_sub_self(&vw->center, disp);
	vec3_sub_self(&vw->eye, disp);

	view_set();
}


/* Coordinate transform to a spaceball mapped on the screen */
static vec3 screen2spaceball(double x, double y)
{
	vec3 coord;

	/* Using y as reference radius */
	coord.c.x = (x-(scene->vp.size[0]>>1)) / (double)scene->vp.size[1];
	coord.c.y = (y-(scene->vp.size[1]>>1)) / (double)scene->vp.size[1];
	double norm2 = coord.c.x*coord.c.x + coord.c.y*coord.c.y;

	/* Depth clamping */
	if (norm2 < 0.995)
		coord.c.z = sqrt(1 - norm2);
	else
		coord.c.z = 0;

	/* Near center? */
	if (coord.c.z > 0.995)
		coord.c.z = 0.995;

	return vec3_normalize(coord);
}

/* View spinning around the rotation center */
void view_spin(double x0, double y0, double x1, double y1)
{
	vec3 last_pt = screen2spaceball(x0, y0);
	vec3 new_pt = screen2spaceball(x1, y1);

	/* Creating rotation axis */
	vec3 disp = vec3_sub(new_pt, last_pt);
	vec3 axis = vec3_normalize(vec3_cross(last_pt, disp));
	double dx = x1 - x0;
	double dy = y1 - y0;
	double angle = sqrt(dx*dx + dy*dy)*4/scene->vp.size[1];

	/* Converting to view coords */
	vec3 raxis = vec3_sub(vec3_add(vec3_scale(scene->view.side, axis.c.x), 
				       vec3_scale(scene->view.up, axis.c.y)), 
			      vec3_scale(scene->view.dir, axis.c.z));
	mat4 rot = mat4_rot(-angle, &raxis);

	/* Rotating */
	scene->view.up = mat4_mult_vec3(&rot, &scene->view.up);
	scene->view.dir = mat4_mult_vec3(&rot, &scene->view.dir);

	scene->view.side = vec3_normalize(vec3_cross(scene->view.dir, 
						     scene->view.up));
	scene->view.up = vec3_normalize(vec3_cross(scene->view.side, 
						   scene->view.dir));

	scene->view.eye = vec3_sub(scene->view.center, vec3_scale(
				   scene->view.dir, scene->view.target_dist));

	/* Everything's different now... (talking to self) */
	view_set();
}

/* Zoom of the view */
void view_zoom(double fact)
{
	/* Only the eye position changes on zoom*/
	scene->view.target_dist *= fact;

	/* Don't zoom too much */
	if (scene->view.target_dist < VIEW_NEAR)
		scene->view.target_dist = VIEW_NEAR;
	else if (scene->view.target_dist > VIEW_FAR)
		scene->view.target_dist = VIEW_FAR;

	scene->view.eye = vec3_sub(scene->view.center, vec3_scale(
				   scene->view.dir, scene->view.target_dist));

	/* Applying mods */
	view_set();
}

const struct obj_comp viewpoint_comp = {
	.init = (init_func)viewpoint_init
};


