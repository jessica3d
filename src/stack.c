/* Memory management */
#include <stdlib.h>

#include "stack.h"

void* stack_top(struct stack *stk)
{
	if (!stk->chks || !stk->num_data)
		return NULL;

	return ((void**)stk->chks->data)[stk->num_data&STACK_CHK_MASK];
}

void* stack_pop(struct stack *stk)
{
	if (!stk->num_data || !stk->chks)
		return NULL;

	/* Noting the top stack value */
	void *val = ((void**)stk->chks->data)[stk->num_data-- & STACK_CHK_MASK];

	/* Policy is keeping a "spare" chunk for to reduce memory alloc */
	if ((stk->num_data&STACK_CHK_MASK) == (1<<STACK_CHK_EXP)-STACK_SPARE && 
	     stk->chks->prev)
		free(list_remove(&stk->chks->prev));

	return val;
}

void stack_push(struct stack *stk, void *data)
{
	if (!stk->chks) {
		list_insert(&stk->chks, malloc(sizeof(void*)<<STACK_CHK_EXP));
		stk->num_data = 0;
	}

	stk->num_data++;
	/* Need another chunk? */
	if (!(stk->num_data & STACK_CHK_MASK)) {
		/* Might already be available */
		if (stk->chks->prev)
			stk->chks = stk->chks->prev;
		else
			list_insert(&stk->chks, 
				    malloc(sizeof(void*)<<STACK_CHK_EXP));
	}
	
	((void**)stk->chks->data)[stk->num_data & STACK_CHK_MASK] = data;
}

