#ifndef SHADING_H
#define SHADING_H

#include <float.h>

#include "list.h"
#include "model.h"
#include "color.h"

typedef float		coeff_t;

/* General renderable shading */
struct shading {
	struct obj		obj;
	struct filling		*filling;
	struct lining		*lining;
	struct material		*material;
	LIST(struct shader)	shaders;
	struct texture		*texture;
	LIST(struct transf)	tex_transfs;
};

/* Shading control */
void shading_setup(struct shading *shd);
void shading_begin(struct shading *shd);
void shading_end(struct shading *shd);


/* Shading properties */
struct filling {
	struct obj	obj;
	int		hatch_style;	/* Negative to disable */
	color3		hatch_color;
};

/* Line styles when rendering wires */
struct lining {
	struct obj	obj;
	int		disabled;	/* Lining enable */
	int		line_style;	/* Line type */
	double		width_scale;	/* <= 0 for smallest possible */
};

enum line_styles { LINE_SOLID=1, LINE_DASHED, LINE_DOTTED, LINE_DASHED_DOTTED,
		   LINE_DASH_DOT_DOT, LINE_SINGLE, LINE_SINGLE_DOT, 
		   LINE_DOUBLE_ARROW, LINE_CHAIN_LINE, LINE_CENTER_LINE, 
		   LINE_HIDDEN_LINE, LINE_PHANTOM_LINE, LINE_BREAK_LINE1, 
		   LINE_BREAK_LINE2, LINE_USER };

struct material {
	struct obj	obj;
	coeff_t		ambient;	/* Ambient coeff */
	color4		color;		/* Opacity stored in alpha component */
	color4		specular;	/* Specular color */
	coeff_t		shininess;	/* Specular exponent */
	color4		emission;	/* Emissive color */
};

extern const struct obj_comp shading_comp;
extern const struct obj_comp lining_comp;
extern const struct obj_comp filling_comp;
extern const struct obj_comp material_comp;

extern const struct obj_type shading_type;
extern const struct obj_type lining_type;
extern const struct obj_type filling_type;
extern const struct obj_type material_type;

#endif	/* SHADING_H */
