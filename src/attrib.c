#include "jessica.h"
#include "model.h"
#include "attrib.h"
#include "sensor.h"

/* Integer getter and setter */
void get_int(struct obj *src, size_t src_off, void *res)
{ *(int*)res = *(int*)((char*)src + src_off); }

void set_int(struct obj *dst, size_t dst_off, void *val)
{ *(int*)((char*)dst + dst_off) = *(int*)val; }

/* vec3 getter and setter */
void get_vec3(struct obj *src, size_t src_off, void *res)
{ *(vec3*)res = *(vec3*)((char*)src + src_off); }

void set_vec3(struct obj *dst, size_t dst_off, void *val)
{ *(vec3*)((char*)dst + dst_off) = *(vec3*)val ; }

/* Quaternion getter and setter */
void get_quat(struct obj *src, size_t src_off, void *res)
{ *(quat*)res = *(quat*)((char*)src + src_off); }

void set_quat(struct obj *dst, size_t dst_off, void *val)
{ *(quat*)((char*)dst + dst_off) = *(quat*)val; }

/* Double value getter and setter */
void get_double(struct obj *src, size_t src_off, void *res)
{ *(timens_t*)res = *(timens_t*)((char*)src + src_off); }

void set_double(struct obj *dst, size_t dst_off, void *val)
{ *(timens_t*)((char*)dst + dst_off) = *(timens_t*)val; }



