#ifndef STACK_H
#define STACK_H

#include "list.h"

/* In power of 2 */
#define	STACK_CHK_EXP	3
#define	STACK_CHK_MASK	(((STACK_CHK_EXP+1)<<1)-1)
/* Threshold to keep a spare chunk */
#define STACK_SPARE	2

#define STACK(type)	struct stack

struct stack {
	int				num_data;
	LIST(void*[1<<STACK_CHK_EXP])	chks;
};

void* stack_top(struct stack *stk);

void* stack_pop(struct stack *stk);

void stack_push(struct stack *stk, void *data);

#endif	/* STACK_H */
