#include "jessica.h"
#include "network.h"

#if HAVE_LIBCURL
/* Network support */
#include <curl/curl.h>
#endif

/* isalnum(3) */
#include <ctype.h>

/* strstr(3) */
#include <string.h>

/* Memory management */
#include <stdlib.h>

/* Network handle */
#if HAVE_LIBCURL
static CURL *net;
#endif

static char err_buf[CURL_ERROR_SIZE];

struct recept_data {
	size_t	size;
	char	*data;
};

/* Tells if a location is remote */
int is_remote(const char *loc)
{
	/* Try to find the url startup */
	char *proto_end = strstr(loc, "://");

	if (!proto_end)
		return 0;

	/* See if the url start is valid */
	const char *proto = loc;
	while (proto != proto_end) {
		if (!isalnum(*proto))
			return 0;
		proto++;
	}

	return 1;
}

/* Data reception function */
static size_t receive(void *data, size_t size, size_t num_data, void *stream)
{
	struct recept_data *rec = (struct recept_data*)stream;
	size_t pos = rec->size;
	size_t rec_size = size*num_data;
	rec->size += rec_size;

	/* Allocating required mem */
	rec->data = (char*)realloc(rec->data, rec->size);
	if (!rec->data)
		return 0;

	memcpy(rec->data+pos, data, rec_size);
	return rec_size;
}

/* Network initialization */
int network_init(void)
{
#if HAVE_LIBCURL
	if (!net)
		net = curl_easy_init();

	/* There might be a problem */
	if (!net)
		return -1;

	/* Transfer setup */
	curl_easy_setopt(net, CURLOPT_NOSIGNAL, 1);
	curl_easy_setopt(net, CURLOPT_WRITEFUNCTION, receive);
	curl_easy_setopt(net, CURLOPT_ERRORBUFFER, err_buf);
#endif

	return 0;
}

/* Network data reception */
int network_receive(const char *url, char **data, size_t *size)
{
#if HAVE_LIBCURL
	printf ("fetching file: %s\n", url);
	CURLcode res;
	struct recept_data rec = {
		.size = 0,
		.data = NULL
	};

	/* Setting up the transfer */
	curl_easy_setopt(net, CURLOPT_URL, url);
	curl_easy_setopt(net, CURLOPT_WRITEDATA, &rec);

	/* GO! */
	res = curl_easy_perform(net);
	if (res) {
		/* Error getting data */
		set_error(err_buf);
		return ERR_STR;
	}

	if (size)
		*size = rec.size;

	*data = rec.data;
	return 0;
#else
	return -1;
#endif
}

