/* Memory management */
#include <stdlib.h>

/* strdup(3) */
#include <string.h>

#include "jessica.h"
#include "texture.h"
#include "image.h"
#include "file.h"
#include "manager.h"
#include "rendering.h"

// TODO: Add gif images support

/* Texture fetch and load */
static int texture2d_texsetup(struct texture2d* tx)
{
	int res;
	char *data = NULL;
	size_t size;

	/* Creating texture object */
	glGenTextures(1, &tx->tid);
	glBindTexture(GL_TEXTURE_2D, tx->tid);

	/* Setting rendering parameters */
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	if (tx->repeat_s)
		glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	else
		glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S,
				GL_CLAMP_TO_EDGE);

	if (tx->repeat_t)
		glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	else
		glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T,
				GL_CLAMP_TO_EDGE);

	/* Trying with all locations */
	int i;
	for (i = 0; i < ((struct texture*)tx)->num_loc; i++) {
		if (data)
			free(data);

		res = file_load(((struct texture*)tx)->locs[i], &data, &size);
		if (res < 0)
			continue;
	
		/* Trying all known file formats */
		/* PNG */
		res = load_png_mem(data, size, GL_TEXTURE_2D, 
			           &tx->width, &tx->height);
		if (!res)
			break;
		if (res < 0)
			continue;

		/* JPEG */
		res = load_jpeg_mem(data, size, GL_TEXTURE_2D, 
			            &tx->width, &tx->height);
		if (!res)
			break;
		if (res < 0)
			continue;
	}

	if (data)
		free(data);

	/* Setting power of two params */
	if (!out_ctx.npot) {
		tx->npot_scale[0] = tx->width / (double)to_pow2(tx->width);
		tx->npot_scale[1] = tx->height / (double)to_pow2(tx->height);
	}

	/* Over with texture setting */
	glBindTexture(GL_TEXTURE_2D, 0);

	/* Couldn't load the image */
	if (i == ((struct texture*)tx)->num_loc && i) {
		fprintf(stderr, "error loading texture %s\n", 
			((struct texture*)tx)->locs[0]);
		glDeleteTextures(1, &tx->tid);
		tx->tid = 0;
		return ERR_NOSTR;
	}

	return 0;
}

/* Common texture destructor */
static void texture_destroy(struct texture *tx)
{
	/* Clearing locations */
	if (tx->num_loc) {
		for (int i = 0; i < tx->num_loc; i++)
			free(tx->locs[i]);
		free(tx->locs);
	}
}

/* Texture 2d destructor */
static void texture2d_destroy(struct texture2d *tx)
{
	/* Clearing texture object */
	if (tx->tid)
		glDeleteTextures(1, &tx->tid);
}

/* Texture2D enabling */
static void texture2d_begin(struct texture2d *tx)
{
	glBindTexture(GL_TEXTURE_2D, tx->tid);
	if (!out_ctx.npot) {
		glMatrixMode(GL_TEXTURE);
		glPushMatrix();
		glScaled(tx->npot_scale[0], tx->npot_scale[1], 1);
		glMatrixMode(GL_MODELVIEW);
	}
}

/* Texture2D disabling */
static void texture2d_end(struct texture2d *tx)
{
	glBindTexture(GL_TEXTURE_2D, 0);
	if (!out_ctx.npot) {
		glMatrixMode(GL_TEXTURE);
		glPopMatrix();
		glMatrixMode(GL_MODELVIEW);
	}
}

/* Texture2D dynamic table setup */
static void texture2d_dyn_setup(struct texture2d_dynt *dynt)
{
	((struct texture_dynt*)dynt)->texsetup = (texture_texsetup_func)
						  texture2d_texsetup;
	((struct texture_dynt*)dynt)->begin = (texture_begin_func)
						texture2d_begin;
	((struct texture_dynt*)dynt)->end = (texture_end_func)
						texture2d_end;
}

/* Base texture component */
const struct obj_comp texture_comp = {
	.destroy = (destroy_func)texture_destroy
};

/* Texture2D component */
const struct obj_comp texture2d_comp = {
	.destroy = (destroy_func)texture2d_destroy,
	.dyn_setup = (dyn_setup_func)texture2d_dyn_setup
};

/* Texture2D dynamic functions */
struct texture2d_dynt texture2d_dynt;

