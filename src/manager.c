/* GTK and GDK structures */
#include <gtk/gtk.h>

/* Mutexes */
#include <pthread.h>

/* Memory management */
#include <stdlib.h>

/* intptr_t */
#include <stdint.h>

/* dirname(3) */
#include <libgen.h>

#include <time.h>

#include "jessica.h"
#include "manager.h"
#include "xml_x3d.h"
#include "rendering.h"
#include "stack.h"
#include "ui.h"
#include "file.h"
#include "event.h"

/* Rendering context */
struct output_context out_ctx;

/* Currently loaded scene */
struct scene *scene;

/* Refresh interval */
static const struct timespec refresh_interval = { 0, 10000000 };

struct framebuffer {
	GdkRectangle	viewport;
	int		yoff;
} static fb;

enum ctrl_states { STATE_NONE=0, STATE_PAN, STATE_SPIN };

static STACK(int state) ctrl_state;

/* Pointer anchor */
double anchor[2];

/* Current render hierarchy */
struct render_context render_ctx;

/* Back to front renderable list */
struct btf_rd {
	struct renderable	*rd;
	coord_t			depth;		/* Renderable depth */
	mat4			model_view;	/* Model_view matrix */
};
static LIST(struct btf_rd*) btf_list;

/* Update of the scene call */
static gboolean manager_update(gpointer data)
{
	/* Removing idle call when no scene setup */
	if (!scene)
		return FALSE;

	/* Waiting a little bit */
	nanosleep(&refresh_interval, NULL);

	/* Ask for an update of the scene */
	manager_need_update();

	/* Reschedule this idle */
	return TRUE;
}

/* Setup of the scene as the current one */
static void set_scene(struct scene *scn)
{
	if (scene) {
		scene_destroy(scene);
		free(scene);
	}

	/* Setting of the scene as the current one */
	scene = scn;
	if (scene) {
		scene_setup(scene);
		g_idle_add(manager_update, NULL);
	}
}

/* Loading of an X3D scene from a file */
int manager_load_file(const char *filename)
{
	int res;

	struct scene *scn = (struct scene*)calloc(1, sizeof(struct scene)); 
	scene_init(scn);

	res = xml_x3d_file_parse(filename, scn);
	if (res < 0)
		goto err_free;
	
	file_set_location(dirname((char*)filename));
	set_scene(scn);
		
	return 0;

err_free:
	free(scn);
	return res;
}

/* Queued events processing */
static void process_events(void)
{
	struct list *rt_lst;
	struct event *ev;
	struct route *rt;

	while ((ev = (struct event*)fifo_pop(&render_ctx.events))) {
		/* Looking for routes handling that event */
		rt_lst = scene->routes;
		while(rt_lst) {
			rt = (struct route*)rt_lst->data;
			if (ev->src == rt->src && ev->src_att == rt->src_att)
				route_event(rt);
			rt_lst = rt_lst->next;
		}
	}
}

/* Rendering of the scene */
static gboolean render(GtkWidget *widget, GdkEventExpose *event, gpointer data)
{
	struct btf_rd* rd;

	/* Setting up rendering context */
	if (!gdk_gl_drawable_gl_begin(out_ctx.win, out_ctx.ctx))
		return FALSE;

	if (scene) {
		render_ctx.btf_rendering = 0;

		/* Getting rendering time */
		clock_gettime(CLOCK_REALTIME, &render_ctx.time);
	
		scene_eval_sensors(scene);

		if (fifo_top(&render_ctx.events))
			process_events();

		scene_render(scene);

		/* Rendering back to front registered elements */
		render_ctx.btf_rendering = 1;
		glPushMatrix();
		while (btf_list) {
			rd = (struct btf_rd*)list_remove(&btf_list);
			glLoadMatrixd((GLdouble*)&rd->model_view);
			renderable_render(rd->rd);
			free(rd);
		}
		glPopMatrix();
	} else {
		glClearColor(0.0, 0.0, 0.0, 1.0);
		glClear(GL_COLOR_BUFFER_BIT);
	}
	
	/* End of rendering */
	gdk_gl_drawable_swap_buffers(out_ctx.win);
	gdk_gl_drawable_gl_end(out_ctx.win);

	/* Don't propagate */
	return TRUE;
}

/* Rendering area size change */
static gboolean resize(GtkWidget *widget, GdkEventConfigure *event,
		       gpointer data)
{
	fb.viewport.x = event->x;
	fb.viewport.y = 0;		/* Window coords are inverted */
	fb.viewport.width = event->width;
	fb.viewport.height = event->height;
	fb.yoff = event->y;

	if (!gdk_gl_drawable_gl_begin(out_ctx.win, out_ctx.ctx))
		return FALSE;

	/* Setting the rendering viewport */
	glViewport(fb.viewport.x, fb.viewport.y, 
		   fb.viewport.width, fb.viewport.height);
	/* Noticing the scene */
	if (scene)
		scene_resize(scene);

	gdk_gl_drawable_gl_end(out_ctx.win);

	/* Don't propagate */
	return TRUE;
}

/* Window to viewport y coord conversion */
static inline double ywin_to_vp(double y)
{
	return fb.viewport.height - y;
}

/* View pan */
static void ctrl_pan(GdkEventMotion* ev)
{
	view_pan(ev->x - anchor[0], anchor[1] - ev->y);

	anchor[0] = ev->x;
	anchor[1] = ev->y;
}

/* View spin */
static void ctrl_spin(GdkEventMotion* ev)
{
	/* Don't try spinning on a null angle... */
	if (anchor[0] == (int)ev->x && anchor[1] == (int)ev->y)
		return;

	view_spin(anchor[0], ywin_to_vp(anchor[1]), 
		  ev->x, ywin_to_vp(ev->y));

	anchor[0] = ev->x;
	anchor[1] = ev->y;
}

/* Scroll (mouse wheel) */
static gboolean scroll(GtkWidget *widget, GdkEventScroll* ev,
		       gpointer data)
{
	switch(ev->direction) {
		case GDK_SCROLL_UP:
			view_zoom(ZOOM_FACTOR);
			break;
		case GDK_SCROLL_DOWN:
			view_zoom(1/ZOOM_FACTOR);
			break;
		default:
			return FALSE;
	}

	return TRUE;
}


/* Pointer motion */
static gboolean pointer_motion(GtkWidget *widget, GdkEventMotion* ev,
			       gpointer data)
{
	void(*handler)(GdkEventMotion* ev) = NULL;

	switch((intptr_t)stack_top(&ctrl_state)) {
		case STATE_PAN:
			handler = ctrl_pan;
			break;
		case STATE_SPIN:
			handler = ctrl_spin;
			break;
	}

	if (handler) {
		handler(ev);
		return TRUE;
	}

	/* Didn't process */
	return FALSE;
}

/* Pointer button press */
static gboolean button_press(GtkWidget *widget, GdkEventButton *ev,
			     gpointer data)
{
	/* Anything that happens will need the coords anyway */
	anchor[0] = ev->x;
	anchor[1] = ev->y;

	if (ev->button == 2 && ev->state & GDK_CONTROL_MASK)
		stack_push(&ctrl_state, (void*)STATE_PAN);
	else if (ev->button == 2)
		stack_push(&ctrl_state, (void*)STATE_SPIN);
	else 
		/* Stay at the current state when no event is handled */
		stack_push(&ctrl_state, stack_top(&ctrl_state));

	/* Don't propagate */
	return TRUE;
}

/* Pointer button release */
static gboolean button_release(GtkWidget *widget, GdkEventButton *ev,
			       gpointer data)
{
	stack_pop(&ctrl_state);

	/* Don't propagate */
	return TRUE;
}

/* Setting of rendering parameters */
static int setup_rendering(void)
{
	if (!gdk_gl_drawable_gl_begin(out_ctx.win, out_ctx.ctx))
		return -1;
	
	/* Checking extension support */
	/* Non power of two textures */
	out_ctx.npot =gdk_gl_query_gl_extension("ARB_texture_non_power_of_two");

	/* Loading required extensions functions */
	if (bind_ext() < 0) {
		fputs("Error loading required GL extensions\n", stderr);
		return -1;
	}

	/* Using Z-buffering */
	glEnable(GL_DEPTH_TEST);

	/* Still using GL fixed functionality */
	/* Gouraud shading */
	glShadeModel(GL_SMOOTH);
	/* This is disabled in shaders anyway... */
	glEnable(GL_NORMALIZE);

	/* Simple Phong lighting model */
	glEnable(GL_LIGHTING);

	/* Texturing required for sure */
	glEnable(GL_TEXTURE_2D);

	return 0;
}

/* Setting of the manager */
int manager_setup(void)
{
	/* Plugging callbacks */
	g_signal_connect(render_win, "expose-event", G_CALLBACK(render), NULL);
	g_signal_connect(render_win, "configure_event", G_CALLBACK(resize),
			 NULL);
	
	/* Setting window callbacks */
	g_signal_connect(render_win, "button-press-event", 
			 G_CALLBACK(button_press), NULL);
	g_signal_connect(render_win, "button-release-event", 
			 G_CALLBACK(button_release), NULL);
	g_signal_connect(render_win, "motion-notify-event", 
			 G_CALLBACK(pointer_motion), NULL);
	g_signal_connect(render_win, "scroll-event", 
			 G_CALLBACK(scroll), NULL);

	gtk_widget_add_events(render_win, GDK_BUTTON_PRESS_MASK | 
					  GDK_BUTTON_RELEASE_MASK | 
					  GDK_BUTTON_MOTION_MASK);

	/* Setting fb size */
	GdkEventConfigure resize_spec;
	gdk_window_get_geometry(render_win->window, 
				&resize_spec.x, &resize_spec.y,
				&resize_spec.width, &resize_spec.height, NULL);
	resize(render_win, &resize_spec, NULL);

	/* Rendering environment */
	return setup_rendering();
}

/* Setting of a render request */
void manager_need_update(void)
{
	gdk_window_invalidate_rect(render_win->window, &fb.viewport, TRUE);
}

/* Render hierarchy back to front render setup */
void manager_back_to_front_register(coord_t depth)
{
	struct list *lst;
	struct btf_rd *rd = (struct btf_rd*)malloc(sizeof(struct btf_rd));

	glGetDoublev(GL_MODELVIEW_MATRIX, (GLdouble*)&rd->model_view);
	rd->rd = (struct renderable*)stack_top(&render_ctx.hierarchy);
	rd->depth = (rd->model_view.v[2].v[0]+rd->model_view.v[1].v[1]+
		     rd->model_view.v[2].v[2])*depth / rd->model_view.v[1].v[2];

	/* Finding the insert position */
	if (!btf_list || depth < ((struct btf_rd*)btf_list->data)->depth) {
		list_insert(&btf_list, rd);
		return;
	}

	lst = btf_list;
	while (lst->next) {
		if (((struct btf_rd*)lst->next->data)->depth > depth) {
			list_insert(&lst->next, rd);
			break;
		}
	}

	if (!lst->next)
		list_append(&lst, rd);
}

