/* memset(3) */
#include <string.h>

#include "primitives.h"
#include "jessica.h"


const mat4 mat4_id = { .v = {
	{ .v = { 1, 0, 0, 0 } },
	{ .v = { 0, 1, 0, 0 } },
	{ .v = { 0, 0, 1, 0 } },
	{ .v = { 0, 0, 0, 1 } }
} };

/* Matrix operations */

vec3 mat4_mult_vec3(const mat4 *m, const vec3 *v)
{
	vec3 r;
	double w;

	for (int i = 0; i < 3; i++)
		r.v[i] = m->v[0].v[i]*v->v[0];
	w = m->v[0].v[3]*v->v[0];

	for (int i = 1; i < 3; i++) {
		for (int j = 0; j < 3; j++)
			r.v[j] += m->v[i].v[j]*v->v[i];
		w += m->v[i].v[3]*v->v[i];
	}

	for (int j = 0; j < 3; j++)
		r.v[j] += m->v[3].v[j];
	w += m->v[3].v[3];
		
	// Normalize
	vec3_scale_self(&r, 1/w);

	return r;
}


/* Matrix multiply */
mat4 mat4_mult(const mat4 *a, const mat4 *b)
{
	mat4 r;
	for (int i = 0; i < 4; i++)
		for (int j = 0; j < 4; j++)
			r.v[i].v[j] = a->v[0].v[j]*b->v[i].v[0] + 
				a->v[1].v[j]*b->v[i].v[1] + 
				a->v[2].v[j]*b->v[i].v[2] + 
				a->v[3].v[j]*b->v[i].v[3];
	return r;
}

mat4 mat4_transl(const vec3 *disp)
{
	mat4 r = mat4_id;
	memcpy(&r.v[3], disp, sizeof(vec3));
	return r;
}

mat4 mat4_rot(angle_t angle, const vec3 *a)
{
	mat4 r;

	coord_t c = cos(angle);
	coord_t s = sin(angle);
	coord_t omc = 1-c;

	r.v[0].v[0] = a->v[0]*a->v[0]*omc+c;
	r.v[0].v[1] = a->v[1]*a->v[0]*omc+a->v[2]*s;
	r.v[0].v[2] = a->v[0]*a->v[2]*omc-a->v[1]*s;
	r.v[0].v[3] = 0;

	r.v[1].v[0] = a->v[0]*a->v[1]*omc-a->v[2]*s;
	r.v[1].v[1] = a->v[1]*a->v[1]*omc+c;
	r.v[1].v[2] = a->v[1]*a->v[2]*omc+a->v[0]*s;
	r.v[1].v[3] = 0;

	r.v[2].v[0] = a->v[0]*a->v[2]*omc+a->v[1]*s;
	r.v[2].v[1] = a->v[1]*a->v[2]*omc-a->v[0]*s;
	r.v[2].v[2] = a->v[2]*a->v[2]*omc+c;
	r.v[2].v[3] = 0;
	
	r.v[3].v[0] = 0;
	r.v[3].v[1] = 0;
	r.v[3].v[2] = 0;
	r.v[3].v[3] = 1;

	return r;
}

mat4 mat4_rotx(angle_t angle)
{
	mat4 r = mat4_id;

	angle_t c = cos(angle);
	angle_t s = sin(angle);

	r.v[1].v[1] = c;
	r.v[1].v[2] = s;
	r.v[2].v[1] = -s;
	r.v[2].v[2] = c;

	return r;
}


mat4 mat4_roty(angle_t angle)
{
	mat4 r = mat4_id;

	angle_t c = cos(angle);
	angle_t s = sin(angle);

	r.v[0].v[0] = c;
	r.v[0].v[2] = -s;
	r.v[2].v[0] = s;
	r.v[2].v[2] = c;

	return r;
}


mat4 mat4_rotz(angle_t angle)
{
	mat4 r = mat4_id;

	angle_t c = cos(angle);
	angle_t s = sin(angle);

	r.v[0].v[0] = c;
	r.v[0].v[1] = s;
	r.v[1].v[0] = -s;
	r.v[1].v[1] = c;

	return r;
}

mat4 mat4_scale(const vec3 *s)
{
	mat4 r;
	memset(&r, 0, sizeof(mat4));

	for (int i = 0; i < 3; i++)
		r.v[i].v[i] = s->v[i];
	r.v[3].v[3] = 1;

	return r;
}

void printmat4(const mat4* m, const char *s)
{
	int len = 0;
	if (s) {
		len = strlen(s)+1;
		printf ("%s:", s);
	}

	for (int i = 0; i < 4; i++) {
		if (i)
			for (int j = 0; j < len; j++)
				putchar(' ');
		for (int j = 0; j < 4; j++) {
			printf(" %g", m->v[j].v[i]);
		}
		putchar('\n');
	}
}
