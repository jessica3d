#ifndef UI_H
#define UI_H

/* Handlers */
gboolean app_win_delete_handler(GtkWidget * widget, GdkEvent *event, 
				gpointer data);

extern GtkWidget	*app_win;
extern GtkWidget	*render_win;

/* Creation of the application dialog */
GtkWidget* create_app_win(void);


#endif
