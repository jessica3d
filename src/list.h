#ifndef LIST_H
#define LIST_H

/* Simple doubly-linked list manip */

struct list {
	struct list	*next;
	struct list	*prev;
	void		*data;
};

#define LIST(type)	struct list*


void list_insert(struct list** lst, void *data);
void list_insert_list(struct list** lst, struct list *add);
void list_append(struct list** lst, void *data);
void list_append_list(struct list** lst, struct list *add);
void* list_remove(struct list** lst);


#endif	/* LIST_H */


