#ifndef SENSOR_H
#define SENSOR_H

#include "model.h"

typedef double timens_t;

/* Sensor base type */
struct sensor {
	struct obj	obj;
	int		enabled;		/* Sensor active */
};

/* Sensor dynamic function table */
struct sensor_dynt {
	void(*eval)(struct sensor*);
};
typedef void(*sensor_eval_func)(struct sensor*);
static inline void sensor_eval(struct sensor* sens)
{ ((struct sensor_dynt*)((struct obj*)sens)->type->dynt)->eval(sens); }

/* Time based value generator */
struct time_sensor {
	struct sensor	sensor;
	timens_t	cycle_interval;		/* Cycle interval */
	int		loop;			/* Reload on cycle end */

	double		value;			/* Current value */
};

struct time_sensor_dynt {
	struct sensor_dynt	sensor_dynt;
} extern time_sensor_dynt;

/* Types and object components */
extern const struct obj_comp sensor_comp;
extern const struct obj_comp time_sensor_comp;

extern const struct obj_type time_sensor_type;

extern const struct obj_att time_sensor_fraction_changed_att;

#endif	/* SENSOR_H */
