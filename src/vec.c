#include "jessica.h"
#include "primitives.h"

/* Vector primitives */
const vec3 vec3_i = { .v = VEC3_I };
const vec3 vec3_j = { .v = VEC3_J };
const vec3 vec3_k = { .v = VEC3_K };
const vec3 vec3_zero = { .v = VEC3_ZERO };

void printvec3(const vec3 v, const char *s)
{
	if (s)
		printf (" %s: [ %g %g %g ]\n", s, v.v[0], v.v[1], v.v[2]);
	else
		printf ("[ %g %g %g ]\n", v.v[0], v.v[1], v.v[2]);
}

