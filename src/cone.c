#include "jessica.h"
#include "geometry.h"
#include "rendering.h"

#define SIDE_PRECISION		10
#define HEIGHT_PRECISION	10

/* Cone display list */
static GLuint side_list;
static GLuint bottom_list;

static void render_bottom()
{
	static const double astep = M_PI/(2*SIDE_PRECISION);
	double ang = M_PI;
	
	vec3 c = { .c = { .y = -1 } };
	glBegin(GL_TRIANGLE_FAN);
	glNormal3d(0, -1, 0);
	for (int i = 0; i < 4*SIDE_PRECISION; i++) {
		c.c.x = cos(ang);
		c.c.z = sin(ang);

		glTexCoord2d(c.c.x*0.5 + 0.5, c.c.z*0.5 + 0.5);
		glVertex3dv(c.v);

		ang -= astep;
	}

	glEnd();
}

/* Rendering of a cone "face" side */
static void render_side()
{
	static const double astep = M_PI/(2*SIDE_PRECISION);
	static const double hstep = 2.0/HEIGHT_PRECISION;
	static const double rstep = 1.0/HEIGHT_PRECISION;
	static const double tstep[2] = { 0.25/SIDE_PRECISION, 
					 1.0/HEIGHT_PRECISION };

	/* Coordinates */
	double ang = M_PI/2;
	vec3 c[2] = { [1] = { .c = { .x = 0, .z = 1 } } };
	/* Normals */
	vec3 n[2] = { [1] = { .c = { .x = 0, .y = 0.5, .z = sqrt(3)*0.5 } } };

	/* Texture coordinates */
	double texs[2] = { [1] = 0 };
	double text;
	double r;

	for (int i = 0; i < SIDE_PRECISION; i++) {
		ang += astep;

		r = 1;
		c[1].c.y = -1;
		c[0] = c[1];
		c[1].c.x = cos(ang);
		c[1].c.z = sin(ang);

		n[0] = n[1];
		n[1].c.x = c[1].c.x*sqrt(3)*0.5;
		n[1].c.z = c[1].c.z*sqrt(3)*0.5;

		text = 0;
		texs[0] = texs[1];
		texs[1] += tstep[0];

		glBegin(GL_QUAD_STRIP);
		for (int j = 0; j <= HEIGHT_PRECISION; j++) {
			glTexCoord2d(texs[0], text);
			glNormal3dv(n[0].v);
			glVertex3d(c[0].c.x*r, c[0].c.y, c[0].c.z*r);

			glTexCoord2d(texs[1], text);
			glNormal3dv(n[1].v);
			glVertex3d(c[1].c.x*r, c[1].c.y, c[1].c.z*r);

			c[0].c.y += hstep;
			c[1].c.y += hstep;
			text += tstep[1];
			r -= rstep;
		}
		glEnd();
	}
}

/* Rendering of the cone object */
void cone_rendergeom(struct cone *cn)
{
	glPushMatrix();

	/* Size adjust */
	glScaled(cn->radius, cn->height/2, cn->radius);
	if (cn->side)
		glCallList(side_list);
	if (cn->bottom)
		glCallList(bottom_list);

	/* Done */
	glPopMatrix();
}

/* Cone default value settings */
static void cone_init(struct cone *cn)
{
	/* X3D Specification 13.3.3 */
	cn->height = 2;
	cn->radius = 1;
	cn->side = cn->bottom = 1;
}

/* Rendering setup */
static void cone_setup(struct cone *cn)
{
	/* All cones use the same display list */
	if (side_list)
		return;

	/* Cone display list creation */
	side_list = glGenLists(1);
	glNewList(side_list, GL_COMPILE);

	/* Drawing side */
	render_side();

	glMatrixMode(GL_TEXTURE);
	glPushMatrix();
	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();

	for (int i = 0; i < 3; i++) {
		glMatrixMode(GL_TEXTURE);
		glTranslated(0.25, 0, 0);
		glMatrixMode(GL_MODELVIEW);
		glRotated(90, 0, 1, 0);

		render_side();
	}

	glMatrixMode(GL_TEXTURE);
	glPopMatrix();
	glMatrixMode(GL_MODELVIEW);

	glEndList();

	/* Top Face */
	bottom_list = glGenLists(1);
	glNewList(bottom_list, GL_COMPILE);
	render_bottom();
	glEndList();
}

/* Cone dynamic table setup */
void cone_dyn_setup(struct cone_dynt *dynt)
{
	dynt->geom_dynt.setup = (geom_setup_func)cone_setup;
	dynt->geom_dynt.rendergeom = (geom_rendergeom_func)cone_rendergeom;
}

/* Cone component */
const struct obj_comp cone_comp = {
	.init = (init_func)cone_init,
	.dyn_setup = (dyn_setup_func)cone_dyn_setup
};

/* Cone dynamic functions */
struct cone_dynt cone_dynt;


