#ifndef FIFO_H
#define FIFO_H

#include "list.h"

/* In power of 2 */
#define	FIFO_CHK_EXP	4
#define	FIFO_CHK_MASK	(((FIFO_CHK_EXP+1)<<1)-1)

#define FIFO(type)	struct fifo

struct fifo {
	int				start_data;
	int				num_data;
	LIST(void*[1<<FIFO_CHK_EXP])	chks;
	struct list			*last_chk;
};

void* fifo_top(struct fifo *ff);

void* fifo_pop(struct fifo *ff);

void fifo_push(struct fifo *ff, void *data);

#endif	/* FIFO_H */
