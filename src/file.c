/* strdup(3) */
#include <string.h>

/* File read */
#include <stdio.h>

/* Memory management */
#include <stdlib.h>

#include "jessica.h"
#include "file.h"
#include "network.h"

static char *location;
static int location_len;

/* Sets the prefix location for local files */
void file_set_location(const char *prefix)
{
	if (location)
		free(location);
	location_len = strlen(prefix);
	location = (char*)malloc(location_len);
	memcpy(location, prefix, location_len);
}

/* Loads a file in memory */
int file_load(const char *loc, char **data, size_t *size)
{
	/* May have passed a URL */
	if (is_remote(loc))
		return network_receive(loc, data, size);

	int len = strlen(loc);
	char *local_loc = (char*)malloc(location_len + len + 2);
	memcpy(local_loc, location, location_len);
	local_loc[location_len] = '/';
	memcpy(local_loc+location_len+1, loc, len+1);

	/* The local location could _also_ be a URL */
	if (is_remote(local_loc))
		return network_receive(local_loc, data, size);

	/* This is local */
	FILE *f = fopen(local_loc, "rb");
	if (!f)
		return -errno;

	/* Taking file size */
	fseek(f, 0, SEEK_END);
	len = ftell(f);
	fseek(f, 0, SEEK_SET);
	*data = (char*)malloc(len);

	/* Reading what _can_ be read */
	len = fread(*data, 1, len, f);
	if (size)
		*size = len;

	fclose(f);
	return 0;
}

