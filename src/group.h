#ifndef GROUP_H
#define GROUP_H

#include "model.h"

/* Renderable common base */
struct renderable {
	struct obj	obj;
};

struct renderable_dynt {
	void	(*render)(struct renderable*);
	void	(*setup)(struct renderable*);
};
typedef void(*renderable_render_func)(struct renderable*);
typedef void(*renderable_setup_func)(struct renderable*);

/* Rendering of a renderable */
static inline void renderable_render(struct renderable* rd)
{ ((struct renderable_dynt*)((struct obj*)rd)->type->dynt)->render(rd); }
static inline void renderable_setup(struct renderable* rd)
{ ((struct renderable_dynt*)((struct obj*)rd)->type->dynt)->setup(rd); }

/* Render group */
struct group {
	struct renderable	renderable;	/* Base */
	LIST(struct renderable)	children;	/* Render hierarchy */

	/* Transforms */
	vec3			transl;		/* Translation */
	vec3			center;		/* Rotation center */
	quat			rotation;	/* Rotation */
	quat			scale_rot;	/* Scale orientation */
	vec3			scale;		/* Scale */

	unsigned	lights;			/* Local lights bitvec */
};

void group_render(struct group* grp);
void group_setup(struct group* grp);

struct group_dynt {
	struct renderable_dynt	renderable_dynt;
} extern group_dynt;

struct shape {
	struct renderable	renderable;	/* Base */
	struct shading		*shading;	/* Display info */
	struct geom		*geom;		/* Shape geometry description */
};

void shape_render(struct shape* shp);
void shape_setup(struct shape* shp);

struct shape_dynt {
	struct renderable_dynt	renderable_dynt;
} extern shape_dynt;

/* Registered types */
extern const struct obj_type group_type;
extern const struct obj_type shape_type;

/* Components */
extern const struct obj_comp renderable_comp;
extern const struct obj_comp shape_comp;
extern const struct obj_comp group_comp;

#endif	/* GROUP_H */
