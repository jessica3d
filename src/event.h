#ifndef EVENT_H
#define EVENT_H

/* Memory allocation */
#include <stdlib.h>

#include "manager.h"
#include "attrib.h"

/* Route between elements  */
struct route {
	struct obj		*src;
	const struct obj_att	*src_att;
	struct obj		*dst;
	const struct obj_att	*dst_att;
};

struct event {
	struct obj		*src;
	const struct obj_att	*src_att;
};

static inline void route_event(struct route *rt)
{
	char buf[rt->src_att->size];
	rt->src_att->get(rt->src, rt->src_att->off, buf);
	rt->dst_att->set(rt->dst, rt->dst_att->off, buf);
}

/* Event emission */
static inline void event_emit(struct obj* obj, const struct obj_att *att)
{
	struct event *ev = (struct event*)malloc(sizeof(struct event));
	ev->src = obj;
	ev->src_att = att;
	fifo_push(&render_ctx.events, ev);
}

#endif	/* EVENT_H */
