#include "jessica.h"
#include "geometry.h"
#include "rendering.h"

#define SIDE_PRECISION		10
#define HEIGHT_PRECISION	10

/* Sphere display list */
static GLuint sphere_list;

/* Rendering of a sphere "face" side */
static void render_side()
{
	static const double astep = M_PI/(2*SIDE_PRECISION);
	static const double rstep = M_PI/HEIGHT_PRECISION;
	static const double tstep[2] = { 0.25/SIDE_PRECISION, 
					 1.0/HEIGHT_PRECISION };

	/* Coordinates */
	double ang = M_PI/2;
	double rang;
	double r;
	vec3 c[2] = { [1] = { .c = { .x = 0, .z = -1 } } };
	vec3 n[2];

	/* Texture coordinates */
	double texs[2] = { [1] = 0 };
	double text;

	for (int i = 0; i < SIDE_PRECISION; i++) {
		ang += astep;
		rang = -M_PI*0.5;

		c[0] = c[1];
		c[1].c.x = cos(ang);
		c[1].c.z = -sin(ang);

		text = 0;
		texs[0] = texs[1];
		texs[1] += tstep[0];

		/* Don't trust the floating point precision */
		if (i == SIDE_PRECISION-1) {
			texs[1] = 0.25;
			c[1].c.x = -1;
			c[1].c.z = 0;
		}
		glBegin(GL_QUAD_STRIP);
		for (int j = 0; j <= HEIGHT_PRECISION; j++) {
			c[0].c.y = sin(rang);
			//text = (c[0].c.y+1) *0.5;
			r = cos(rang);
			n[0] = vec3_c(c[0].c.x*r, c[0].c.y, c[0].c.z*r);
			n[1] = vec3_c(c[1].c.x*r, c[0].c.y, c[1].c.z*r);

			glTexCoord2d(texs[0], text);
			glNormal3dv(n[0].v);
			glVertex3dv(n[0].v);

			glTexCoord2d(texs[1], text);
			glNormal3dv(n[1].v);
			glVertex3dv(n[1].v);

			rang += rstep;
			text += tstep[1];
		}
		glEnd();
	}
}

/* Rendering of the sphere object */
void sphere_rendergeom(struct sphere *sph)
{
	glPushMatrix();

	/* Size adjust */
	glScaled(sph->radius, sph->radius, sph->radius);
	glCallList(sphere_list);

	/* Done */
	glPopMatrix();
}

/* Sphere default value settings */
static void sphere_init(struct sphere *sph)
{
	/* X3D Specification 13.3.3 */
	sph->radius = 1;
}

/* Rendering setup */
static void sphere_setup(struct sphere *sph)
{
	/* All spheres use the same display list */
	if (sphere_list)
		return;

	/* Sphere display list creation */
	sphere_list = glGenLists(1);
	glNewList(sphere_list, GL_COMPILE);

	/* Drawing side */
	render_side();

	glMatrixMode(GL_TEXTURE);
	glPushMatrix();
	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();

	for (int i = 0; i < 3; i++) {
		glMatrixMode(GL_TEXTURE);
		glTranslated(0.25, 0, 0);
		glMatrixMode(GL_MODELVIEW);
		glRotated(90, 0, 1, 0);

		render_side();
	}

	glMatrixMode(GL_TEXTURE);
	glPopMatrix();
	glMatrixMode(GL_MODELVIEW);

	glEndList();
}

/* Sphere dynamic table setup */
void sphere_dyn_setup(struct sphere_dynt *dynt)
{
	dynt->geom_dynt.setup = (geom_setup_func)sphere_setup;
	dynt->geom_dynt.rendergeom = (geom_rendergeom_func)sphere_rendergeom;
}

/* Sphere component */
const struct obj_comp sphere_comp = {
	.init = (init_func)sphere_init,
	.dyn_setup = (dyn_setup_func)sphere_dyn_setup
};

/* Sphere dynamic functions */
struct sphere_dynt sphere_dynt;


