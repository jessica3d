/* Memory management */
#include <stdlib.h>

#include "jessica.h"
#include "geometry.h"
#include "rendering.h"

/* Mesh rendering */
static void mesh_rendergeom(struct mesh *msh)
{
	/* Drawing using created VBOs */
	glEnableClientState(GL_VERTEX_ARRAY);
	glBindBuffer(GL_ARRAY_BUFFER, msh->data_vbo);

	glVertexPointer(3, GL_DOUBLE, 0, (GLvoid*)0);

	/* Normal array */
	if (msh->normal_off && (msh->attribs & (1<<MESH_NORMALS))) {
		glNormalPointer(GL_DOUBLE, 0, (GLvoid*)msh->normal_off);
		glEnableClientState(GL_NORMAL_ARRAY);
	}

	/* Texture coordinates */
	if (msh->tex_coord_off && (msh->attribs & (1<<MESH_TEX_COORDS))) {
		glTexCoordPointer(2, GL_DOUBLE, 0,(GLvoid*)msh->tex_coord_off);
		glEnableClientState(GL_TEXTURE_COORD_ARRAY);
	}

	/* Color array */
	if (msh->color_off && (msh->attribs & (1<<MESH_COLORS))) {
		glColorPointer(2, GL_FLOAT, 0,(GLvoid*)msh->color_off);
		glEnableClientState(GL_COLOR_ARRAY);
	}

	/* Fog coordinates array */
	if (msh->fog_coord_off && (msh->attribs & (1<<MESH_FOG_COORDS))) {
		glColorPointer(1, GL_DOUBLE, 0,(GLvoid*)msh->color_off);
		glEnableClientState(GL_FOG_COORDINATE_ARRAY);
	}

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, msh->elements_vbo);
	glDrawElements(GL_TRIANGLES, msh->num_face*3, GL_UNSIGNED_INT, NULL);

	/* Disabling VBO */
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
}

/* Normal generation for a mesh */
static void gen_normals(struct mesh *msh)
{
	msh->normals = (vec3*)malloc(sizeof(vec3)*msh->num_vertex);

	vec3 sides[2];
	vec3 n;
	for (int i = 0; i < msh->num_face; i++) {
		sides[0] = vec3_sub(msh->vertices[msh->faces[i].v[2]], 
			            msh->vertices[msh->faces[i].v[0]]);
		sides[1] = vec3_sub(msh->vertices[msh->faces[i].v[1]], 
			            msh->vertices[msh->faces[i].v[0]]);
		n = vec3_normalize(vec3_cross(sides[1], sides[2]));
		for (int j = 0; j < 3; j++)
			msh->normals[msh->faces[i].v[j]] = n;
		printvec3(n, "Normal");
	}
}

/* Rendering setup */
static void mesh_setup(struct mesh *msh)
{
	GLsizei off = 0;
	GLsizei size = sizeof(vec3);

	/* Summing attrib size */
	/* Normals */
	size += sizeof(vec3);
	if (msh->colors)
		size += sizeof(color3);
	if (msh->tex_coords)
		size += sizeof(vec2);
	if (msh->fog_coords)
		size += sizeof(coord_t);
	size *= msh->num_vertex;

	/* Using a vertex buffer to render */
	glGenBuffers(1, &msh->data_vbo);
	glBindBuffer(GL_ARRAY_BUFFER, msh->data_vbo);
	glBufferData(GL_ARRAY_BUFFER, size, NULL, GL_STATIC_DRAW);

	/* Assigning data */
	
	/* Vertices */
	glBufferSubData(GL_ARRAY_BUFFER, off, sizeof(vec3)*msh->num_vertex,
		        msh->vertices);
	off += sizeof(vec3)*msh->num_vertex;
	pglerror("hum");

	/* There may not be any normal specified */
	if (!msh->normals)
		gen_normals(msh);
	pglerror("ok");

	/* No need for vertices anymore */
	free(msh->vertices);
	msh->vertices = NULL;

	/* Normals */
	glBufferSubData(GL_ARRAY_BUFFER, off, 
			sizeof(vec3)*msh->num_vertex, msh->normals);
	pglerror("shit");
	msh->normal_off = off;
	off += sizeof(vec3)*msh->num_vertex;
	
	/* Normals Not needed anymore */
	free(msh->normals);
	msh->normals = NULL;

	/* Texture coordinates */
	if (msh->tex_coords) {
		glBufferSubData(GL_ARRAY_BUFFER, off,
			     sizeof(vec2)*msh->num_vertex, msh->tex_coords);
		msh->tex_coord_off = off;
		off += sizeof(vec2)*msh->num_vertex;

		/* No need for that */
		free(msh->tex_coords);
		msh->tex_coords = NULL;
	}

	/* Vertex Colors */
	if (msh->colors) {
		glBufferSubData(GL_ARRAY_BUFFER, off,
			     sizeof(color3)*msh->num_vertex, msh->colors);
		msh->color_off = off;
		off += sizeof(color3)*msh->num_vertex;

		/* No need for that */
		free(msh->colors);
		msh->colors = NULL;
	}

	/* Fog coordinates */
	if (msh->fog_coords) {
		glBufferSubData(GL_ARRAY_BUFFER, off,
			     sizeof(coord_t)*msh->num_vertex, msh->fog_coords);
		msh->fog_coord_off = off;
		off += sizeof(coord_t)*msh->num_vertex;

		/* No need for that */
		free(msh->fog_coords);
		msh->fog_coords = NULL;
	}

	glBindBuffer(GL_ARRAY_BUFFER, 0);

	/* Finaly the Faces */
	glGenBuffers(1, &msh->elements_vbo);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, msh->elements_vbo);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(struct face)*msh->num_face,
		     msh->faces, GL_STATIC_DRAW);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
	free(msh->faces);
	msh->faces = NULL;

	/* Over with the setup */
	//pglerror("mesh setup done");
}

/* Mesh destruction */
static void mesh_destroy(struct mesh *msh)
{
	/* Destroying VBOS */
	if (msh->data_vbo)
		glDeleteBuffers(1, &msh->data_vbo);
		
	if (msh->elements_vbo)
		glDeleteBuffers(1, &msh->elements_vbo);

	/* Destroying Data */
	if (msh->faces)
		free(msh->faces);

	if (msh->vertices)
		free(msh->vertices);

	if (msh->normals)
		free(msh->normals);

	if (msh->tex_coords)
		free(msh->tex_coords);

	if (msh->colors)
		free(msh->colors);

	if (msh->fog_coords)
		free(msh->fog_coords);
}

/* Box dynamic table setup */
void mesh_dyn_setup(struct mesh_dynt *dynt)
{
	dynt->geom_dynt.setup = (geom_setup_func)mesh_setup;
	dynt->geom_dynt.rendergeom = (geom_rendergeom_func)mesh_rendergeom;
}

/* Box component */
const struct obj_comp mesh_comp = {
	.dyn_setup = (dyn_setup_func)mesh_dyn_setup,
	.destroy = (destroy_func)mesh_destroy
};

/* Box dynamic functions */
struct mesh_dynt mesh_dynt;


