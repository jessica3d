#include "jessica.h"
#include "geometry.h"
#include "rendering.h"

#define SIDE_PRECISION		10
#define HEIGHT_PRECISION	10

/* Cylinder display list */
static GLuint side_list;
static GLuint top_list;

static void render_top()
{
	static const double astep = M_PI/(2*SIDE_PRECISION);
	double ang = -M_PI;
	
	vec3 c = { .c = { .y = 1 } };
	glBegin(GL_TRIANGLE_FAN);
	glNormal3dv(vec3_j.v);
	for (int i = 0; i < 4*SIDE_PRECISION; i++) {
		c.c.x = cos(ang);
		c.c.z = sin(ang);

		glTexCoord2d(c.c.x*0.5 + 0.5, c.c.z*0.5 + 0.5);
		glVertex3dv(c.v);

		ang += astep;
	}

	glEnd();
}

/* Rendering of a cylinder "face" side */
static void render_side()
{
	static const double astep = M_PI/(2*SIDE_PRECISION);
	static const double hstep = 2.0/HEIGHT_PRECISION;
	static const double tstep[2] = { 0.25/SIDE_PRECISION, 
					 1.0/HEIGHT_PRECISION };

	/* Coordinates */
	double ang = M_PI/2;
	vec3 c[2] = { [1] = { .c = { .x = 0, .z = 1 } } };

	/* Texture coordinates */
	double texs[2] = { [1] = 0 };
	double text;

	for (int i = 0; i < SIDE_PRECISION; i++) {
		ang += astep;

		c[1].c.y = -1;
		c[0] = c[1];
		c[1].c.x = cos(ang);
		c[1].c.z = sin(ang);

		text = 0;
		texs[0] = texs[1];
		texs[1] += tstep[0];

		glBegin(GL_QUAD_STRIP);
		for (int j = 0; j <= HEIGHT_PRECISION; j++) {
			glTexCoord2d(texs[0], text);
			glNormal3d(c[0].c.x, 0, c[0].c.z);
			glVertex3dv(c[0].v);

			glTexCoord2d(texs[1], text);
			glNormal3d(c[1].c.x, 0, c[1].c.z);
			glVertex3dv(c[1].v);

			c[0].c.y += hstep;
			c[1].c.y += hstep;
			text += tstep[1];
		}
		glEnd();
	}
}

/* Rendering of the cylinder object */
void cylinder_rendergeom(struct cylinder *cyl)
{
	glPushMatrix();

	/* Size adjust */
	glScaled(cyl->radius, cyl->height/2, cyl->radius);
	if (cyl->side)
		glCallList(side_list);
	if (cyl->top)
		glCallList(top_list);
	if (cyl->bottom) {
		/* Texture is inverted on the bottom */
		glMatrixMode(GL_TEXTURE);
		glPushMatrix();
		glTranslated(0, 1, 0);
		glScaled(1, -1, 1);

		/* Face also is... */
		glMatrixMode(GL_MODELVIEW);
		glScaled(1, -1, 1);

		glCallList(top_list);

		glMatrixMode(GL_TEXTURE);
		glPopMatrix();
		glMatrixMode(GL_MODELVIEW);
	}

	/* Done */
	glPopMatrix();
}

/* Cylinder default value settings */
static void cylinder_init(struct cylinder *cyl)
{
	/* X3D Specification 13.3.3 */
	cyl->height = 2;
	cyl->radius = 1;
	cyl->side = cyl->bottom = cyl->top = 1;
}

/* Rendering setup */
static void cylinder_setup(struct cylinder *cyl)
{
	/* All cylinders use the same display list */
	if (side_list)
		return;

	/* Cylinder display list creation */
	side_list = glGenLists(1);
	glNewList(side_list, GL_COMPILE);

	/* Drawing side */
	render_side();

	glMatrixMode(GL_TEXTURE);
	glPushMatrix();
	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();

	for (int i = 0; i < 3; i++) {
		glMatrixMode(GL_TEXTURE);
		glTranslated(0.25, 0, 0);
		glMatrixMode(GL_MODELVIEW);
		glRotated(90, 0, 1, 0);

		render_side();
	}

	glMatrixMode(GL_TEXTURE);
	glPopMatrix();
	glMatrixMode(GL_MODELVIEW);

	glEndList();

	/* Top Face */
	top_list = glGenLists(1);
	glNewList(top_list, GL_COMPILE);
	render_top();
	glEndList();
}

/* Cylinder dynamic table setup */
void cylinder_dyn_setup(struct cylinder_dynt *dynt)
{
	dynt->geom_dynt.setup = (geom_setup_func)cylinder_setup;
	dynt->geom_dynt.rendergeom = (geom_rendergeom_func)cylinder_rendergeom;
}

/* Cylinder component */
const struct obj_comp cylinder_comp = {
	.init = (init_func)cylinder_init,
	.dyn_setup = (dyn_setup_func)cylinder_dyn_setup
};

/* Cylinder dynamic functions */
struct cylinder_dynt cylinder_dynt;


