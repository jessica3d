#include "jessica.h"
#include "shading.h"
#include "rendering.h"
#include "texture.h"


/* Material init for default settings */
static void material_init(struct material *mat)
{
	/* From specification 12.4.4 */
	mat->ambient = 0.2;
	mat->color = color4_grey(0.8);	/* Alpha to 1 for total opacity */
	mat->shininess = 0.2;
}

/* Material state spec */
static void material_begin(struct material *mat)
{
	/* TODO: Add transparency */
	color4 amb_color = color4_scale(mat->color, mat->ambient);

	/* Ambient component */
	glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT, amb_color.v);

	/* Diffuse component */
	glMaterialfv(GL_FRONT_AND_BACK, GL_DIFFUSE, mat->color.v);

	/* Specular component*/
	glMaterialfv(GL_FRONT_AND_BACK, GL_SPECULAR, mat->specular.v);
	glMaterialf(GL_FRONT_AND_BACK, GL_SHININESS, mat->shininess);

	/* Emissive color (why would someone use this?) */
	glMaterialfv(GL_FRONT_AND_BACK, GL_EMISSION, mat->emission.v);
}

/* Line rendering patterns */
void lining_begin(struct lining *ln)
{
	/* TODO */
}

/* Shading compoments setup */
void shading_setup(struct shading *shd)
{
	if (shd->texture)
		texture_setup(shd->texture);
}

/* Shading setting before rendering */
void shading_begin(struct shading *shd)
{
	/* Lining for wireframe structures */
	if (shd->material)
		material_begin(shd->material);
	else {
		/* Lighting only enabled when material defined */
		glDisable(GL_LIGHTING);
		if (shd->lining)
			lining_begin(shd->lining);
	}

	/* Texturing is nearly always applied */
	if (shd->texture)
		texture_begin(shd->texture);
}

void lining_end(struct lining *ln)
{
	/* TODO */
	
}

/* Shading state restore */
void shading_end(struct shading *shd)
{
	if (!shd->material) {
		glEnable(GL_LIGHTING);
		if (shd->lining)
			lining_end(shd->lining);
	}
	if (shd->texture)
		texture_end(shd->texture);
}

/* Lining default settings */
void lining_init(struct lining *ln)
{
	ln->line_style = LINE_SOLID;
}


const struct obj_comp lining_comp = {
	.init = (init_func)lining_init
};

const struct obj_comp material_comp = {
	.init = (init_func)material_init
};

const struct obj_comp shading_comp;
const struct obj_comp filling_comp;

