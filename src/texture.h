#ifndef TEXTURE_H
#define TEXTURE_H

#include "model.h"
#include "rendering.h"

/* Texture */
struct texture {
	struct obj	obj;
	int		num_loc;	/* Number of locations */
	char		**locs;		/* Locations of this texture */

	int		setup_done;	/* Setup status */
};

/* Texture dynamic functions */
struct texture_dynt {
	int	(*texsetup)(struct texture *tex);
	void	(*begin)(struct texture *tex);
	void	(*end)(struct texture *tex);
};

/* Loads texture data */
void texture_load(void);
/* Clears up all texture objects */
void texture_clear(void);

static inline int texture_setup(struct texture *tx)
{ int res; if (!tx->setup_done) {
    res = ((struct texture_dynt*)((struct obj*)tx)->type->dynt)->texsetup(tx);
    if (!res)
	    tx->setup_done = 1;
    return res;
  } else return 0; }

static inline void texture_begin(struct texture *tx)
{ ((struct texture_dynt*)((struct obj*)tx)->type->dynt)->begin(tx); }

static inline void texture_end(struct texture *tx)
{ ((struct texture_dynt*)((struct obj*)tx)->type->dynt)->end(tx); }

typedef	int(*texture_texsetup_func)(struct texture *);
typedef	void(*texture_begin_func)(struct texture *);
typedef	void(*texture_end_func)(struct texture *);

/* 2D Texture */
struct texture2d {
	struct texture	texture;	/* Base object */
	GLuint		tid;		/* GL texture object */

	int		width;		/* Texture width */
	int		height;		/* Texture height */
	double		npot_scale[2];	/* Non Power of two rescale */

	int		repeat_s;	/* Repeat on s dir flag */
	int		repeat_t;	/* Repeat on t dir flag */
};

struct texture2d_dynt {
	struct texture_dynt	texture_dynt;
} extern texture2d_dynt;

extern const struct obj_comp texture_comp;
extern const struct obj_comp texture2d_comp;

extern const struct obj_type texture2d_type;

#endif	/* TEXTURE_H */
