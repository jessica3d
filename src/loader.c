/* Memory management */
#include <stdlib.h>

/* isspace(3) */
#include <ctype.h>

/* sscanf(3) */
#include <stdio.h>

/* strchr(3) */
#include <string.h>

/* uint8_t */
#include <stdint.h>

#include <assert.h>

#include "loader.h"

#define BUF_INC		64
#define STR_INC		2

/* Generic type filler */
static int loader_fill(void *dest, const char **values, int num,
		       int opsize, const char *fmt)
{
	int i;
	char *dst = (char*)dest;
	int step;
	for (i = 0; i < num; i++) {
		while (*values && (isspace((*values)[0]) ||(*values)[0] == ','))
			(*values)++;

		if (sscanf(*values, fmt, dst, &step) < 1)
			break;
		*values += step;

		dst += opsize;
		if (!values[0])
			break;
	}

	return i;
}

/* Boolean value type filler */
int loader_fill_bool(int *dest, const char **values, int num)
{
	int i;
	for (i = 0; i < num; i++) {
		while (*values && (isspace((*values)[0]) ||(*values)[0] == ','))
			(*values)++;

		if (!strncmp(*values, "true", 4)) {
			dest[i] = 1;
			*values += 4;
		} else {
			if (strncmp(*values, "false", 5))
				break;
			dest[i] = 0;
			*values += 5;
		}
	}

	return i;
}

/* String loader */
int loader_fill_string(char **dest, const char **values, int num)
{
	int i;
	char *pos;
	int len;
	for (i = 0; i < num; i++) {
		while (isspace(**values))
			(*values)++;

		if (!**values)
			break;

		/* String might be quoted or not if on one line */
		if (!i && **values != '"') {
			len = strlen(*values);
			dest[0] = (char*)malloc(len + 1);
			memcpy(dest[i], *values, len+1);
			*values += len;
			return 1;
		}
		if (**values != '"')
			break;
		(*values)++;

		pos = strchr(*values, '"');
		if (!pos)
			break;

		len = pos - *values;
		dest[i] = (char*)malloc(len + 1);
		memcpy(dest[i], *values, len);
		dest[i][len] = 0;
		*values += len + 1;
	}

	return i;
}

/* Loading of double types */
int loader_fill_double(double *dest, const char **values, int num)
{
	return loader_fill(dest, values, num, sizeof(double), "%lf %n");
}

/* Loading of float types */
int loader_fill_float(float *dest, const char **values, int num) 
{
	return loader_fill(dest, values, num, sizeof(float), "%f %n");
}

/* Loading of int types */
int loader_fill_int(int *dest, const char **values, int num)
{
	int dummy;
	int res = loader_fill(dest, values, num, sizeof(int), "%d %n");

	const char *values_save = *values;
	if (loader_fill(&dummy, values, 1, sizeof(int), "%d %n")) {
		if (dummy >= 0)
			*values = values_save;
	}

	return res;
}

/* Value array loading */
int loader_fill_array(char **array, const char *values, int pack_num,
		      int opsize, const char *fmt)
{
	int res = pack_num;

	int buf_size = 0;
	int num_val = 0;
	int pos = 0;
	while (res >= pack_num) {
		/* Allocating required memory */
		if (num_val + pack_num > buf_size) {
			buf_size += BUF_INC;
			*array = (char*)realloc(*array, buf_size*opsize);
		}

		res = loader_fill((*array)+pos, &values, pack_num, opsize, fmt);
		num_val += res;
		pos += res*opsize;
	}

	/* Don't overuse memory... */
	*array = realloc(*array, num_val*opsize);

	return num_val;
}

int loader_fill_double_array(double **array, const char *values, 
			            int pack_num)
{
	return loader_fill_array((char**)array, values, pack_num, 
				 sizeof(double), "%lf %n");
}

static int loader_fill_coord_array(coord_t **array, const char *values, 
			           int pack_num)
{
	return loader_fill_double_array((double**)array, values, pack_num);
}

int loader_fill_vec3_array(vec3 **array, const char *values)
{
	return loader_fill_coord_array((coord_t**)array, values, 3);
}

/* Integer array filling */
int loader_fill_int_array(int **array, const char *values, int pack_num)
{
	int *stor = NULL;
	setlinebuf(stdout);
	int res = loader_fill_array(((char**)(void*)&stor), values, pack_num+1, 
				    sizeof(int), "%d %n");

	int new_count = 0;
	*array = (int*)malloc(res*sizeof(int));
	for (int i=0; i < res;) {
		memcpy((*array)+new_count*pack_num, stor+i, 
		       pack_num*sizeof(int));
		i += pack_num;
		new_count++;
		if (i == res)
			break;
		if (stor[i] < 0) {
			i++;
		} else {
			fprintf(stderr, 
				"Warning: Less than %d values in value list\n", 
				pack_num);
		}
	}

	free(stor);
	*array = (int*)realloc(*array, new_count*pack_num*sizeof(int));
	return new_count;
}

/* String array loading */
int loader_fill_string_array(char ***dest, const char *values)
{
	int num = 0;
	int alloced = STR_INC;

	*dest = (char**)malloc(STR_INC*sizeof(char*));
	for(;;) {
		if (num == alloced) {
			alloced += STR_INC;
			*dest = (char**)realloc(*dest, alloced*sizeof(char*));
		}

		if (!loader_fill_string(&(*dest)[num], &values, 1))
			break;
		num++;
	}

	*dest = realloc(*dest, num*sizeof(char**));
	return num;
}

/* Rotation loading to a quaternion */
int loader_fill_rot(quat *qt, const char **values)
{
	int res;
	vec3 axis;

	res = loader_fill_vec3(&axis, values);
	if (res < 1)
		return 0;

	angle_t ang = 0;
	res = loader_fill_angle(&ang, values, 1);

	*qt = quat_rot(vec3_normalize(axis), ang);

	return 1;
}

/* Rotation/Quaternion array loading */
int loader_fill_rot_array(quat **dest, const char *values)
{
	int num = 0;
	int alloced = BUF_INC;

	*dest = (quat*)malloc(BUF_INC*sizeof(quat));
	for(;;) {
		if (num == alloced) {
			alloced += BUF_INC;
			*dest = (quat*)realloc(*dest, alloced*sizeof(quat));
		}

		if (!loader_fill_rot(&(*dest)[num], &values))
			break;
		num++;
	}

	*dest = realloc(*dest, num*sizeof(quat));
	return num;
}

