#ifndef XML_X3D_H
#define XML_X3D_H


/* X3D parsing from a file */
int xml_x3d_file_parse(const char * filename, struct scene *scene);



#endif	/* XML_X3D_H */
