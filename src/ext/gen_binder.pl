#!/usr/bin/perl

@ext = ();

open IN, $ARGV[0] || die "Can't open $ARGV[0]";
open OUT, ">$ARGV[1].c";
select OUT;

print <<EOS
/* This code is generated automatically */

#include <GL/gl.h>
#include <GL/glext.h>
#include <config.h>
#include <gtk/gtkgl.h>

#include <assert.h>

EOS
;



while (<IN>) {
	chomp;
	if (!$_) {
		next;
	}
		
	my $proto = 'PFN' . uc($_) . 'PROC';
	my %pair = ( ext => $_, proto => $proto );
	push @ext, \%pair;

	print "$proto $_" . ";\n";
}

close IN;


print <<EOS

int bind_ext(void)
{
EOS
;

for my $pair (@ext) {
	print "\t" . $pair->{ext} . ' = (' . $pair->{proto} . ")\n\t\t" . 'gdk_gl_get_proc_address("' . $pair->{ext} . '");' . "\n";
	print "\tif (!" . $pair->{ext} . ") return -1;\n"
}

print <<EOS
	return 0;
}
EOS
;


close OUT;

open OUT, ">$ARGV[1].h";
$up = uc($ARGV[1]);

print <<EOS
#ifndef ${up}_H
#define ${up}_H

#include <GL/gl.h>
#include <GL/glext.h>

#ifdef __cplusplus
extern "C" {
#endif
int bind_ext();

EOS
;

for my $pair (@ext) {
	print "extern " . $pair->{proto} . " " . $pair->{ext} . ";\n";
}

print <<EOS

#ifdef __cplusplus
}
#endif

#endif  // ${up}_H

EOS
;


